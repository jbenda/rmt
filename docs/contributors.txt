/*! \page contributors List of Contributors
 \brief  List of Contributors

 The following people have contributed to the development of the RMT code:

Holly Lavery \n
Aaron Nugent \n
Sean Marshallsay \n
Lynda Hutcheson \n
Luke Roantree \n
Greg Armstrong \n
Jakub Benda \n
Andrew Brown \n
Daniel Clarke \n
Jimena Gorfinkiel \n
Kathryn Hamilton \n
Michael Lysaght \n
Zdenek Masin \n
Robert McGibbon \n
Laura Moore \n
Lampros Nikolopoulos \n
Jonathan Parker \n
Martin Plummer \n
Ken Taylor \n
Hugo van der Hart \n
Jack Wragg \n
*/

