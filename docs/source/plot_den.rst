plot_den command
***********************

.. automodule:: rmt_utilities.plot_dist.plot_den
.. argparse::
   :module: rmt_utilities.reform_cli
   :func: density_command_line
   :prog: RMT_plot_den

