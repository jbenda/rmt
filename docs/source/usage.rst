=====
Usage
=====

Regression tests
****************

For simple, manual checks between two runs of the same calculation :ref:`rmt_compare`
can be used.

Programmatic testing of a single test case
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To test a single RMT calculation produces output matching a previous version,
the :class:`rmt_utilities.regress.testcase` class can be used.

For programmatic regression testing several things are required:
* a reference calculation in a directory named ``regress_run``, 
* a set of input files in a directory named ``inputs``
* an RMT executable
* a parallel executor (e.g. ``mpirun``) 

E.G I have built a new RMT executable
``rmt.x`` in the ``build/bin`` directory, and have a test directory ``tests/Ne``:


.. code-block:: bash

   >>> ls -F build/bin
   rmt.x*
   >>> ls -F tests/Ne
   inputs/ regress_run/
   >>> which mpirun
   /opt/local/bin/mpirun

I can execute an rmt calculation and assert that the output matches that of
``tests/Ne/regress_run`` with 


.. code-block:: python

   from rmt_utilities.regress import testcase

   calc = testcase('./tests/Ne', './build/bin/rmt.x', '/opt/local/bin/mpirun')
   assert calc.runTest()
   calc.cleanupdir()

The python snippet will execute without error and remove all new calculation
directories (``calc.cleanupdir()``) if the data matches ``regress_run``.

You may specify a tolerance (number of decimal places of expected agreement)
with the tolerance argument to calc.runTest(). The default is 9, but more or
less strict agreement can be enforced, e.g.:

.. code-block:: python

   from rmt_utilities.regress import testcase

   calc = testcase('./tests/Ne', './build/bin/rmt.x', '/opt/local/bin/mpirun')
   assert calc.runTest(tolerance=14)
   calc.cleanupdir()

If you wish to oversubscribe the parallel execution of rmt.x (i.e. run
an MPI job with more tasks than there are physical cores available) you may pass
the --oversubscribe option as the optional argument ``mpiopts`` in the testcase:

.. code-block:: python

   calc = testcase('./tests/Ne', './build/bin/rmt.x', '/opt/local/bin/mpirun',
                   mpiopts='--oversubscribe')

Similarly, if the parallel executor you are using assigns mpi tasks with a
different command line option, this may be set with the optional argument
``taskopt``. E.G for the slurm batch scheduler, the ``srun`` parallel executor
uses ``--ntasks``:

.. code-block:: python

   calc = testcase('./tests/Ne', './build/bin/rmt.x', '/opt/local/bin/srun',
                   taskopt = '--ntasks')


Programmatic testing of a multiple test cases
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For efficient development, it may be desirable to trigger a suite of test
calculations to ensure that all match the previously generated data. This is
accomplished using the :class:`rmt_utilities.regress.RMT_regression_tests` class.

The list of test calculation directories is passed as a yaml file as the first
argument to the RMT_regression_tests instance. The other arguments match the
individual testcase. The yaml file contains key:value pairs with the name of the
test calculation and the path to the test directory:

.. code-block:: bash

   >>> head -3 testcalcs.yml
   ---
   Ne : ./tests/Ne/
   Ar : ./tests/Ar/

The RMT_regression_tests are then built and executed 

.. code-block:: python

   from rmt_utilities.regress import RMT_regression_tests as rgt

   alltests = rgt('./testcalcs.yml', './build/bin/rmt.x', '/opt/local/bin/mpirun')
   for calc in alltests:
      assert calc.runTest()
      calc.cleanupdir()
