Changes to RMT implemented to enable molecular calculations
===========================================================

I have summarized below the most important changes that have been made to implement the molecular RMT. In the following description ``RMT" refers to the atomic code and ``MRMT" to its molecular equivalent.

The only parts of the code that have been changed were the ones using the lenght gauge. No attempt has been made to implement the dipole velocity gauge since its use in the molecular case seems unlikely in the near future.

1) The program RMT_INTERFACE from ukrmol-out/dev-branches/zdenek now generates a single file molecular_data which is required by MRMT. To generate this file RMT_INTERFACE requires the standard input files for SWINTERF, the N-electron target properties file and the N+1 dipoles file generated by CDENPROP. At the moment the requirement of MRMT is that N+1 dipoles for all three components of the dipole operator are included on the file. All three components may not be needed for a particular calculation but they still must be on the file. 

The reading of the molecular_data file can be understood best by looking into the MRMT output file log.out for the test He_test_molecular_benchmark which shows the reading process in the section: 

          read_molecular_data: start
          ============================
...
          read_molecular_data: finished
          =============================

This reading process includes contracting the inner region dipole matrix elements for the x,y,z components with the given direction of the electric field as described in point 5 below.

2) In addition to the input data mentioned above RMT_INTERFACE requires the values NFDM, DELTA_R which define the set of inner region finite-difference points in the same way as in RMT. Given these values RMT_INTERFACE generates the amplitudes w_ik for the set of these points and saves them (together with the standard amplitudes for the R-matrix radius) on the file molecular_data. Once the file molecular_data has been read-in it is checked that the set of inner region points obtained from the molecular_data file is the same as the one generated by MRMT. Reading of the file molecular_data has been implemented in the module READHD replacing the atomic routines Read_D_*, Read_H_*, Read_Spline* with the pair of routines READ_MOLECULAR_DATA and REMOVE_STATES. The latter routine removes the high-lying states from the calculation using exactly the same algorithm as in RMT.

Note on the definition of the channel amplitudes:
=================================================

In ukrmol-out/swinterf the definition of the boundary amplitudes w_ik is such that the expression for the R-matrix at energy E is: a*R_ij = sum_k w_ik*w_jk/(Ek-E), where ``a" is the R-matrix radius.
This means that the raw boundary amplitudes w_ik are multiplied by the factor 1/sqrt(2) so that the factor 1/2 (which in the std. definition of the R-matrix multiplies the sum over k above) is absorbed
into the product w_ik*w_jk. This definition of w_ik is different to the one assumed in the atomic codes which do not include the factor 1/sqrt(2). That this is true can be seen from the interface to PFARM (pfarm_interface.f90) which multiplies the amplitudes obtained from swinterf by sqrt(2). When generating the file molecular_data the program RMT_INTERFACE applies the factor sqrt(2) to the amplitudes generated by SWINTERF obtaining amplitudes compatible with RMT.

3) The file molecular_data includes the non-zero Gaunt coupling coefficients <l1,m1|1,m|l2,m2> for the real spherical harmonics for all combinations of the angular channel numbers (l1,m1), (l2,m2) and m=-1,0,1. These coefficients are needed in MRMT to construct the outer region WP potential describing the laser interaction with the continuum electron.

4) Module INITIAL_CONDITIONS (input file input.conf):
   A) Added a new input variable E_field_xyz(1:3): the direction of the electric field vector in the molecular frame.
   B) The input variable GS_finast has been replaced with a new variable GS_IRR which has the meaning of the irreducible representation of the initial state. The value of GS_finast, which is the sequence number of the initial-state's symmetry as stored on the file molecular_data, is determined automatically in READHD.

5) The variable E_field_xyz is used in READHD to figure out which wavefunction symmetries are coupled together (those need to be included in the calculation) for the particular choice of the Electric field vector. The input vector does not have to be normalized to 1. The vector is normalized and used in MRMT only at the reading stage in the routine READ_MOLECULAR_DATA: the x,y,z components of the target dipoles and the N+1 dipoles are immediately contracted with the x,y,z components of the E-field direction. The m=-1,0,1 coupling coefficients described in point 3 above are contracted with the x,y,z components of the field too, producing a set of coupling coefficients defined only by two pairs of channel numbers: (l1,m1), (l2,m2).

6) Following the calls to READ_MOLECULAR_DATA and REMOVE_STATES the data are redistributed from the master in the same way as in RMT but there are several new variables that needed distributing, e.g. the channel m-quantum numbers. Most importantly the new matrix dipole_coupled(8,8) of type logical is determined by all tasks. This matrix is constructed by the master in READ_MOLECULAR_DATA. The other tasks reconstruct it once they've received all variables that master has. If dipole_coupled(i,j) == .true. then the pair of symmetries with sequence numbers i and j are dipole coupled. This matrix is used in the later stages of the calculation to distribute the dipole blocks from the master and to perform the matrix*vector multiplications.

7) The outer region potentials WE, WD, WP are constructed in the module LRPOTS just like in RMT but with some modifications to accommodate the molecular case. The WE potential is constructed just like in RMT using the coefficients obtained from the molecular_data file. The WD and WP potentials are calculated as described in the separate document ``MRMT outer region potentials".

8) The amplitudes in the inner region are distributed directly from the array wamp2 obtained from the molecular_data file. This is done as in RMT in the module DISTRIBUTE_WV_DATA, routine Send_IB_SurfAmps_To_Lb_Masters. However, all parts of the routine dealing with assembling the amplitudes from the B-spline coefficients have been replaced with calls to MPI SEND/RECEIVE.

9) DISTRIBUTE_HD_BLOCKS, DISTRIBUTE_HD_BLOCKS2: these modules required replacing the atomic rules determining which dipole blocks are coupled with new rules for the molecular case.

We use the array dipole_coupled(i,j) constructed earlier (see point 6) to distribute the dipole blocks from the inner master to the block masters for each symmetry. The new routine Setup_And_Distribute_Dblocks from the module DISTRIBUTE_HD_BLOCKS has been written to perform the redistribution. In it all inner block masters go through the whole table dipole_coupled(i,j). If the symmetries with indices i,j are coupled the inner master sends the (i,j) dipole block to the block master with rank j-1. The block master j-1 receives the dipole block from the inner master. In the molecular case (depending on the point group symmetry and the Electric field direction chosen) anywhere from one to three symmetries can couple to any given symmetry. This is true for the atomic case also and the three arrays dblock_d, dblock_s, dblock_u (d - down, s - same, u - up) are used in RMT to store the dipole blocks. We keep using the same arrays in MRMT. The arrays dblock_d, dblock_s, dblock_u are mapped to the indices of the symmetries in the following way:

Assume we include 8 symmetries in the calculation. Let's pick a symmetry j and assume that the logical array dipole_coupled(:,j) contains the following values: T,F,T,T,F,F,F,F
This means that symmetries with indices 1,3,4 are coupled to symmetry j. The arrays dblock_u,s,d are then mapped onto the indices of the symmetries in the order from the lowest index to the largest index. In the present case the mapping would be:

dblock_d-> 1
dblock_s-> 3
dblock_u-> 4

The mapping between the d,s,u indices and the symmetry indices is performed by the new routine Setup_D_blocks_to_d_s_u_mapping in the module DISTRIBUTE_HD_BLOCKS. The same logic in mapping the d,s,u indices has been employed in the module DISTRIBUTE_HD_BLOCKS2 where the dipole blocks are distributed to the child tasks of each block master.

9) LIVE_COMMUNICATIONS: modifications were required to carry out the matrix*vector multiplication in the routine Parallel_Matrix_Vector_Multiply. To do this the routines Setup_Block_Links and Send_Recv_Master_Vecs_Ptp had to be modified. In Setup_Block_Links the integer arrays wf_syms_need(3,8) and n_syms_need(8) are determined by each inner region processor. n_syms_need(j) has the meaning of the number of wavefunction symmetries which are needed to perform the matrix*vector multiplication by the tasks assigned to symmetry with index j. The array wf_syms_need(1:n_syms_need(j),j) contains indices of the symmetries needed to perform the matrix*vector multiplication by the tasks assigned to symmetry with index j. In routine Send_Recv_Master_Vecs_Ptp algorithm similar to the distribution of the dipole blocks is employed: each processor goes through the array table wf_syms_need(3,8) and sends and receives wavefunction blocks. The output of the routine has been augmented by three integers: n_up,n_down,n_same which have the meaning of the number of elements in the arrays vecup,vecdown,vecsame which have been received from other processes. The routine Parallel_Matrix_Vector_Multiply, which is a caller of Send_Recv_Master_Vecs_Ptp, performs the matrix*vector multiplications by the wavefunctions that have been received (i.e. those that couple to the block master's symmetry).

10) OUTER_HAMILTONIAN: routines incr_w_WE_Ham_x_Vector_outer, incr_w_WD_Ham_x_Vector_outer, incr_w_WP_Ham_x_Vec_out_len have been modified to use the coupling rules for the molecular case as described in the document ``MRMT outer region potentials". Additionally, the way any variable of the name total_L* is used had to be changed. In RMT total_L stands for the angular momentum of the system (starting from 0) but in MRMT it stands for the irreducible representation so it starts from 1. Especially, any occurence of ``total_L+1" in the ``molecular" subroutines had to be changed to just ``total_L".

Z. Masin, MBI Berlin, 24/10/2017
