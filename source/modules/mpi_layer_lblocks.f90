! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets up the first level of parallelism in the inner region- divides the
!> Hamiltonian matrix into symmetry blocks and then allocates processors to blocks based on
!> the block size. Also sets up second layer of parallelism- sets up communicators
!> within each block, and between the block masters.
!> In this iteration, if the number of LML blocks is greater than the number of inner region pes,
!> then the routine setup_Lblock_communicator_size_pn is called to distribute pes among blocks.
!> The general algorithm is described in the eCSE report, but the commenting in code could be
!> better and will be improved. Compared to the previous Gitlab update (branch mp_ecse) the restrictions
!> on the variable numbers of LML blocks per task has been relaxed (check the gitlab commit comparisons.
!> This means the program should accept a broader range of inner region pe numbers, however performance
!> may suffer. If the program stops unexpctedly at start-up, check for comments starting 'Whoops' or
!> 'problem'. The amount of start-up output can be large, this may be comented out in future.
!> A Huffman tree approach is being studied to improve the balance of blocks to pes in both cases.


MODULE mpi_layer_lblocks

    USE readhd,             ONLY: no_of_LML_blocks
    USE mpi_communications, ONLY: mpi_comm_region
    USE rmt_assert,         ONLY: assert
    USE MPI

    IMPLICIT NONE

    INTEGER, ALLOCATABLE, SAVE  :: Lb_comm_size(:)
    INTEGER, SAVE               :: Lb_color, Lb_comm, Lb_size, Lb_rank
    INTEGER, SAVE               :: Lb_master_color, Lb_master_rank
    INTEGER, SAVE               :: Lb_m_comm, Lb_m_size, Lb_m_rank
    INTEGER, SAVE               :: my_LML_block_id  ! = my_ang_mom_id = Lb_color
    INTEGER, SAVE               :: my_num_LML_blocks ! = my_num_ang_mom_blocks
    INTEGER, SAVE               :: max_num_LML_blocks ! = largest value of my_num_ang_mom_blocks
    INTEGER, allocatable, SAVE  :: my_LML_blocks(:)  ! set of LML blocks for each block master (Lb_m_rank) 
    INTEGER, SAVE               :: my_ground_state_block  ! local index for ground state (set to -9999 otherwise)
    INTEGER, SAVE               :: my_ground_state_row  ! local row position index for ground state (set to -9999 otherwise)
    INTEGER, allocatable, SAVE  :: master_for_LML_blocks(:)  ! rank of block master (Lb_m_rank) dealing with LML
    INTEGER, allocatable, SAVE  :: LML_blocks_per_Lb_master(:)  ! number of LML blocks for each block master
    LOGICAL, SAVE               :: i_am_block_master
    LOGICAL, SAVE               :: i_have_gs_L
    LOGICAL, SAVE               :: i_am_gs_master
    INTEGER, SAVE               :: pe_gs_id
    INTEGER, SAVE               :: inner_region_rank
    INTEGER, allocatable, SAVE  :: num_blocks_pe(:), block_start_pe(:), pes_per_Lblock(:)

    INTEGER, SAVE               :: LML_pe_method
    integer, save               :: istep_v 

    PRIVATE setup_Lblock_communicator_size
    PRIVATE setup_colors_for_Lblock_comms
    PRIVATE setup_inner_region_rank
 
    PUBLIC setup_inner_region_comms
    PUBLIC createcomm_order_Lblock
    PUBLIC createcomm_order_Lblock_masters
    PUBLIC Lb_m_comm, Lb_comm, Lb_size, Lb_rank, Lb_m_size, Lb_m_rank
    PUBLIC dealloc_setup_Lblock_comm_size
    PUBLIC i_am_block_master
    PUBLIC my_LML_block_id
    PUBLIC my_num_LML_blocks, max_num_LML_blocks
    PUBLIC master_for_LML_blocks, LML_blocks_per_Lb_master, num_blocks_pe
    PUBLIC my_ground_state_block
    PUBLIC my_ground_state_row
    PUBLIC inner_region_rank, istep_v

CONTAINS

!-----------------------------------------------------------------------
!SETUP COMMUNICATORS AASOCIATED WITH EACH L BLOCK   
!-----------------------------------------------------------------------

    SUBROUTINE setup_inner_region_comms(i_am_inner_master, states_per_LML_block, &
                                        my_ang_mom_id)

        LOGICAL, INTENT(IN)    :: i_am_inner_master
        INTEGER, INTENT(IN)    :: states_per_LML_block(-1:no_of_LML_blocks)
        INTEGER, INTENT(INOUT) :: my_ang_mom_id ! lowest LML associated with pe

        CALL setup_inner_region_rank
        CALL Setup_Lblock_Communicator_Size(i_am_inner_master, states_per_LML_block)
        CALL Setup_Colors_For_Lblock_Comms(my_ang_mom_id, states_per_LML_block)

    END SUBROUTINE setup_inner_region_comms

!-----------------------------------------------------------------------

    SUBROUTINE setup_inner_region_rank

        INTEGER :: ierr

        CALL mpi_comm_rank(mpi_comm_region, inner_region_rank, ierr)

    END SUBROUTINE setup_inner_region_rank

!-----------------------------------------------------------------------
! DDAC: NOTE THAT EVERY 'Lblock' BELOW SHOULD BE UNDERSTOOD AS 'LMLblock'
!-----------------------------------------------------------------------

    SUBROUTINE setup_Lblock_communicator_size(i_am_inner_master, states_per_LML_block)

        USE initial_conditions, ONLY : debug

        LOGICAL, INTENT(IN) :: i_am_inner_master
        ! ACB: hugo fix for bug in inner block allocation
        INTEGER, INTENT(IN) :: states_per_LML_block(-1:no_of_LML_blocks)

        INTEGER :: no_of_inner_region_pes
        INTEGER :: err, L, Iterations
        INTEGER :: states_per_sub_block(no_of_LML_blocks)
        INTEGER :: biased_states_per_LML_block(no_of_LML_blocks)
        INTEGER :: max_sub_block_LML(1), no_of_remaining_pes

        ALLOCATE (Lb_comm_size(no_of_LML_blocks), pes_per_lblock(no_of_LML_blocks), stat=err)
        CALL assert(err .EQ. 0, 'allocation error in Setup_Lblock_Communicator_Size')

        CALL MPI_COMM_SIZE(mpi_comm_region, no_of_inner_region_pes, err)

        LML_pe_method = 0
        my_num_LML_blocks = 1  ! default
        IF (no_of_inner_region_pes < no_of_LML_blocks) THEN
            PRINT *, 'no_of_inner_region_pes = ', no_of_inner_region_pes
            PRINT *, 'Number of LML blocks     = ', no_of_LML_blocks
!            PRINT *, 'Must have no_of_inner_region_pes >= no_of_LML_blocks'
            PRINT *, 'Entering new code for more than 1 LML block per pe'
            LML_pe_method = 1
!  fixed value allocation of LML blocks to pes, editable for explicit customization
!            call setup_Lblock_communicator_size_pt(i_am_inner_master, states_per_LML_block)
!  automated allocation of LML blocks to pes, can be edited for tweaking            
            call setup_Lblock_communicator_size_pn(i_am_inner_master, states_per_LML_block)
            return 
        END IF

        Lb_comm_size = 1
        no_of_remaining_pes = no_of_inner_region_pes - no_of_LML_blocks

        biased_states_per_LML_block = NINT((DBLE(states_per_LML_block(1:no_of_LML_blocks)))**1.8)
        ! Bias the allocation by pretending we had a different no of states per block.
        ! Perfect parallelism: would want States_Per_Sub_Block = states_per_LML_block**2

        states_per_sub_block = biased_states_per_LML_block

        DO iterations = 1, SUM(states_per_LML_block)

            IF (no_of_remaining_pes < 1) THEN
                EXIT
            END IF

            ! allocate the PE to LML_Block with the most states:

            max_sub_block_LML = MAXLOC(states_per_sub_block, 1)
            L = max_sub_block_LML(1)
            Lb_comm_size(L) = Lb_comm_size(L) + 1
            no_of_remaining_pes = no_of_remaining_pes - 1

            states_per_sub_block(L) = biased_states_per_LML_block(L)/Lb_comm_size(L)

        END DO

        CALL assert(SUM(Lb_comm_size) .EQ. no_of_inner_region_pes, 'Number of processors assigned to each LML block is incorrect.')

        IF (i_am_inner_master .AND. debug) THEN
            PRINT *, 'LML block Communicator size'
            DO L = 1, no_of_LML_blocks              ! DDAC: NOTE THAT L NO LONGER ASSUMES THE VALUES OF THE ANGULAR MOMENTA
                PRINT *, L, Lb_comm_size(L)         ! AND IS SIMPLY AN INDEX
            END DO
        END IF

    END SUBROUTINE setup_Lblock_communicator_size

    SUBROUTINE setup_Lblock_communicator_size_pn(i_am_inner_master, states_per_LML_block)

      USE initial_conditions, ONLY : debug, ellipticity, molecular_target, cross_polarized, &
                                     euler_alpha, euler_beta, euler_gamma, ellipticity_xuv, &
                                     xy_plane_desired, ml_max 

        LOGICAL, INTENT(IN) :: i_am_inner_master
        ! ACB: hugo fix for bug in inner block allocation
        INTEGER, INTENT(IN) :: states_per_LML_block(-1:no_of_LML_blocks)

        INTEGER :: no_of_inner_region_pes
        INTEGER :: err, L, Iterations
        INTEGER :: states_per_sub_block(no_of_LML_blocks)
        INTEGER :: biased_states_per_LML_block(no_of_LML_blocks)
        INTEGER :: max_sub_block_LML(1), no_of_remaining_pes
        REAL    :: biased_average, test, test2, test3, diff_below, diff_above, temp_sum
        INTEGER :: lml, i, j, sum_num, pe_num, pe_numm, lml_temp, lml_multi, i_last, my_starting_block
        INTEGER, allocatable :: biased_tot_pe(:)
        integer  ::   num, ist, isf, number_to_divide_by, lml_temp2, num_av, number_to_check_1, &
                      number_to_check_2, num_extra_LML, npe_extra_st, num_spare_pes, num_catchup, nrem, &
                      my_unit  
        
        CALL MPI_COMM_SIZE(mpi_comm_region, no_of_inner_region_pes, err)
        ALLOCATE (num_blocks_pe(no_of_inner_region_pes), &
                  block_start_pe(no_of_inner_region_pes), biased_tot_pe(no_of_inner_region_pes), stat=err)
        CALL assert(err .EQ. 0, 'allocation error in Setup_Lblock_Communicator_Size')
        

        Lb_comm_size = 1
        biased_states_per_LML_block = NINT((DBLE(states_per_LML_block(1:no_of_LML_blocks)))**1.8)
        ! Bias the allocation by pretending we had a different no of states per block.
        ! Perfect parallelism: would want States_Per_Sub_Block = states_per_LML_block**2


! note no need for wp here
        number_to_divide_by = no_of_inner_region_pes
        states_per_sub_block = biased_states_per_LML_block
        pes_per_Lblock(1:no_of_LML_blocks) = 1

! Keep the range of LML blocks per pe close to average for molecular and elliptically polarized light.
! This keeps the number of isend/irecv messages fetching and carrying data to a reasonable size.
! Allow the range to be large for linearly polarized light as the number of messages is small and
! we want equal distribution of work.         
        num_av = NINT(REAL(no_of_LML_blocks) / REAL(no_of_inner_region_pes))
        if (REAL(SUM(ellipticity(:))) == 0.0 .and. .NOT.(molecular_target) &
             .AND. .NOT.(ANY(cross_polarized)) .AND. .NOT.(ANY(xy_plane_desired)) &
             .AND. ml_max == 0 &
             .AND. REAL(SUM(ellipticity_xuv(:))) == 0.0 .AND. REAL(SUM(euler_alpha(:))) == 0.0 &
             .AND. REAL(SUM(euler_beta(:))) == 0.0 .AND. REAL(SUM(euler_gamma(:))) == 0.0) &
             num_av = no_of_LML_blocks  
        ! ie don't allow too many LML blocks per pe as the multiple message wait time slows things down  
        Lb_comm_size(1:no_of_lml_blocks) = pes_per_Lblock(1:no_of_lml_blocks)  
        
        number_to_check_1 = 0
        number_to_check_2 = 0
        do !  several goes at getting the numbers right: it should not be able to go on indefinitely 
        biased_average = REAL(SUM(biased_states_per_LML_block)) / REAL(number_to_divide_by) 
        sum_num = 0
        temp_sum = 0
        pe_num = 1
        lml_temp = 1
        lml_multi = 0
        i_last = 0
        block_start_pe = 0
        num_blocks_pe = 0
        biased_tot_pe = 0
        DO lml = 1, no_of_LML_blocks

           sum_num = sum_num + biased_states_per_LML_block(lml) 
           diff_below = ABS(biased_average - temp_sum)
           diff_above = ABS(biased_average - sum_num) 
           if (inner_region_rank == 0) then
              print *, 'lml, pe_num, lml_temp, sum_num, temp_sum =', lml, pe_num, lml_temp, sum_num, temp_sum
              print *,  'biased_states_per_LML_block(lml) =', biased_states_per_LML_block(lml)
              print *, 'biased_average, diff_below, diff_above =', biased_average, diff_below, diff_above
           end if
!           if (diff_below < diff_above .or. (lml - lml_temp) > (num_av + 1)) then
           if (diff_below < diff_above .or. (lml - lml_temp) > (num_av )) then
              num_blocks_pe(pe_num) = num_blocks_pe(pe_num) + lml - lml_temp 
              if (block_start_pe(pe_num) == 0) block_start_pe(pe_num) = lml_temp
              biased_tot_pe(pe_num) = temp_sum
              lml_temp = lml
              sum_num = biased_states_per_LML_block(lml)
              temp_sum = biased_states_per_LML_block(lml)
              pe_num = pe_num + 1 
              lml_multi = 0
              lml_temp2 = lml
               if (pe_num > no_of_inner_region_pes) then
                 if (pe_num > no_of_inner_region_pes + 1 .or. lml_temp /= no_of_LML_blocks) exit
              end if
           else
              temp_sum = sum_num
              lml_temp2 = lml
           end if
        end do
        if ((pe_num == no_of_inner_region_pes .and. pes_per_lblock(no_of_LML_blocks) == 1 &
             .and. lml_temp == no_of_LML_blocks) &
      &       .or.  (i_last == 1 .and. lml_multi /= 0)) then
           num_blocks_pe(pe_num) = no_of_LML_blocks + 1 - lml_temp
           block_start_pe(pe_num) = lml_temp
           biased_tot_pe(pe_num) = temp_sum
           exit
           !           print *, ' blocks_per_pe set OK'
        else if ((pe_num == no_of_inner_region_pes .and. pes_per_lblock(no_of_LML_blocks) == 1 &
             .and. lml_temp2 == no_of_LML_blocks) &
      &       .or.  (i_last == 1 .and. lml_multi /= 0)) then
           num_blocks_pe(pe_num) = no_of_LML_blocks + 1 - lml_temp
           block_start_pe(pe_num) = lml_temp
           biased_tot_pe(pe_num) = temp_sum
           exit
           !           print *, ' blocks_per_pe set OK'
        else if ((pe_num == no_of_inner_region_pes + 1 .and. pes_per_lblock(no_of_LML_blocks) == 1 &
             .and. lml_temp == no_of_LML_blocks) &
      &            .or. (i_last == 1 .and. i_last == 0)) then
           pe_num = pe_num - 1
           num_blocks_pe(pe_num) = num_blocks_pe(pe_num) + no_of_LML_blocks + 1 - lml_temp
           biased_tot_pe(pe_num) = biased_tot_pe(pe_num) + temp_sum
           exit
           !           print *, ' blocks_per_pe set OK'
!        else if (lml_temp <= no_of_LML_blocks .or. lml_temp2 <= no_of_LML_blocks) then
        else if (lml_temp2 < no_of_LML_blocks) then
           number_to_check_1 = number_to_divide_by
           number_to_divide_by = number_to_divide_by - 1
           if (number_to_divide_by == number_to_check_2) then
              if (inner_region_rank == 0) then
                 print *, 'number_to_divide_by is oscillating, ending loop'
              end if
              num_extra_LML = no_of_LML_blocks - lml_temp2 + 1
              lml_temp = no_of_LML_blocks - num_extra_LML + 1
              npe_extra_st = no_of_inner_region_pes - num_extra_LML + 1
              num_blocks_pe(npe_extra_st) = num_blocks_pe(npe_extra_st) + 1
              biased_tot_pe(npe_extra_st) = biased_tot_pe(npe_extra_st) + biased_states_per_LML_block(lml_temp)
              do i = npe_extra_st + 1, no_of_inner_region_pes
                 block_start_pe(i) = block_start_pe(i) + i - npe_extra_st
                 num_blocks_pe(i) = num_blocks_pe(i) + 1
                 sum_num = 0
                 lml_temp = block_start_pe(i) - 1
                 do j = 1, num_blocks_pe(i)
                    sum_num = sum_num + biased_states_per_LML_block(lml_temp + j)
                 end do   
                 biased_tot_pe(i) = sum_num
              end do
              exit
           end if 
           if (inner_region_rank == 0) print *, 'number_to_divide_by set to:', number_to_divide_by 
           if (number_to_divide_by < (no_of_inner_region_pes - (no_of_inner_region_pes / 3))) &
                call assert (.false., 'number_to_divide_by is too small, think again')
           cycle
        else if (lml_temp2 == no_of_LML_blocks .and. pe_num <= no_of_inner_region_pes) then
           number_to_check_2 = number_to_divide_by
           number_to_divide_by = number_to_divide_by + 1
           if (number_to_divide_by == number_to_check_1) then
              if (inner_region_rank == 0) then
                 print *, 'number_to_divide_by is starting to oscillate'
              end if
              num_spare_pes = no_of_inner_region_pes - pe_num + 1
              num_extra_LML = no_of_LML_blocks - lml_temp + 1
              if (num_extra_LML >= num_spare_pes) then
                 nrem = MOD(num_extra_LML, num_spare_pes)
                 if (block_start_pe(pe_num -1) + num_blocks_pe(pe_num - 1) /= lml_temp) &
                       call assert(.false., 'problem with lml_temp after oscillation')
                 do i = pe_num, pe_num + nrem - 1
                    block_start_pe(i) = block_start_pe(i-1) + num_blocks_pe(i-1)
                    num_blocks_pe(i) = 2
                    sum_num = 0
                    lml_temp = block_start_pe(i) - 1
                    do j = 1, 2
                       sum_num = sum_num + biased_states_per_LML_block(lml_temp + j)
                    end do   
                    biased_tot_pe(i) = sum_num
                 end do   
                 do i = pe_num + nrem, no_of_inner_region_pes
                    block_start_pe(i) = block_start_pe(i-1) + num_blocks_pe(i-1)
                    num_blocks_pe(i) = 1
                    lml_temp = block_start_pe(i) 
                    biased_tot_pe(i) = biased_states_per_LML_block(lml_temp)
                 end do   
                 exit
              else
                 num_catchup = num_spare_pes - num_extra_LML                 
                 npe_extra_st = pe_num  - num_catchup
                 lml_temp = block_start_pe(npe_extra_st) + num_blocks_pe(npe_extra_st) - 1
                 num_blocks_pe(npe_extra_st) = num_blocks_pe(npe_extra_st) - 1
                 biased_tot_pe(npe_extra_st) = biased_tot_pe(npe_extra_st) - biased_states_per_LML_block(lml_temp)
                 do i = npe_extra_st + 1, npe_extra_st + num_catchup - 1
                    block_start_pe(i) = block_start_pe(i) - i + npe_extra_st
                    num_blocks_pe(i) = num_blocks_pe(i) - 1
                    sum_num = 0
                    lml_temp = block_start_pe(i) - 1
                    do j = 1, num_blocks_pe(i)
                       sum_num = sum_num + biased_states_per_LML_block(lml_temp + j)
                    end do   
                    biased_tot_pe(i) = sum_num
                 end do
                 do i = pe_num, no_of_inner_region_pes
                    block_start_pe(i) = block_start_pe(i-1) + num_blocks_pe(i-1)
                    num_blocks_pe(i) = 1
                    lml_temp = block_start_pe(i)
                    biased_tot_pe(i) = biased_states_per_LML_block(lml_temp)
                 end do   
                 exit    
              end if
           end if
           if (inner_region_rank == 0) print *, 'number_to_divide_by set to:', number_to_divide_by 
!           if (number_to_divide_by > (no_of_inner_region_pes + (no_of_inner_region_pes / 5))) &
! number_to_divide_by criterion relaxed here to allow more flexible inner region pe transformations:
! this means that more preliminary tests can (should) be carried out.           
           if (number_to_divide_by > (no_of_inner_region_pes + (no_of_inner_region_pes / 1))) then
              if (inner_region_rank < 2 .or. MOD(inner_region_rank, 500) == 0) &
                   print *, 'lml_temp2, no_of_LML_blocks, number_to_divide_by, pe_num, no_of_inner_region_pes =', &
              lml_temp2, no_of_LML_blocks, number_to_divide_by, pe_num, no_of_inner_region_pes
              call assert (.false., 'number_to_divide_by is too large, think again')
           end if
           cycle
        else
           if (inner_region_rank == 0) then
              print *, 'pe_num, lml_temp, sum_num, temp_sum, number_to_divide_by =', &
                   pe_num, lml_temp, sum_num, temp_sum, number_to_divide_by
              num = no_of_inner_region_pes / 10
! block_start_pe:
              do i = 1, num
                 ist = (i-1) * 10 + 1
                 isf = (i-1) * 10 + 10
                 print *, 'ist, isf =', ist, isf, ', Block_start_pe(ist:isf) =', Block_start_pe(ist:isf) 
              end do
              if (num * 10 < no_of_inner_region_pes) then
                 ist = num* 10 + 1
                 isf = no_of_inner_region_pes
                 print *, 'ist, isf =', ist, isf, ', Block_start_pe(ist:isf) =', Block_start_pe(ist:isf) 
              end if   
! num_blocks_pe:
              do i = 1, num
                 ist = (i-1) * 10 + 1
                 isf = (i-1) * 10 + 10
                 print *, 'ist, isf =', ist, isf, ', num_blocks_pe(ist:isf) =', num_blocks_pe(ist:isf) 
              end do
              if (num * 10 < no_of_inner_region_pes) then
                 ist = num* 10 + 1
                 isf = no_of_inner_region_pes
                 print *, 'ist, isf =', ist, isf, ', num_blocks_pe(ist:isf) =', num_blocks_pe(ist:isf) 
              end if   
! biased_tot_pe:        
              do i = 1, num
                 ist = (i-1) * 10 + 1
                 isf = (i-1) * 10 + 10
                 print *, 'ist, isf =', ist, isf, ', biased_tot_pe(ist:isf) =', biased_tot_pe(ist:isf) 
              end do
              if (num * 10 < no_of_inner_region_pes) then
                 ist = num* 10 + 1
                 isf = no_of_inner_region_pes
                 print *, 'ist, isf =', ist, isf, ', biased_tot_pe(ist:isf) =', biased_tot_pe(ist:isf) 
              end if   
           end if   
           call assert (.false., 'LML to pe assignment problem at final LML')
        end if
        
        end do  ! backup cycle to get the right numbers
        if (SUM(num_blocks_pe(1:no_of_inner_region_pes)) /= no_of_LML_blocks) then
           print *, 'Whoops, blocks_per_pe error, num_blocks_pe =', &
      &                              num_blocks_pe(1:no_of_inner_region_pes)
           call assert (.false., 'LML to pe assignment problem')
        else if (SUM(biased_tot_pe(1:no_of_inner_region_pes)) /= SUM(biased_states_per_LML_block)) then
           if (inner_region_rank < 1 .or. MOD(inner_region_rank, 800) == 0) then 
                print *, 'Whoops, biased_tot_pe error, biased_tot_pe =', &
                     &                              biased_tot_pe(1:no_of_inner_region_pes)
                print *, 'Whoops, SUM(biased_tot_pe(1:no_of_inner_region_pes)), SUM(biased_states_per_LML_block) =', &
                     SUM(biased_tot_pe(1:no_of_inner_region_pes)), SUM(biased_states_per_LML_block)
           end if     
!  forced stoppage cancelled out here: it need not be fatal
           !                call assert (.false., 'biased_tot_pe to pe assignment problem')
        else if (inner_region_rank == 0) then
           num = no_of_inner_region_pes / 10
! block_start_pe:
           do i = 1, num
              ist = (i-1) * 10 + 1
              isf = (i-1) * 10 + 10
              print *, 'ist, isf =', ist, isf, ', Block_start_pe(ist:isf) =', Block_start_pe(ist:isf) 
           end do
           if (num * 10 < no_of_inner_region_pes) then
              ist = num* 10 + 1
              isf = no_of_inner_region_pes
              print *, 'ist, isf =', ist, isf, ', Block_start_pe(ist:isf) =', Block_start_pe(ist:isf) 
           end if   
! num_blocks_pe:
           do i = 1, num
              ist = (i-1) * 10 + 1
              isf = (i-1) * 10 + 10
              print *, 'ist, isf =', ist, isf, ', num_blocks_pe(ist:isf) =', num_blocks_pe(ist:isf) 
           end do
           if (num * 10 < no_of_inner_region_pes) then
              ist = num* 10 + 1
              isf = no_of_inner_region_pes
              print *, 'ist, isf =', ist, isf, ', num_blocks_pe(ist:isf) =', num_blocks_pe(ist:isf) 
           end if   
! biased_tot_pe:        
           do i = 1, num
              ist = (i-1) * 10 + 1
              isf = (i-1) * 10 + 10
              print *, 'ist, isf =', ist, isf, ', biased_tot_pe(ist:isf) =', biased_tot_pe(ist:isf) 
           end do
           if (num * 10 < no_of_inner_region_pes) then
              ist = num* 10 + 1
              isf = no_of_inner_region_pes
              print *, 'ist, isf =', ist, isf, ', biased_tot_pe(ist:isf) =', biased_tot_pe(ist:isf) 
           end if   
        end if
        if ((ABS(biased_tot_pe(no_of_inner_region_pes) - biased_average) / biased_average) > 0.3) then
           if (inner_region_rank == 0) print *, &
                'for final_pe, possibly too large a difference fron biased average =', biased_average, &
                ', we are allowing it if final_pe is underperforming compared to the others,',&
                ' but not the other way round. Consider (small) modifications to the number of pes.'
          
!           if (biased_tot_pe(no_of_inner_region_pes) - biased_average > 0.0) call assert (.false., &
!                'problem with assignment of LML blocks to pes, try adding or subtracting 1 pe')
        end if   

        if (SUM(num_blocks_pe(1:no_of_inner_region_pes))/= no_of_LML_blocks) then
           print *, 'Whoops, blocks_per_pe error, num_blocks_pe =', &
      &                              num_blocks_pe(1:no_of_inner_region_pes)
           CALL assert(.false., 'num_blocks_pe do not add up to  no_of_LML_blocks')
        end if    

        my_starting_block = block_start_pe(inner_region_rank+1)
        my_num_LML_blocks = num_blocks_pe(inner_region_rank+1)
        IF (i_am_inner_master) THEN   ! provide details for reform to read inner wavefunction files
           OPEN (newunit=my_unit, file='reform_inner_blocks_parallel_info', FORM='UNFORMATTED')
           write(my_unit) no_of_inner_region_pes
           write(my_unit) block_start_pe(1:no_of_inner_region_pes)
           write(my_unit) num_blocks_pe(1:no_of_inner_region_pes)
           CLOSE (my_unit)
           ! after the program has finished, this file needs to be moved to state/ground to run reform
        END IF   
        max_num_LML_blocks = MAXVAL(num_blocks_pe(1:no_of_inner_region_pes))
        allocate (my_LML_blocks(my_num_LML_blocks), stat=err)
        CALL assert(err .EQ. 0, 'setup_Lblock_communicator_size_p')
        do i = 1, my_num_LML_blocks  
           my_LML_blocks(i) = my_starting_block - 1 + i
        end do           
        
        CALL assert(SUM(Lb_comm_size) >= no_of_inner_region_pes, 'Number of processors assigned to each LML block is incorrect.')

        IF (i_am_inner_master .AND. debug) THEN
            PRINT *, 'LML block Communicator size'
            DO L = 1, no_of_LML_blocks              ! DDAC: NOTE THAT L NO LONGER ASSUMES THE VALUES OF THE ANGULAR MOMENTA
                PRINT *, L, Lb_comm_size(L)         ! AND IS SIMPLY AN INDEX
            END DO
        END IF

    END SUBROUTINE setup_Lblock_communicator_size_pn
    SUBROUTINE setup_Lblock_communicator_size_pt(i_am_inner_master, states_per_LML_block)

        USE initial_conditions, ONLY : debug

        LOGICAL, INTENT(IN) :: i_am_inner_master
        ! ACB: hugo fix for bug in inner block allocation
        INTEGER, INTENT(IN) :: states_per_LML_block(-1:no_of_LML_blocks)

        INTEGER :: no_of_inner_region_pes
        INTEGER :: err, L, Iterations
        INTEGER :: states_per_sub_block(no_of_LML_blocks)
        INTEGER :: biased_states_per_LML_block(no_of_LML_blocks)
        INTEGER :: max_sub_block_LML(1), no_of_remaining_pes
        REAL    :: biased_average, test, test2, test3, diff_below, diff_above, temp_sum, num_sum, &
                   biased_tot_average
        INTEGER :: lml, i, sum_num, pe_num, pe_numm, lml_temp, lml_multi, i_last, my_starting_block
        INTEGER, allocatable :: biased_tot_pe(:)
        integer  ::   num, ist, isf, my_unit


        
        CALL MPI_COMM_SIZE(mpi_comm_region, no_of_inner_region_pes, err)
        ALLOCATE (num_blocks_pe(no_of_inner_region_pes), &
                  block_start_pe(no_of_inner_region_pes), biased_tot_pe(no_of_inner_region_pes), stat=err)
        CALL assert(err .EQ. 0, 'allocation error in Setup_Lblock_Communicator_Size')
        
        LML_pe_method = 1 
!        IF (no_of_inner_region_pes < no_of_LML_blocks) THEN
!            PRINT *, 'no_of_inner_region_pes = ', no_of_inner_region_pes
!            PRINT *, 'Number of LML blocks     = ', no_of_LML_blocks
!            PRINT *, 'Must have no_of_inner_region_pes >= no_of_LML_blocks'
!            PRINT *, 'Entering new code for more than 1 LML block per pe'
!            LML_pe_method = 1
!        END IF

        Lb_comm_size = 1
        biased_states_per_LML_block = NINT((DBLE(states_per_LML_block(1:no_of_LML_blocks)))**1.8)
        ! Bias the allocation by pretending we had a different no of states per block.
        ! Perfect parallelism: would want States_Per_Sub_Block = states_per_LML_block**2

        no_of_remaining_pes = no_of_inner_region_pes - no_of_LML_blocks


        ! note no need for wp here
        pes_per_lblock(:) = 1
        lb_comm_size(:) = 1
        If (no_of_inner_region_pes == 13 .and. no_of_LML_blocks == 17) then  
           block_start_pe(1) = 1
           num_blocks_pe(1) = 2
           block_start_pe(2) = 3
           num_blocks_pe(2) = 2
           do i = 3, 10
              block_start_pe(i) = 2 + i
              num_blocks_pe(i) = 1
           end do
           block_start_pe(11) = 13
           num_blocks_pe(11) = 2
           block_start_pe(12) = 15
           num_blocks_pe(12) = 2
           block_start_pe(13) = 17
           num_blocks_pe(13) = 1
        else if (no_of_LML_blocks == 169 .and. no_of_inner_region_pes == 83) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 4
           block_start_pe(2) = 5
           num_blocks_pe(2) = 3
           block_start_pe(3) = 8
           num_blocks_pe(3) = 2
           do i = 4, 83
              num_blocks_pe(i) = 2
              block_start_pe(i) = block_start_pe(i-1) + 2
           end do
        else if (no_of_LML_blocks == 169 .and. no_of_inner_region_pes == 43) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 4
           block_start_pe(2) = 5
           num_blocks_pe(2) = 4
           do i = 3, 41
              num_blocks_pe(i) = 4
              block_start_pe(i) = block_start_pe(i-1) + 4
           end do
           num_blocks_pe(42) = 3
           block_start_pe(42) = block_start_pe(41) + 4
           num_blocks_pe(43) = 2
           block_start_pe(43) = block_start_pe(42) + 3
        else if (no_of_LML_blocks == 196 .and. no_of_inner_region_pes == 97) then
           num_blocks_pe(1) = 2
           block_start_pe(1) = 1
           do i = 2, 4
              num_blocks_pe(i) = 2
              block_start_pe(i) = block_start_pe(i-1) + 2
           end do   
           num_blocks_pe(5) = 3
           block_start_pe(5) = 9
           num_blocks_pe(6) = 2
           block_start_pe(6) = 12
           do i = 7, 10
              num_blocks_pe(i) = 2
              block_start_pe(i) = block_start_pe(i-1) + 2
           end do   
           num_blocks_pe(11) = 3
           block_start_pe(11) = 22
           num_blocks_pe(12) = 2
           block_start_pe(12) = 25
           do i = 13, 97
              num_blocks_pe(i) = 2
              block_start_pe(i) = block_start_pe(i-1) + 2
           end do   
        else if (no_of_LML_blocks == 196 .and. no_of_inner_region_pes == 64) then
           num_blocks_pe(1) = 3
           block_start_pe(1) = 1
           do i = 2, 52
              num_blocks_pe(i) = 3
              block_start_pe(i) = block_start_pe(i-1) + 3
           end do   
           num_blocks_pe(53) = 4
           block_start_pe(53) = block_start_pe(52) + 3
           num_blocks_pe(54) = 4
           block_start_pe(54) = block_start_pe(53) + 4
           num_blocks_pe(55) = 5
           block_start_pe(55) = block_start_pe(54) + 4
           num_blocks_pe(56) = 3
           block_start_pe(56) = block_start_pe(55) + 5
           do i = 57, 64
              num_blocks_pe(i) = 3
              block_start_pe(i) = block_start_pe(i-1) + 3
           end do   
              
        else if (no_of_LML_blocks == 31 .and. no_of_inner_region_pes == 15) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 3
           block_start_pe(2) = 4
           num_blocks_pe(2) = 2
           do i = 3, no_of_inner_region_pes
              block_start_pe(i) = block_start_pe(i-1) + 2
              num_blocks_pe(i) = 2
           end do
        else if (no_of_LML_blocks == 10 .and. no_of_inner_region_pes == 9) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 2
           block_start_pe(2) = 3
           num_blocks_pe(2) = 1
           do i = 3, no_of_inner_region_pes
              block_start_pe(i) = block_start_pe(i-1) + 1
              num_blocks_pe(i) = 1
           end do
        else if (no_of_LML_blocks == 10 .and. no_of_inner_region_pes == 3) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 4
           block_start_pe(2) = 5
           num_blocks_pe(2) = 3
           do i = 3, no_of_inner_region_pes
              block_start_pe(i) = block_start_pe(i-1) + 3
              num_blocks_pe(i) = 3
           end do
        else if (no_of_LML_blocks == 71 .and. no_of_inner_region_pes == 41) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 2
           block_start_pe(2) = 3
           num_blocks_pe(2) = 2
           block_start_pe(3) = 5
           num_blocks_pe(3) = 1
           do i = 4, 10
              block_start_pe(i) = block_start_pe(i-1) + 1
              num_blocks_pe(i) = 1
           end do
           block_start_pe(11) = 13
           num_blocks_pe(11) = 2
           do i = 12, 16
              block_start_pe(i) = block_start_pe(i-1) + 2
              num_blocks_pe(i) = 2
           end do
           block_start_pe(17) = 25
           num_blocks_pe(17) = 1
           do i = 18, 23
              block_start_pe(i) = block_start_pe(i-1) + 1
              num_blocks_pe(i) = 1
           end do
           block_start_pe(24) = 32
           num_blocks_pe(24) = 2
           do i = 25, 32
              block_start_pe(i) = block_start_pe(i-1) + 2
              num_blocks_pe(i) = 2
           end do
           block_start_pe(33) = 50
           num_blocks_pe(33) = 3
           do i = 34, 36
              block_start_pe(i) = block_start_pe(i-1) + 3
              num_blocks_pe(i) = 3
           end do
           block_start_pe(37) = 62
           num_blocks_pe(37) = 2
           do i = 38, 41
              block_start_pe(i) = block_start_pe(i-1) + 2
              num_blocks_pe(i) = 2
           end do
           
        else if (no_of_inner_region_pes == 1920) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 2
           do i = 2, 1699
              block_start_pe(i) = block_start_pe(i-1) + 2
              num_blocks_pe(i) = 2
           end do
           block_start_pe(1700) = block_start_pe(1699) + 2
           num_blocks_pe(1700) = 1
           do i = 1701, 1920
              block_start_pe(i) = block_start_pe(i-1) + 1
              num_blocks_pe(i) = 1
           end do
           
        else if (no_of_inner_region_pes == 1280) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 3
           do i = 2, 1059
              block_start_pe(i) = block_start_pe(i-1) + 3
              num_blocks_pe(i) = 3
           end do
           block_start_pe(1060) = block_start_pe(1059) + 3
           num_blocks_pe(1060) = 2
           do i = 1061, 1280
              block_start_pe(i) = block_start_pe(i-1) + 2
              num_blocks_pe(i) = 2
           end do
           
        else if (no_of_inner_region_pes == 640) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 6
           do i = 2, 419
              block_start_pe(i) = block_start_pe(i-1) + 6
              num_blocks_pe(i) = 6
           end do
           block_start_pe(420) = block_start_pe(419) + 6
           num_blocks_pe(420) = 5
           do i = 421, 640
              block_start_pe(i) = block_start_pe(i-1) + 5
              num_blocks_pe(i) = 5
           end do
           
        else if (no_of_inner_region_pes == 320) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = 12
           do i = 2, 99
              block_start_pe(i) = block_start_pe(i-1) + 12
              num_blocks_pe(i) = 12
           end do
           block_start_pe(100) = block_start_pe(99) + 12
           num_blocks_pe(100) = 11
           do i = 101, 320
              block_start_pe(i) = block_start_pe(i-1) + 11
              num_blocks_pe(i) = 11
           end do
           
        else if (no_of_inner_region_pes == 4) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = no_of_LML_blocks / 4
           block_start_pe(2) = num_blocks_pe(1) + 1
           num_blocks_pe(2) = no_of_LML_blocks/2 - num_blocks_pe(1)
           block_start_pe(3) = block_start_pe(2) + num_blocks_pe(2)
           num_blocks_pe(3) = (3* no_of_LML_blocks)/4 +1 - block_start_pe(3) 
           block_start_pe(4) = block_start_pe(3) + num_blocks_pe(3)
           num_blocks_pe(4) = no_of_LML_blocks +1 - block_start_pe(4) 
        else if (no_of_inner_region_pes == 2) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = no_of_LML_blocks / 2
           block_start_pe(2) = num_blocks_pe(1) + 1
           num_blocks_pe(2) = no_of_LML_blocks - num_blocks_pe(1)
        else if (no_of_inner_region_pes == 1) then
           block_start_pe(1) = 1
           num_blocks_pe(1) = no_of_LML_blocks
        end if   
        do i = 1, no_of_inner_region_pes
           ist = block_start_pe(i)
           isf = ist + num_blocks_pe(i) - 1
           biased_tot_pe(i) = SUM(biased_states_per_LML_block(ist:isf))
        end do   
        biased_average = REAL(SUM(biased_states_per_LML_block)) / REAL(no_of_inner_region_pes) 

        if (inner_region_rank == 0) then
           num = no_of_inner_region_pes / 10
! block_start_pe:
           do i = 1, num
              ist = (i-1) * 10 + 1
              isf = (i-1) * 10 + 10
              print *, 'ist, isf =', ist, isf, ', Block_start_pe(ist:isf) =', Block_start_pe(ist:isf) 
           end do
           if (num * 10 < no_of_inner_region_pes) then
              ist = num* 10 + 1
              isf = no_of_inner_region_pes
              print *, 'ist, isf =', ist, isf, ', Block_start_pe(ist:isf) =', Block_start_pe(ist:isf) 
           end if   
! num_blocks_pe:
           do i = 1, num
              ist = (i-1) * 10 + 1
              isf = (i-1) * 10 + 10
              print *, 'ist, isf =', ist, isf, ', num_blocks_pe(ist:isf) =', num_blocks_pe(ist:isf) 
           end do
           if (num * 10 < no_of_inner_region_pes) then
              ist = num* 10 + 1
              isf = no_of_inner_region_pes
              print *, 'ist, isf =', ist, isf, ', num_blocks_pe(ist:isf) =', num_blocks_pe(ist:isf) 
           end if   
! biased_tot_pe:        
           do i = 1, num
              ist = (i-1) * 10 + 1
              isf = (i-1) * 10 + 10
              print *, 'ist, isf =', ist, isf, ', biased_tot_pe(ist:isf) =', biased_tot_pe(ist:isf) 
           end do
           if (num * 10 < no_of_inner_region_pes) then
              ist = num* 10 + 1
              isf = no_of_inner_region_pes
              print *, 'ist, isf =', ist, isf, ', biased_tot_pe(ist:isf) =', biased_tot_pe(ist:isf) 
           end if   
        end if
        if ((ABS(biased_tot_pe(no_of_inner_region_pes) - biased_average) / biased_average) > 0.3) then
           if (inner_region_rank == 0) print *, 'for final_pe, possibly too large a difference fron biased average =', &
                biased_average, ', we are allowing it as this is a manual selection of pes.'
          
        end if   
        
        if (SUM(num_blocks_pe(1:no_of_inner_region_pes))/= no_of_LML_blocks) then
           print *, 'Whoops, blocks_per_pe error, num_blocks_pe =', &
      &                              num_blocks_pe(1:no_of_inner_region_pes)
        end if    

        my_starting_block = block_start_pe(inner_region_rank+1)
        my_num_LML_blocks = num_blocks_pe(inner_region_rank+1)
        IF (i_am_inner_master) THEN   ! provide details for reform to read inner wavefunction files
           OPEN (newunit=my_unit, file='reform_inner_blocks_parallel_info', FORM='UNFORMATTED')
           write(my_unit) no_of_inner_region_pes
           write(my_unit) block_start_pe(1:no_of_inner_region_pes)
           write(my_unit) num_blocks_pe(1:no_of_inner_region_pes)
           CLOSE (my_unit)
        END IF   
        max_num_LML_blocks = MAXVAL(num_blocks_pe(1:no_of_inner_region_pes))
        allocate (my_LML_blocks(my_num_LML_blocks), stat=err)
        CALL assert(err .EQ. 0, 'setup_Lblock_communicator_size_p')
        do i = 1, my_num_LML_blocks  
           my_LML_blocks(i) = my_starting_block - 1 + i
        end do           
        
        CALL assert(SUM(Lb_comm_size) >= no_of_inner_region_pes, 'Number of processors assigned to each LML block is incorrect.')

        IF (i_am_inner_master .AND. debug) THEN
            PRINT *, 'LML block Communicator size'
            DO L = 1, no_of_LML_blocks              ! DDAC: NOTE THAT L NO LONGER ASSUMES THE VALUES OF THE ANGULAR MOMENTA
                PRINT *, L, Lb_comm_size(L)         ! AND IS SIMPLY AN INDEX
            END DO
         END IF
               PRINT *, 'I am', inner_region_rank, 'my_starting_block, my_num_LML_blocks =', my_starting_block, my_num_LML_blocks

         
    END SUBROUTINE setup_Lblock_communicator_size_pt
    
!-----------------------------------------------------------------------
! SET UP COLORS FOR EACH COMMUNICATOR                         
!-----------------------------------------------------------------------

    SUBROUTINE setup_colors_for_Lblock_comms(my_ang_mom_id, states_per_LML_block)     !DDAC: LMLblock_Comms

        USE initial_conditions, ONLY: gs_finast, debug

        IMPLICIT NONE

        INTEGER, INTENT(INOUT) :: my_ang_mom_id ! lowest LML associated with pe
        INTEGER, INTENT(IN)    :: states_per_LML_block(-1:no_of_LML_blocks)
        INTEGER :: rsum, i, j, err, no_of_inner_region_pes

        i_have_gs_L = .false.
        i_am_gs_master = .false.

        IF (LML_pe_method == 1) then
           call setup_colors_for_Lblock_comms_p(my_ang_mom_id, states_per_LML_block)
           return
        END IF

        Lb_color = 0
        rsum = 0

        CALL MPI_COMM_SIZE(mpi_comm_region, no_of_inner_region_pes, err)
        allocate (my_LML_blocks(1), num_blocks_pe(no_of_inner_region_pes), block_start_pe(no_of_inner_region_pes), stat=err)
        ! Each core works with a block of given symmetry
        ! Set Lb_color = i, where i is the ID of the block symmetry
        my_ground_state_block = -9999 !local count, error value, to be set to 1 when true as 1 lml block per pe
        my_ground_state_row = -9999 !local count, error value, to be set to 1 when true as 1 lml block per p        
        DO i = 1, no_of_LML_blocks
            pes_per_Lblock(i) = Lb_comm_size(i) 
            DO j = 1, Lb_comm_size(i)
                rsum = rsum + 1
                block_start_pe(rsum) = i
                num_blocks_pe(rsum) = 1
                IF ((inner_region_rank + 1) == rsum) THEN
                    Lb_color = i          !Lblock Color

                    IF (debug) PRINT *, 'i =', i, ' j =', j, ' Lb_color =', Lb_color

                    IF (i .EQ. gs_finast) THEN
                        i_have_gs_L = .true.
                        my_ground_state_block = 1
                        my_ground_state_row = 1 ! This assumes the 'gs' is the lowest energy state.
                        !note this could be mofified for more flexibility in state choice 
                    END IF

                END IF
            END DO
        END DO
        IF (.NOT.(i_have_gs_L)) my_ground_state_row = 1
        ! dummy variable used in wavefunction:inner_wavefunction_update, then ignored  
        
        my_ang_mom_id = Lb_color
        my_LML_block_id = Lb_color
        my_num_LML_blocks = 1
        max_num_LML_blocks = 1
        CALL assert(err .EQ. 0, 'setup_colors_for_Lblock_comms')
        my_LML_blocks(1) = Lb_color

        CALL createcomm_order_Lblock(inner_region_rank, Lb_color)

        if (i_am_block_master) then
           allocate(master_for_LML_blocks(no_of_LML_blocks), LML_blocks_per_Lb_master(Lb_m_size), stat=err)
           call assert(err == 0, 'problem allocating master_for_LML_blocks')
           master_for_LML_blocks(:) = -999
           do i = 1, no_of_LML_blocks
              master_for_LML_blocks(i) = i - 1
           end do   
           LML_blocks_per_Lb_master(:) = 1
        end if

     END SUBROUTINE setup_colors_for_Lblock_comms
    SUBROUTINE setup_colors_for_Lblock_comms_p(my_ang_mom_id, states_per_LML_block)     !DDAC: LMLblock_Comms

        USE initial_conditions, ONLY: gs_finast, debug

        IMPLICIT NONE

        INTEGER, INTENT(INOUT) :: my_ang_mom_id ! lowest LML associated with pe
        INTEGER, INTENT(IN)    :: states_per_LML_block(-1:no_of_LML_blocks)
        INTEGER :: rsum, i, j, err, Lb_m_size, sum_rows, LML_block_count
        INTEGER, allocatable   :: my_id_for_my_blocks(:), dummy(:) 
        
        i_have_gs_L = .false.
        i_am_gs_master = .false.

        lb_color = my_LML_blocks(1)
!        IF (debug) PRINT *, 'i =', i, ' j =', j, ' Lb_color =', Lb_color
        my_ground_state_block = -9999  ! local count, error value, to be set when true 
        my_ground_state_row = -9999 !local count, error value, to be set to 1 when true as 1 lml block per p  
        if (gs_finast >= lb_color .and. gs_finast <= my_LML_blocks(my_num_LML_blocks)) then
            i_have_gs_L = .true.
            sum_rows = 0 
            DO i = 1, my_num_LML_blocks
               LML_block_count = my_LML_blocks(i)
               IF (gs_finast == LML_block_count) THEN
                  my_ground_state_block = i   ! local index
                  my_ground_state_row = sum_rows + 1   ! first row of the correct LML block  
                  exit
               END IF
               sum_rows = sum_rows + states_per_LML_block(LML_block_count) 
            END DO   
            IF (debug) PRINT *, 'Process ', inner_region_rank, 'has GS L'
        ELSE
            my_ground_state_row = 1    
            ! dummy variable used in wavefunction:inner_wavefunction_update, then ignored  
        END IF

        my_ang_mom_id = Lb_color
        my_LML_block_id = Lb_color
!        print *, 'colours_p, inner_region_rank =', inner_region_rank, &
!             ', and my_ang_mom_id, my_num_LML_blocks =', my_ang_mom_id, my_num_LML_blocks        
        CALL createcomm_order_Lblock(inner_region_rank, Lb_color)

        if (i_am_block_master) then
           call MPI_COMM_SIZE(Lb_m_comm, Lb_m_size, err)
           allocate(master_for_LML_blocks(no_of_LML_blocks), my_id_for_my_blocks(my_num_LML_blocks), &
                    LML_blocks_per_Lb_master(Lb_m_size), dummy(Lb_m_size), stat=err)
           call assert(err == 0, 'problem allocating master_for_LML_blocks')
           master_for_LML_blocks(:) = -999
           do i = 1, my_num_LML_blocks
              my_id_for_my_blocks(i) = Lb_m_rank
           end do   
           call MPI_ALLGATHER (my_num_LML_blocks, 1, MPI_INTEGER, LML_blocks_per_Lb_master, 1, &
                               MPI_INTEGER, Lb_m_comm, err)
           call assert(err == 0, 'MPI_ALLGATHER for LML_blocks_per_Lb_master')
           print *, 'Lb_m_rank =', lb_m_rank, 'LML_blocks_per_Lb_master =', LML_blocks_per_Lb_master
           dummy(1) = 0
           do i = 2, lb_m_size
             dummy(i) = dummy(i-1) + LML_blocks_per_Lb_master(i-1)
           end do
!          print *, 'Lb_m_rank =', lb_m_rank, 'shifts for allgatherv =', dummy
!           if (lb_m_rank > 0) print *, 'Lb_m_rank =', lb_m_rank, 'SUM shifts for allgatherv =', &
!                                       SUM(LML_blocks_per_Lb_master(1:lb_m_rank))
           call MPI_ALLGATHERV (my_id_for_my_blocks, my_num_LML_blocks, MPI_INTEGER, &
                master_for_LML_blocks, LML_blocks_per_Lb_master, &
                dummy, MPI_INTEGER, Lb_m_comm, err)  
!                LML_blocks_per_Lb_master - LML_blocks_per_Lb_master(1), MPI_INTEGER, Lb_m_comm, err)  
           call assert(err == 0, 'MPI_ALLGATHERV for master_for_LML_blocks')
           deallocate(my_id_for_my_blocks, dummy, stat=err)
           call assert(err == 0, 'deallocation error, for my_id_for_my_blocks')
           print *, 'Lb_m_rank =', lb_m_rank, 'master_for_LML_blocks =', master_for_LML_blocks
        end if
        
    END SUBROUTINE setup_colors_for_Lblock_comms_p

     
!-----------------------------------------------------------------------
! SPLIT MPI_COMM_WORLD                               
!-----------------------------------------------------------------------

    SUBROUTINE createcomm_order_Lblock(inner_region_rank, Lb_color)

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: inner_region_rank, Lb_color
        INTEGER :: ierr

        ! Create a communicator to contain all cores working with same symmetry block
        ! (i.e. mpi_split mpi_comm_region)
        CALL MPI_COMM_SPLIT(mpi_comm_region, Lb_color, inner_region_rank, Lb_comm, ierr)
        CALL MPI_COMM_SIZE(Lb_comm, Lb_size, ierr)
        CALL MPI_COMM_RANK(Lb_comm, Lb_rank, ierr)

        CALL createcomm_order_Lblock_masters(Lb_rank, inner_region_rank)

    END SUBROUTINE createcomm_order_Lblock

!-----------------------------------------------------------------------
! GROUP THE MASTER NODES OF EACH BLOCK COMMUNICATOR INTO A 
! MASTER COMMUNICATOR                                          
!-----------------------------------------------------------------------

    SUBROUTINE createcomm_order_Lblock_masters(Lb_rank, inner_region_rank)
        USE initial_conditions, ONLY : debug

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: Lb_rank, inner_region_rank
        INTEGER :: ierr

        i_am_block_master = .false.

        Lb_master_color = 0

        IF (Lb_rank == 0) THEN
            Lb_master_color = no_of_LML_blocks + 1
            Lb_master_rank = inner_region_rank
            i_am_block_master = .true.
            IF (i_have_gs_L) i_am_gs_master = .true.
        END IF

        ! Lb_m_comm contains all LML block masters that handle the same propagation orders
        ! i.e. the mpi_split is performed on ocomm : now on mpi_comm_region as spltype is removed
        CALL MPI_COMM_SPLIT(mpi_comm_region, Lb_master_color, Lb_master_rank, &
                            Lb_m_comm, ierr)
        CALL MPI_COMM_SIZE(Lb_m_comm, Lb_m_size, ierr)
        CALL MPI_COMM_RANK(Lb_m_comm, Lb_m_rank, ierr)

        IF (i_am_block_master .AND. debug) THEN
            PRINT *, 'in_reg_rank', inner_region_rank, 'lb_rank, Lb_master_rank =', lb_rank, Lb_master_rank
            PRINT *, 'in_reg_rank', inner_region_rank, 'Lb_m_rank', Lb_m_rank, 'Lb_m_size', Lb_m_size
        END IF

    END SUBROUTINE createcomm_order_Lblock_masters

!-----------------------------------------------------------------------
! DEALLOCATIONS                                             
!-----------------------------------------------------------------------

    SUBROUTINE dealloc_setup_Lblock_comm_size

        IMPLICIT NONE

        INTEGER :: err

        DEALLOCATE (Lb_comm_size, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with setup_Lblock_communicator_size')

    END SUBROUTINE dealloc_setup_Lblock_comm_size

END MODULE mpi_layer_lblocks
