! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!
!> @ingroup source
!> @brief Handles the numerical portion of output/input file names.

MODULE file_num

    IMPLICIT NONE

    PUBLIC file_number

CONTAINS

    FUNCTION file_number(istep, nstep)

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: istep, nstep

        INTEGER :: width
        CHARACTER (LEN=6) :: index_format
        CHARACTER (LEN=:), ALLOCATABLE :: file_number

        SELECT CASE  (nstep)
            CASE (0);  width = 1
            CASE (1:); width = 1 + INT(LOG10(REAL(nstep)))
        END SELECT 

        width = MAX(width,4)
        ALLOCATE (CHARACTER(LEN=width) :: file_number)

        WRITE (index_format, '("(I",I1,".",I1,")")') width, width
        WRITE (file_number, index_format) istep

    END FUNCTION file_number

END MODULE file_num
