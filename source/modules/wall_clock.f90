! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles timing of calculation, including writing out timing files.

MODULE wall_clock

    USE precisn,            ONLY: wp, longint
    USE io_parameters,      ONLY: version
    USE initial_conditions, ONLY: disk_path

    IMPLICIT NONE

    INTEGER(longint), PRIVATE :: clok_iteration_start

    REAL(wp) :: time_start_inner, time_end_inner
    REAL(wp) :: time_diff_inner, sum_wait_time_inner

    REAL(wp) :: time1, time2, time3, time4, time5, time6, time7
    REAL(wp) :: sum_time_bndries, sum_time_surf, sum_time_arnoldi, sum_time_sendtoinner
    REAL(wp) :: sum_time_firstpe, sum_time_setup, sum_time_wait
    REAL(wp) :: sum_time_bound, sum_time_diag, sum_time_gram, sum_time_trans
    REAL(wp) :: program_start_time

    REAL(wp) :: sum_time_fp_share, time_share, time_share2, sum_time_ew_share, sum_time_start_psi, &
                time_eshare, time_eshare2
    REAL(wp) :: sum_time_test1, sum_time_test2, time_s, time_b, sum_time_test1b
    REAL(wp) :: timeg1, timeg2, timeg3, timeg4, timeg5, timeg6
    REAL(wp) :: sum_time_g1, sum_time_g2, sum_time_g3, sum_time_g4, sum_time_g5, sum_time_g6
    REAL(wp) :: timea1, timea2, timea3, timea4, timea5, timea6
    REAL(wp) :: sum_time_a0, sum_time_a1, sum_time_a2, sum_time_a3, sum_time_a4, sum_time_a5

  CONTAINS

    REAL(wp) FUNCTION hel_time()

        INTEGER(longint) :: it0, count_rate

        it0 = 0
        count_rate = 1

        CALL SYSTEM_CLOCK(it0, count_rate)
        hel_time = REAL(it0, wp) / REAL(count_rate, wp)

    END FUNCTION hel_time

!-----------------------------------------------------------------------

    SUBROUTINE start_program_clock

        INTEGER(longint) :: clok, count_rate

        program_start_time = hel_time()
        CALL SYSTEM_CLOCK(clok, count_rate)
        clok_iteration_start = clok

    END SUBROUTINE start_program_clock

!-----------------------------------------------------------------------

    SUBROUTINE output_time(iteration_time)

        REAL(wp), INTENT(OUT) :: iteration_time
        INTEGER(longint)      :: clok_iteration_end, count_rate

        CALL SYSTEM_CLOCK(clok_iteration_end, count_rate)
        iteration_time = REAL(clok_iteration_end - clok_iteration_start, wp) / REAL(count_rate, wp)
        clok_iteration_start = clok_iteration_end

    END SUBROUTINE output_time

!-----------------------------------------------------------------------

    SUBROUTINE update_time_start_inner(k1)

        INTEGER, INTENT(IN) :: k1

        time_start_inner = hel_time()

        IF (k1 .EQ. -1) THEN ! reset to 0
            sum_wait_time_inner = 0.0_wp
            sum_time_arnoldi = 0.0_wp
            sum_time_trans = 0.0_wp
            sum_time_gram = 0.0_wp
            sum_time_diag = 0.0_wp
            sum_time_bound = 0.0_wp
            sum_time_fp_share = 0.0_wp
            sum_time_start_psi = 0.0_wp
            sum_time_ew_share = 0.0_wp
            sum_time_test1 = 0.0_wp
            sum_time_test2 = 0.0_wp
            sum_time_test1b = 0.0_wp

            sum_time_g1 = 0.0_wp
            sum_time_g2 = 0.0_wp
            sum_time_g3 = 0.0_wp
            sum_time_g4 = 0.0_wp
            sum_time_g5 = 0.0_wp
            sum_time_g6 = 0.0_wp

            sum_time_a1 = 0.0_wp
            sum_time_a2 = 0.0_wp
            sum_time_a3 = 0.0_wp
            sum_time_a4 = 0.0_wp
            sum_time_a5 = 0.0_wp
        END IF

    END SUBROUTINE update_time_start_inner

!-----------------------------------------------------------------------

    SUBROUTINE update_start_iter

        time1 = hel_time()

    END SUBROUTINE update_start_iter
    SUBROUTINE update_start_share

        time_share = hel_time()

    END SUBROUTINE update_start_share
    SUBROUTINE update_end_share

        time_share2 = hel_time()
        sum_time_fp_share = sum_time_fp_share - time_share + time_share2

    END SUBROUTINE update_end_share
    SUBROUTINE update_start_psi

        time_share = hel_time()

    END SUBROUTINE update_start_psi
    SUBROUTINE update_end_psi

        time_share2 = hel_time()
        sum_time_start_psi = sum_time_start_psi - time_share + time_share2

    END SUBROUTINE update_end_psi

!-----------------------------------------------------------------------

    SUBROUTINE update_start_arnoldi(k1)
        INTEGER, INTENT(in)   :: k1

        time2 = hel_time()
        sum_time_bound = sum_time_bound - time1 + time2
        if (k1 /= 0) sum_time_test2 = sum_time_test2 - time_s + time2  

    END SUBROUTINE update_start_arnoldi
    SUBROUTINE update_time_test_a0

        timea1 = hel_time()

    END SUBROUTINE update_time_test_a0
    SUBROUTINE update_time_test_a1

        timea2 = hel_time()
        sum_time_a1 = sum_time_a1 - timea1 + timea2

    END SUBROUTINE update_time_test_a1
    SUBROUTINE update_time_test_a2

        timea3 = hel_time()
        sum_time_a2 = sum_time_a2 - timea2 + timea3

    END SUBROUTINE update_time_test_a2
    SUBROUTINE update_time_test_a3

        timea4 = hel_time()
        sum_time_a3 = sum_time_a3 - timea3 + timea4

    END SUBROUTINE update_time_test_a3
    SUBROUTINE update_time_test_a4

        timea5 = hel_time()
        sum_time_a4 = sum_time_a4 - timea4 + timea5

    END SUBROUTINE update_time_test_a4
    SUBROUTINE update_time_test_a5

        timea6 = hel_time()
        sum_time_a5 = sum_time_a5 - timea5 + timea6

    END SUBROUTINE update_time_test_a5

!-----------------------------------------------------------------------

    SUBROUTINE update_start_gram

        time1 = hel_time()
        sum_time_arnoldi = sum_time_arnoldi - time2 + time1

    END SUBROUTINE update_start_gram
    SUBROUTINE update_time_test_g1

        timeg1 = hel_time()
        sum_time_g1 = sum_time_g1 - time1 + timeg1

    END SUBROUTINE update_time_test_g1
    SUBROUTINE update_time_test_g2

        timeg2 = hel_time()
        sum_time_g2 = sum_time_g2 - timeg1 + timeg2

    END SUBROUTINE update_time_test_g2
    SUBROUTINE update_time_test_g3

        timeg3 = hel_time()
        sum_time_g3 = sum_time_g3 - timeg2 + timeg3

    END SUBROUTINE update_time_test_g3
    SUBROUTINE update_time_test_g4

        timeg4 = hel_time()
        sum_time_g4 = sum_time_g4 - timeg3 + timeg4

    END SUBROUTINE update_time_test_g4
    SUBROUTINE update_time_test_g5

        timeg5 = hel_time()
        sum_time_g5 = sum_time_g5 - timeg4 + timeg5

    END SUBROUTINE update_time_test_g5
    SUBROUTINE update_time_test_g6

        timeg6 = hel_time()
        sum_time_g6 = sum_time_g6 - timeg5 + timeg6

    END SUBROUTINE update_time_test_g6

!-----------------------------------------------------------------------

    SUBROUTINE update_start_trans

        time2 = hel_time()
        sum_time_gram = sum_time_gram - time1 + time2

    END SUBROUTINE update_start_trans

!-----------------------------------------------------------------------

    SUBROUTINE update_start_diag

        time3 = hel_time()
        sum_time_trans = sum_time_trans + time3 - time2
        time2 = time3

    END SUBROUTINE update_start_diag

!-----------------------------------------------------------------------

    SUBROUTINE update_end_iter

        time1 = hel_time()
        sum_time_diag = sum_time_diag + time1 - time2

    END SUBROUTINE update_end_iter

!-----------------------------------------------------------------------

    SUBROUTINE update_time_end_inner

        time_end_inner = hel_time()
        time_diff_inner = time_end_inner - time_start_inner
        sum_wait_time_inner = sum_wait_time_inner + time_diff_inner
        sum_time_bound = sum_time_bound - time_diff_inner

    END SUBROUTINE update_time_end_inner
    SUBROUTINE update_time_test

        time_s = hel_time()
!        sum_time_test1 = sum_time_test1 + time_s - time_end_inner
        sum_time_test1 = sum_time_test1 + time_s - time_b
                
    END SUBROUTINE update_time_test
    SUBROUTINE update_time_testb

        time_b = hel_time()
        sum_time_test1b = sum_time_test1b + time_b - time_end_inner
                
    END SUBROUTINE update_time_testb

!-----------------------------------------------------------------------

    SUBROUTINE open_inner_timings_file

        OPEN (UNIT=53, FILE=disk_path//'timing_inner.'//version)

        WRITE (53, *)  'These timings indicate various sections of the code (test1b, test2 test1 are', &
             ' details of the outer to inner region message passing and broadcasting for experts. '
        WRITE (53, *) 'Non-experts should concentrate on Iteration, Wait, eshare. Iteration is the inner', &
             ' iteration time, wait is the wait time within the inner region, eshare gives an idea of', &
             ' the time the inner region waits for the outer region to go through the iteration.'
        WRITE (53, *) 'The timing_outer.0 file includes complementary values of iteration and eshare', &
             ' for the outer region part of the calculation (i.e. eshare is roughly the time the outer ', &
             ' region waits for the inner region).'
        WRITE (53, *) 'Iteration ---  Wait  ---   Bound   --- test1b--- test1 --- test2 --- arnoldi', &
             ' --- gram   --- trans   --- diag   --- psi   --- fpshare   --- eshare'

        OPEN (UNIT=5553, FILE=disk_path//'timing_gram.'//version)
        WRITE (5553, *) 'These timings investigate the various detailed operations in ', &
             'gram_schmidt_orthog_lanczos (module live_communications) and are for experts only'        
        WRITE (5553, *) 'g1 ---  g2  ---   g3   --- g4   --- g5   --- g6'
        
        OPEN (UNIT=6553, FILE=disk_path//'timing_par.'//version)
        WRITE (5553, *) 'These timings investigate the various detailed operations in', &
             ' parallel_matrix_vector_multiply (module live_communications) and are for experts only'        
        WRITE (6553, *) 'a1 ---  a2  ---   a3   --- a4   --- a5'

    END SUBROUTINE open_inner_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE write_timings_inner(iteration_time)

        REAL(wp), INTENT(IN) :: Iteration_Time

        WRITE (53, &
   '(E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4,1X,E11.4)') &
              iteration_time, sum_wait_time_inner, sum_time_bound, sum_time_test1b, sum_time_test1, &
              sum_time_test2, sum_time_arnoldi, sum_time_gram, sum_time_trans, sum_time_diag, &
              sum_time_start_psi, sum_time_fp_share, sum_time_ew_share 
        FLUSH (53)

        WRITE (5553, '(5(E11.4,1x),E11.4)') &
             sum_time_g1, sum_time_g2, sum_time_g3, sum_time_g4, sum_time_g5, sum_time_g6
        FLUSH (5553)     
        WRITE (6553, '(4(E11.4,1x),E11.4)') &
             sum_time_a1, sum_time_a2, sum_time_a3, sum_time_a4, sum_time_a5
        FLUSH (6553)     

    END SUBROUTINE write_timings_inner

!-----------------------------------------------------------------------

    SUBROUTINE close_inner_timings_file

        CLOSE (53)
        CLOSE (5553)     
        CLOSE (6553)     

    END SUBROUTINE close_inner_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE open_outer0_timings_file

        OPEN (UNIT=55, FILE=disk_path//'timing_outer0.'//version)

        WRITE (55, *) 'Iteration-- Bndries --- setup --- bndries --- surf --- arnoldi', &
             ' ---  send   ---  wait   ---  fpshare   --- eshare'

    END SUBROUTINE open_outer0_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE write_timings_outer0(iteration_time)

        REAL(wp), INTENT(IN) :: iteration_time

        WRITE (55, &
            '(E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3)') &
            iteration_time, sum_time_firstpe, sum_time_setup, sum_time_bndries, sum_time_surf, &
            sum_time_arnoldi, sum_time_sendtoinner, sum_time_wait, sum_time_fp_share, sum_time_ew_share
        FLUSH (55)

    END SUBROUTINE write_timings_outer0

!-----------------------------------------------------------------------

    SUBROUTINE close_outer0_timings_file

        CLOSE (55)

    END SUBROUTINE close_outer0_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE open_outer1_timings_file

        OPEN (UNIT=54, FILE=disk_path//'timing_outer1.'//version)

        WRITE (54, *) 'Iteration ---  firstpe ----   setup  ---  bndries --- Arnoldi'

    END SUBROUTINE open_outer1_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE write_timings_outer1(iteration_time)

        REAL(wp), INTENT(IN) :: iteration_time

        WRITE (54, '(E10.3,1X,E10.3,1X,E10.3,1X,E10.3,1X,E10.3)') Iteration_Time, sum_time_firstpe, &
            sum_time_setup, sum_time_bndries, sum_time_arnoldi
        FLUSH (54)

    END SUBROUTINE write_timings_outer1

!-----------------------------------------------------------------------

    SUBROUTINE close_outer1_timings_file

        CLOSE (54)

    END SUBROUTINE close_outer1_timings_file

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time1(m)

        INTEGER, INTENT(IN) :: m

        ! reset sums of times to zero at start of each iteration
        IF (m .EQ. -1) THEN
            sum_time_bndries = 0.0_wp
            sum_time_surf = 0.0_wp
            sum_time_arnoldi = 0.0_wp
            sum_time_sendtoinner = 0.0_wp
            sum_time_wait = 0.0_wp
            sum_time_fp_share = 0.0_wp
            sum_time_ew_share = 0.0_wp
        END IF

        time7 = hel_time()

        IF (m .EQ. 0) THEN
            sum_time_setup = time7 - time1
        END IF

        time1 = time7

    END SUBROUTINE update_outer_time1

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time2

        time2 = hel_time()

        sum_time_bndries = sum_time_bndries + time2 - time1

    END SUBROUTINE update_outer_time2

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time3

        time3 = hel_time()

        sum_time_surf = sum_time_surf + time3 - time2

    END SUBROUTINE update_outer_time3

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time3_inter

        time2 = hel_time()

        sum_time_wait = sum_time_wait + time2 - time3

    END SUBROUTINE update_outer_time3_inter

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time4

        time4 = hel_time()

        sum_time_arnoldi = sum_time_arnoldi + time4 - time3

    END SUBROUTINE update_outer_time4

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time5

        time5 = hel_time()

    END SUBROUTINE update_outer_time5

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time6

        time6 = hel_time()

        sum_time_sendtoinner = sum_time_sendtoinner + time6 - time5

    END SUBROUTINE update_outer_time6

!-----------------------------------------------------------------------

    SUBROUTINE update_outer_time7

        time7 = hel_time()

        sum_time_firstpe = time7 - time1

        time1 = time7

    END SUBROUTINE update_outer_time7

!-----------------------------------------------------------------------

    SUBROUTINE write_timings_files(iteration_time, &
                                   i_am_inner_master, &
                                   i_am_outer_master, &
                                   i_am_outer_rank1)

        USE mpi_communications, ONLY : all_processor_barrier                                   
        USE initial_conditions, ONLY : use_two_inner_grid_pts

        REAL(wp), INTENT(IN) :: iteration_time
        LOGICAL, INTENT(IN)  :: i_am_inner_master
        LOGICAL, INTENT(IN)  :: i_am_outer_master
        LOGICAL, INTENT(IN)  :: i_am_outer_rank1
        REAL(wp)             :: start_wait

        start_wait = hel_time()
        call all_processor_barrier
        sum_time_wait = MERGE(hel_time() - start_wait, sum_time_wait, &
                              use_two_inner_grid_pts)

        IF (i_am_inner_master) THEN
            sum_time_ew_share = sum_time_ew_share + hel_time() - start_wait 
            CALL write_timings_inner(iteration_time)
        END IF

        IF (i_am_outer_master) THEN
            sum_time_ew_share = sum_time_ew_share + hel_time() - start_wait 
            CALL Write_Timings_Outer0(iteration_time)
        END IF

        IF (i_am_outer_rank1) THEN
            CALL write_timings_outer1(iteration_time)
        END IF

    END SUBROUTINE write_timings_files

END MODULE wall_clock
