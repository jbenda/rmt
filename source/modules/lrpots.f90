! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets up long-range potential matrices W_E, W_P and W_D for outer-region.
!> sets up matrices: WE,WD and WP
!> written by MAL Oct 2007
!> modified for CP fields by DDAC March 2017
!> modified for reduced storage by ACB Aug 2018

MODULE lrpots

    USE precisn,            ONLY: wp
    USE global_data,        ONLY: zero
    USE rmt_assert,         ONLY: assert
    USE angular_momentum,   ONLY: cg, dracah
    USE initial_conditions, ONLY: dipole_velocity_output, &
                                  RmatrixI_format_id, &
                                  RmatrixII_format_id, &
                                  dipole_format_id, &
                                  molecular_target, &
                                  LS_coupling_id,   &
                                  jK_coupling_id,   &
                                  coupling_id, &
                                  double_ionization
    USE readhd,             ONLY: ntarg, &
                                  L_block_npty, &
                                  LML_block_npty, &
                                  L_block_lrgl, &
                                  LML_block_lrgl, &
                                  LML_block_ml, &
                                  ltarg, &
                                  L_block_l2p, &
                                  LML_block_l2p, &
                                  m2p, &
                                  LML_block_nspn, &
                                  no_of_L_blocks, &
                                  no_of_LML_blocks, &
                                  L_block_tot_nchan, &
                                  LML_block_tot_nchan, &
                                  L_block_nconat, &
                                  LML_block_nconat, &
                                  crlv, &
                                  LML_block_cf, &
                                  lamax, &
                                  etarg, &
                                  crlv_v, &
                                  wp_read_store, &
                                  wd_read_store, &
                                  wd_read_store_v, &
                                  LML_wp_read_store, &
                                  LML_wd_read_store, &
                                  LML_wd_read_store_v, &
                                  L_block_nchan, &
                                  ichl,          &
                                  mat_size_wp,   &
                                  mat_size_wd,   &
                                  two_electron_nchan, &
                                  ion_l, &
                                  ion_s, &
                                  first_l, &
                                  second_l, &
                                  two_electron_l, &
                                  tot_l, &
                                  tot_ml


    IMPLICIT NONE

    REAL(wp), ALLOCATABLE, SAVE     :: cfuu(:, :, :), etuu(:)
    COMPLEX(wp), ALLOCATABLE, SAVE  :: wp2pot(:, :, :), wdpot(:, :, :)

    COMPLEX(wp), ALLOCATABLE, SAVE  :: wp1pot(:, :, :), wp2pot_v(:, :, :), wdpot_v(:, :, :)
    INTEGER, ALLOCATABLE, SAVE      :: liuu(:), miuu(:), ltuu(:), lmuu(:), lpuu(:), lsuu(:), icuu(:)
    COMPLEX(wp), ALLOCATABLE, SAVE  :: ksq(:)
    INTEGER, SAVE                   :: we_size, wd_size, wp_size
    INTEGER, SAVE                   :: region_three_we_width, region_three_wp_width

    COMPLEX(wp), ALLOCATABLE, SAVE  :: region_three_wpmat(:, :)
    COMPLEX(wp), ALLOCATABLE, SAVE  :: region_three_wemat(:, :, :)

    PRIVATE

    PUBLIC liuu, miuu, ltuu, lmuu, cfuu, lpuu, lsuu, etuu, icuu, ksq, &
           init_lrpots,  deallocate_lrpots, wp2pot_v, wp2pot, wdpot_v, wdpot, wp1pot, &
           we_size, wd_size, wp_size, region_three_wpmat, region_three_wemat, &
           region_three_we_coupled, region_three_wp_coupled, &
           region_three_we_width, region_three_wp_width

CONTAINS

    !> \brief Set up long-range potentials (WE, WD, WP)
    !>
    !> The routines `setupwd_*` and `setuppmatco*` are called twice: once to
    !> determine the size of the matrices, and once to allocate and populate them
    !>
    !> In general the WD and WP arrays could couple any channel to any other, so
    !> the matrices should be of dimension `LML_block_tot_nchan` * `LML_block_tot_nchan`. For systems
    !> with many channels, this becomes prohibitively large. Hence these are
    !> stored in a banded-structure. In practice, only a subset of channels is
    !> coupled, and the resulting array is a banded diagonal. Thus we store the
    !> matrix element coupling channel `ii` to channel `jj` in element (ii - jj,
    !> jj) of the matrix. The dimension is thus `MAX( coupled (ii - jj)) ,
    !> L_block_tot_nchan)`, which is in general much smaller than the full matrix.
    !>
    SUBROUTINE init_lrpots

        IF (.NOT. molecular_target) THEN
            CALL makecfuu(.TRUE., we_size)                     !determines size of WE
            CALL makecfuu(.FALSE., we_size)                    !sets up coefficients for WE

            IF (double_ionization) THEN
               CALL setup_region_three_ham_matrix(.TRUE., region_three_we_width, region_three_wp_width)
               CALL setup_region_three_ham_matrix(.FALSE., region_three_we_width, region_three_wp_width)
            END IF

            IF (dipole_format_id.EQ.RmatrixII_format_id) THEN
                CALL setupwd_len_atomic(.TRUE., wd_size)           !determines size of WD
                CALL setupwd_len_atomic(.FALSE., wd_size)          !sets up coefficients for WD
                CALL setuppmatco_len_atomic(.TRUE., wp_size)       !determines size of WP
                CALL setuppmatco_len_atomic(.FALSE., wp_size)      !sets up coefficients for WP
                IF (dipole_velocity_output) THEN
                    CALL setupwd_vel_atomic(.FALSE., wd_size)       !sets up coefficients for WD (velocity gauge)
                    CALL setuppmatco_vel_atomic(.FALSE., wp_size)   !sets up coefficients for WP (velocity gauge)
                END IF
            ELSE
               CALL setup_wp_and_wd_for_RMatrixI
               CALL setuppmatco_len_atomic(.false., mat_size_wp) !Check wp matrix
            END IF
        ELSE
            CALL makecfuu(.TRUE., we_size)                     !determines size of WE
            CALL makecfuu(.FALSE., we_size)                    !sets up coefficients for WE
            CALL setupwd_len_molecular(.TRUE., wd_size)        !determines size of WD
            CALL setupwd_len_molecular(.FALSE., wd_size)       !sets up coefficients for WD
            CALL setuppmatco_len_molecular(.TRUE., wp_size)    !determines size of WP
            CALL setuppmatco_len_molecular(.FALSE., wp_size)   !sets up coefficients for WP
        END IF

    END SUBROUTINE init_lrpots

    !>\brief allocates WP and WD matricies for when RMatrixI inputs are used
    !>
    !> The WP and WD matrix data has already been read from readhd. and stored
    !> in LML_wd_read_store and LML_wp_read_store.  This subroutine allocates
    !> and wdpot and wp2pot
    SUBROUTINE setup_wp_and_wd_for_RMatrixI
        INTEGER :: err

        wd_size=mat_size_wd
        wp_size=mat_size_wp

        !TODO - velocity gauge here
        ALLOCATE (wp2pot(1, -mat_size_wp:mat_size_wp, LML_block_tot_nchan), stat=err)
        ALLOCATE (wdpot(1, -mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat=err)

        wdpot(1,:,:)=LML_wd_read_store
        wp2pot(1,:,:)=LML_wp_read_store

        IF (dipole_velocity_output) THEN
            ALLOCATE(wdpot_v(1, -mat_size_wd:mat_size_wd, LML_block_tot_nchan), stat=err)
            wdpot_v(1,:,:)=LML_wd_read_store_v


            ALLOCATE (wp1pot(1, -mat_size_wp:mat_size_wp, LML_block_tot_nchan), &
                      wp2pot_v(1, -mat_size_wp:mat_size_wp, LML_block_tot_nchan), stat=err)
            wp2pot_v=0.0_wp
            wp1pot=0.0_wp
        END IF

    End SUBROUTINE

    !> \brief determine size/allocate and set up coefficients for WD
    !>
    !> The WD matrix describes the laser coupling with the residual ion. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    SUBROUTINE setupwd_len_atomic(get_size, mat_size)

        ! DDAC: SIGNS CONSISTENT WITH ORIGINAL RMT FOR delta_ML = 0 CASE

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER  :: i, j, ist, jst, nctf, ncti, ichf, ichi
        REAL(wp) :: w6j
        INTEGER  :: chcount1, chcount2, targ1, targ2
        INTEGER  :: li, lj
        INTEGER  :: lcfgf, lcfgi
        INTEGER  :: blf, bli, ii, jj, ptyi, ptyf
        INTEGER  :: mli, mlf, delta_ml
        INTEGER  :: a2, a3, a4, a5, a6, err
        REAL(wp) :: c, a, aa, sign1, tmom

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wdpot(1, -mat_size:mat_size, LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wdmat ')
            wdpot = (0.0_wp, 0.0_wp)
        END IF

        IF (dipole_format_id .EQ. RmatrixII_format_id) THEN
            ! WARNING: ISSUE WITH WHICH IS INITIAL AND FINAL TARG STATE
            ii = 0
            c = 0
            aa = 0
            w6j = 0
            tmom = 0
            DO i = 1, no_of_LML_blocks
                bli = LML_block_lrgl(i)
                mli = LML_block_ml(i)
                ptyi = LML_block_npty(i)
                chcount1 = 0
                DO ist = 1, ntarg
                    DO ncti = 1, LML_block_nconat(ist, i)
                        chcount1 = chcount1 + 1
                        ichi = chcount1
                        targ1 = ist
                        lcfgi = ltarg(targ1)
                        li = LML_block_l2p(ichi, i)
                        ii = ii + 1
                        jj = 0

                        DO j = 1, no_of_LML_blocks
                            blf = LML_block_lrgl(j)
                            mlf = LML_block_ml(j)
                            ptyf = LML_block_npty(j)
                            chcount2 = 0

                            DO jst = 1, ntarg
                                DO nctf = 1, LML_block_nconat(jst, j)
                                    chcount2 = chcount2 + 1
                                    ichf = chcount2
                                    targ2 = jst
                                    lcfgf = ltarg(targ2)
                                    lj = LML_block_l2p(ichf, j)
                                    jj = jj + 1

                                    IF (li == lj .AND. ABS(crlv(targ1, targ2)) &
                                        .NE. 0.0_wp &
                                        .AND. ABS(mlf - mli) .LE. 1 &
                                        .AND. ABS(blf - bli) .LE. 1) THEN

                                        a = SQRT(2._wp*REAL(blf, wp) + 1._wp)
                                        sign1 = 1.0_wp

                                        IF (MOD(lcfgi + lcfgf + 1, 2) == 1) THEN
                                            sign1 = -1.0_wp
                                        END IF

                                        c = a*sign1
                                        a2 = lcfgf + lcfgf
                                        a3 = bli + bli
                                        a4 = li + li
                                        a5 = lcfgi + lcfgi
                                        a6 = blf + blf
                                        tmom = crlv(targ1, targ2)     ! length target moments
                                        CALL dracah(2, a2, a3, a4, a5, a6, w6j)

                                        delta_ml = mlf - mli

                                        IF (delta_ml == 1) THEN
                                            aa = -cg(1, blf, bli, -1, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot(1, ii - jj, jj) = c*aa*w6j*tmom
                                            END IF
                                        END IF

                                        IF (delta_ml == -1) THEN
                                            aa = -cg(1, blf, bli, 1, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot(1, ii - jj, jj) = c*aa*w6j*tmom
                                            END IF
                                        END IF

                                        IF (delta_ml == 0) THEN
                                            aa = -cg(1, blf, bli, 0, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot(1, ii - jj, jj) = c*aa*w6j*tmom
                                            END IF
                                        END IF

                                    END IF

                                END DO
                            END DO
                        END DO
                    END DO
                END DO
            END DO

        ELSE
            wdpot(1, :, :) = LML_wd_read_store(:, :)
        END IF


        ! Length gauge only - multiply by i
        ! Fano Racah phase convention
!       wdpot = (0.0_wp,1.0_wp)*wdpot  ! DDAC: Field comps. elsewhere

        IF (.NOT. get_size) wdpot = (-1.0_wp, 0.0_wp)*wdpot


    END SUBROUTINE setupwd_len_atomic

!-------------------------------------------------------------------------------------

    !> \brief determine size/allocate and set up coefficients for WD
    !>
    !> The WD matrix describes the laser coupling with the residual ion. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    SUBROUTINE setupwd_len_molecular(get_size, mat_size)

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER :: i, j, ichf, ichi
        INTEGER :: targ1, targ2
        INTEGER :: li, lj, mi, mj
        INTEGER :: ii, jj
        INTEGER :: err
        REAL(wp), DIMENSION(3) :: tmom

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wdpot(3, -mat_size:mat_size, LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wdpot')
            wdpot = (0.0_wp, 0.0_wp)
        END IF
        ii = 0

        DO i = 1, no_of_L_blocks
            DO ichi = 1, L_block_nchan(i)
                li = L_block_l2p(ichi, i)
                mi = m2p(ichi, i)
                targ1 = ichl(ichi, i)
                ii = ii + 1
                jj = 0

                DO j = 1, no_of_L_blocks
                    DO ichf = 1, L_block_nchan(j)
                        lj = L_block_l2p(ichf, j)
                        mj = m2p(ichf, j)
                        targ2 = ichl(ichf, j)
                        jj = jj + 1
                        tmom(1:3) = crlv(3*(targ1 - 1) + 1:3*(targ1 - 1) + 3, targ2)

                        ! todo implement selection rule on the total spins in i and j symmetry

                        IF (li == lj .AND. mi == mj .AND. any(tmom /= 0)) THEN
                            IF (get_size) THEN
                                mat_size = MAX( ABS(ii-jj), mat_size)
                            ELSE
                                wdpot(1, ii - jj, jj) = tmom(1)
                                wdpot(2, ii - jj, jj) = tmom(2)
                                wdpot(3, ii - jj, jj) = tmom(3)
                            END IF
                        END IF

                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE setupwd_len_molecular

!-------------------------------------------------------------------------------------
! SET UP WD matrix   FOR LENGTH GAUGE
!-------------------------------------------------------------------------------------

    SUBROUTINE setupwd_vel_atomic(get_size, mat_size)

    ! DDAC: SIGNS CONSISTENT WITH ORIGINAL RMT FOR delta_ML = 0 CASE

    ! This subroutine is required only to compute the expectation value of
    ! dipole velocity in the outer region. For this purpose, the
    ! electric-field array is replaced with factors_axis (see
    ! subroutine hhg_inner in module inner_propagators) that
    ! selects which dipole blocks should be included in the calculations.
    ! At present, only the data for the M_{L}-conserving transitions are
    ! retained, as necessary to calculate the expectation value of the
    ! z-component of dipole velocity. Although factors_axis does not
    ! represent a field, its elements are treated like those of the electric-field
    ! array, and are multiplied by the appropriate phase factors in
    ! field_sph_component. The phase factors appearing in this subroutine
    ! compensate for these multiplications, such as to produce the correct
    ! harmonic phases.



        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER  :: i, j, ist, jst, nctf, ncti, ichf, ichi
        REAL(wp) :: w6j
        INTEGER  :: chcount1, chcount2, targ1, targ2
        INTEGER  :: li, lj
        INTEGER  :: lcfgf, lcfgi
        INTEGER  :: blf, bli, ii, jj, ptyi, ptyf
        INTEGER  :: mli, mlf, delta_ml
        INTEGER  :: a2, a3, a4, a5, a6, err
        REAL(wp) :: c, a, aa, sign1, tmom

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wdpot_v(1, -mat_size:mat_size, LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wdmat ')
            wdpot_v = (0.0_wp, 0.0_wp)
        END IF

        IF (dipole_format_id .EQ. RmatrixII_format_id) THEN
            ! WARNING: CARE NEEDED WITH WHICH IS INITIAL AND FINAL TARG STATE
            ii = 0
            c = 0
            aa = 0
            w6j = 0
            tmom = 0
            DO i = 1, no_of_LML_blocks
                bli = LML_block_lrgl(i)
                mli = LML_block_ml(i)
                ptyi = LML_block_npty(i)
                chcount1 = 0
                DO ist = 1, ntarg
                    DO ncti = 1, LML_block_nconat(ist, i)
                        chcount1 = chcount1 + 1
                        ichi = chcount1
                        targ1 = ist
                        lcfgi = ltarg(targ1)
                        li = LML_block_l2p(ichi, i)
                        ii = ii + 1
                        jj = 0

                        DO j = 1, no_of_LML_blocks
                            blf = LML_block_lrgl(j)
                            mlf = LML_block_ml(j)
                            ptyf = LML_block_npty(j)
                            chcount2 = 0

                            DO jst = 1, ntarg
                                DO nctf = 1, LML_block_nconat(jst, j)
                                    chcount2 = chcount2 + 1
                                    ichf = chcount2
                                    targ2 = jst
                                    lcfgf = ltarg(targ2)
                                    lj = LML_block_l2p(ichf, j)
                                    jj = jj + 1

                                    IF (li == lj .AND. ABS(crlv_v(targ1, targ2)) &
                                        .NE. 0.0_wp &
                                        .AND. ABS(mlf - mli) .LE. 1 &
                                        .AND. ABS(blf - bli) .LE. 1) THEN

                                        a = SQRT(2._wp*REAL(blf, wp) + 1._wp)
                                        sign1 = 1.0_wp

                                        IF (MOD(lcfgi + lcfgf + 1, 2) == 1) THEN
                                            sign1 = -1.0_wp
                                        END IF

                                        c = a*sign1
                                        a2 = lcfgf + lcfgf
                                        a3 = bli + bli
                                        a4 = li + li
                                        a5 = lcfgi + lcfgi
                                        a6 = blf + blf
                                        tmom = crlv_v(targ1, targ2)     ! Velocity target moments
                                        CALL dracah(2, a2, a3, a4, a5, a6, w6j)

                                        delta_ml = mlf - mli

                                        IF (delta_ml == 1) THEN
                                            aa = -cg(1, blf, bli, -1, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                ! DDAC: Factor -i since p_{z} = -id/dz
                                                wdpot_v(1, ii - jj, jj) = (0.0_wp,-1.0_wp)*c*aa*w6j*tmom
                                            END IF
                                        END IF

                                        IF (delta_ml == -1) THEN
                                            aa = -cg(1, blf, bli, 1, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot_v(1, ii - jj, jj) = (0.0_wp,-1.0_wp)*c*aa*w6j*tmom
                                            END IF
                                        END IF

                                        IF (delta_ml == 0) THEN
                                            aa = -cg(1, blf, bli, 0, mlf, mli)
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wdpot_v(1, ii - jj, jj) = (0.0_wp,-1.0_wp)*c*aa*w6j*tmom
                                            END IF
                                        END IF

                                    END IF


                                END DO
                            END DO
                        END DO
                    END DO
                END DO
            END DO

        ELSE
            wdpot_v(1, :, :) = -wd_read_store(:, :)  !GSJA added factor of -1 needed due to multiplication by -1 below
        END IF



    END SUBROUTINE setupwd_vel_atomic

!-------------------------------------------------------------------------------------

    !> \brief determine size/allocate and set up coefficients for WP
    !>
    !> The WP matrix describes the laser coupling with the outer electron. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    !>
    !> (based on R-matrix book, pg 399)
    SUBROUTINE setuppmatco_len_atomic(get_size, mat_size)

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER     :: ii, jj, i, j, ist, jst, ncti, nctf
        INTEGER     :: bli, ptyi, blf, ptyf, ichi, jchf
        INTEGER     :: chcount1, chcount2, targ1, targ2
        INTEGER     :: lcfgi, li, lcfgf, lj, err
        INTEGER     :: bli2, blf2, li2, lj2, lcfgi2
        INTEGER     :: mli, mlf, delta_ml
        REAL(wp)    :: sign1, b, c, d, w6j
        COMPLEX(wp) :: pmtrxel1, pmtrxel2
        REAL(wp)    :: al, bl
        COMPLEX(wp) :: sign2
        COMPLEX(wp), ALLOCATABLE :: coeff_check(:, :)

        LOGICAL :: All_Passed

        IF (get_size) THEN
            mat_size = 0
        ELSE
            IF (dipole_format_id .eq. RmatrixII_format_id) THEN
                ALLOCATE (wp2pot(1, -mat_size:mat_size, LML_block_tot_nchan), stat=err)
                CALL assert(err .EQ. 0, 'allocation error in wpmat ')
                wp2pot = (0.0_wp, 0.0_wp)
            END IF
        END IF


        ! If we are using RMatrixI dipole data, then the wp matricies should have already
        ! been read in and are stored in wp_read_store . Here we only need to assign
        ! the data to the wp2pot matrix. We still run the RMT code to create the matrix,
        ! but rather than write the pmtrexl1 to the wp2pot array, we store it in coeff_check.
        ! We then perform a comparison of wp2pot and coeff_check at the end.
        IF (dipole_format_id .eq. RmatrixI_format_id) THEN
            ALLOCATE (coeff_check(-mat_size:mat_size, LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in coeff_check ')
        END IF

        ! Begin calculation of wp2pot(if RMatrixII dipole format) or coeff_check(if RMatrixI dipole format)
        ii = 0
        DO i = 1, no_of_LML_blocks
            bli = LML_block_lrgl(i)
            ptyi = LML_block_npty(i)
            mli = LML_block_ml(i)
            chcount1 = 0

            DO ist = 1, ntarg
                DO ncti = 1, LML_block_nconat(ist, i)
                    chcount1 = chcount1 + 1
                    ichi = chcount1
                    targ1 = ist
                    lcfgi = ltarg(targ1)
                    li = LML_block_l2p(ichi, i)
                    ii = ii + 1
                    jj = 0

                    DO j = 1, no_of_LML_blocks
                        blf = LML_block_lrgl(j)
                        ptyf = LML_block_npty(j)
                        mlf = LML_block_ml(j)
                        chcount2 = 0

                        DO jst = 1, ntarg
                            DO nctf = 1, LML_block_nconat(jst, j)
                                chcount2 = chcount2 + 1
                                jchf = chcount2
                                targ2 = jst
                                lcfgf = ltarg(targ2)
                                lj = LML_block_l2p(jchf, j)
                                jj = jj + 1

                                IF (targ1 == targ2 .AND. (ptyi /= ptyf)) THEN
                                    pmtrxel1 = (0._wp, 0.0_wp)
                                    pmtrxel2 = (0._wp, 0.0_wp)
                                    sign1 = 1.0_wp    !DDAC: consistent with original RMT

                                    ! warning with signs
                                    IF (MOD(blf + bli, 2) == 1) THEN
                                        sign1 = -1.0_wp    !DDAC: consistent with original RMT
                                    END IF

                                    ! WARNING ON SIGNS
!                                    sign2 = (0.0_wp,1.0_wp)   !length gauge

                                    sign2 = (1.0_wp, 0.0_wp)

                                    b = SQRT((2._wp*REAL(lj, wp) + 1._wp)*(2._wp*REAL(bli, wp) + 1._wp))
!                                   c = cg(bli,1,blf,ml,0,ml)
                                    d = cg(li, 1, lj, 0, 0, 0)
                                    li2 = li + li
                                    lj2 = lj + lj
                                    bli2 = bli + bli
                                    blf2 = blf + blf
                                    lcfgi2 = lcfgi + lcfgi
                                    CALL dracah(2, lj2, bli2, lcfgi2, li2, blf2, w6j)

                                    IF (li == (lj - 1)) THEN
                                        !!!!!!!!WARNING ON SIGN!!!!!!!!!!!!
                                        al = REAL(lj, wp)/SQRT((2._wp*REAL(lj, wp) - 1._wp)* &
                                                               (2._wp*REAL(lj, wp) + 1._wp))
                                        !!!!!!!!!!!!!!!!!!!!!
                                        bl = REAL(lj, wp)

                                    ELSE
                                        IF (li == (lj + 1)) THEN
                                            ! Warning on sign change for al in length gauge
                                            al = -(REAL(lj, wp) + 1._wp)/SQRT((2._wp*REAL(lj, wp) + 1._wp)* &
                                                                              (2._wp*REAL(lj, wp) + 3._wp))
                                            bl = -REAL(lj, wp) - 1._wp
                                        END IF
                                    END IF

                                    delta_ml = mlf - mli

!                                   IF (ABS(d) < 0.1e-11_wp) THEN
!                                       pmtrxel1 = (0.0_wp,0.0_wp)
!                                       pmtrxel2 = (0.0_wp,0.0_wp)
!                                   ELSE

                                    IF (delta_ml == 1) THEN
                                        IF (ABS(d) < 0.1e-11_wp) THEN
                                            pmtrxel1 = (0.0_wp, 0.0_wp)
                                            pmtrxel2 = (0.0_wp, 0.0_wp)
                                        ELSE
                                            c = cg(bli, 1, blf, mli, 1, mlf)
                                            pmtrxel1 = -(sign1*sign2*b*c*w6j*al)/d  ! DDAC: Length gauge
                                            pmtrxel2 = (sign1*sign2*b*c*w6j*al*bl)/d  ! DDAC: Velocity gauge
                                        END IF
                                    END IF

                                    IF (delta_ml == -1) THEN
                                        IF (ABS(d) < 0.1e-11_wp) THEN
                                            pmtrxel1 = (0.0_wp, 0.0_wp)
                                            pmtrxel2 = (0.0_wp, 0.0_wp)
                                        ELSE
                                            c = cg(bli, 1, blf, mli, -1, mlf)
                                            pmtrxel1 = -(sign1*sign2*b*c*w6j*al)/d  !DDAC: Length gauge
                                            pmtrxel2 = (sign1*sign2*b*c*w6j*al*bl)/d  ! DDAC: Velocity gauge
                                        END IF
                                    END IF

                                    IF (delta_ml == 0) THEN
                                        IF (ABS(d) < 0.1e-11_wp) THEN
                                            pmtrxel1 = (0.0_wp, 0.0_wp)
                                            pmtrxel2 = (0.0_wp, 0.0_wp)
                                        ELSE
                                            c = cg(bli, 1, blf, mli, 0, mlf)
                                            pmtrxel1 = (sign1*sign2*b*c*w6j*al)/d  ! DDAC: Length gauge
                                            pmtrxel2 = (sign1*sign2*b*c*w6j*al*bl)/d  ! DDAC: Velocity gauge
                                        END IF
                                    END IF

!                                   END IF

!                                   IF (ABS(d) < 0.1e-11_wp) THEN
!                                       pmtrxel1 = (0.0_wp,0.0_wp)
!                                       pmtrxel2 = (0.0_wp,0.0_wp)
!                                   ELSE
!                                       pmtrxel1 = (sign1*sign2*b*c*w6j*al)/d    ! length gauge
!                                       pmtrxel2 = (sign1*sign2*b*c*w6j*al*bl)/d
!                                   END IF

                                    IF (dipole_format_id .EQ. RmatrixII_format_id) THEN
                                        IF (pmtrxel1 /= (0.0_wp, 0.0_wp)) THEN
                                            IF (get_size) THEN
                                                mat_size = MAX( ABS(ii-jj), mat_size)
                                            ELSE
                                                wp2pot(1, ii-jj, jj) = pmtrxel1
                                            END IF
                                        END IF
                                    ELSE
                                       if (abs(pmtrxel1).gt.0.00000000001) then
                                       if (abs(ii-jj).gt.abs(mat_size)) then
                                          print *,'about to crash',ii,jj,pmtrxel1,mat_size
                                       end if
                                       coeff_check(ii-jj, jj) = pmtrxel1
                                       END IF
                                    END IF

!                               ELSE

!                                   IF (dipole_format_id .EQ. RmatrixII_format_id) THEN
!                                       wp2pot(1, ii-jj, jj) = (0.0_wp, 0.0_wp)
!                                   ELSE
!                                       coeff_check(ii, jj) = 0.0_wp
!                                   END IF

                                END IF

                            END DO
                        END DO
                    END DO
                END DO
            END DO
        END DO

        ! If we use RMatrixI dipole format, we Check that RMatrixI wp wp2pots agree
        ! with RMT calculated wp2pots
        IF (dipole_format_id .EQ. RmatrixI_format_id) THEN

            All_Passed=.true.

            IF (coupling_id.EQ.LS_coupling_id) THEN
            DO ii = -mat_size, mat_size
                DO jj = 1, LML_block_tot_nchan

                    ! if the two matrices don't agree then -
                    IF (ABS(ABS(coeff_check(ii, jj) - wp2pot(1, ii, jj))) .GT. 0.00001_wp) THEN
!                       PRINT diagnostic info
                        PRINT *, 'error in wp2pot!', ii, jj
                        PRINT *, 'coeff_check', ABS(coeff_check(ii, jj)),ABS(wp2pot(1, ii, jj))
                        ! and exit RMT.
                        All_Passed=.false.
                    END IF

                END DO
            END DO
            END IF

            CALL assert(All_Passed, 'Stopping')

            DEALLOCATE (coeff_check)

        END IF

    END SUBROUTINE setuppmatco_len_atomic

!-------------------------------------------------------------------------------------


    !> \brief determine size/allocate and set up coefficients for WP
    !>
    !> The WP matrix describes the laser coupling with the outer electron. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    !>
    !> Needed for molecular case:
    !> Must be diagonal in target space and contains coupling cfs for three real spherical harmonics.
    SUBROUTINE setuppmatco_len_molecular(get_size, mat_size)

        USE global_data, ONLY: pi

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        COMPLEX(wp) :: pmtrxel(3)
        INTEGER :: ii, jj, i, j
        INTEGER :: ichi, ichj
        INTEGER :: targ1, targ2
        INTEGER :: li, lj, mi, mj, err

        ! norm removes the normalization factor sqrt(3/(4*pi)) for the real sph. harmonic, e.g. S_{1,1} = sqrt(3/(4*pi))*x/r but we need only x/r.
        ! and the minus sign is for electron charge
        REAL(wp), parameter :: norm = -SQRT(4.0_wp*pi/3.0_wp)

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wp2pot(3, -mat_size:mat_size, LML_block_tot_nchan), &
                      stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wpmat ')
            wp2pot = (0.0_wp, 0.0_wp)
        END IF

        ii = 0
        DO i = 1, no_of_L_blocks
            DO ichi = 1, L_block_nchan(i)
                li = LML_block_l2p(ichi, i)
                mi = m2p(ichi, i)
                targ1 = ichl(ichi, i)
                ii = ii + 1
                jj = 0

                DO j = 1, no_of_L_blocks
                    DO ichj = 1, L_block_nchan(j)
                        lj = LML_block_l2p(ichj, j)
                        mj = m2p(ichj, j)
                        targ2 = ichl(ichj, j)
                        jj = jj + 1

                        ! todo implement selection rule on the total spins in i and j symmetry


                        pmtrxel(1) = norm*get_coupling(li, lj, mi, mj, 1, 1)
                        pmtrxel(2) = norm*get_coupling(li, lj, mi, mj, 1, -1)
                        pmtrxel(3) = norm*get_coupling(li, lj, mi, mj, 1, 0)

                        IF (targ1 == targ2) THEN
                            IF (ANY(ABS(pmtrxel) > 1e-12_wp)) THEN
                                IF (get_size) THEN
                                    mat_size = MAX( ABS(ii-jj), mat_size)
                                ELSE
                                    ! integral over three real spherical harmonics with the dipole spherical harmonic Y(1,mi+mj)
                                    wp2pot(1, ii - jj, jj) = pmtrxel(1) !   wp2pot(1,:,:) corresponds to Y(1,+1), i.e. ~x
                                    wp2pot(2, ii - jj, jj) = pmtrxel(2) !   wp2pot(2,:,:)     "          Y(1,-1), i.e. ~y
                                    wp2pot(3, ii - jj, jj) = pmtrxel(3) !   wp2pot(3,:,:)     "          Y(1, 0), i.e. ~z
                                END IF
                            END IF
                        END IF

                    END DO
                END DO
            END DO
        END DO

        ! finally, drop negligible couplings
        IF (.NOT. get_size) THEN
            WHERE (ABS(wp2pot) < 1e-12_wp) wp2pot = 0
        END IF

    END SUBROUTINE setuppmatco_len_molecular

!-------------------------------------------------------------------------------------

    FUNCTION get_coupling(li, lj, mi, mj, L, M)

        USE readhd, ONLY: n_rg, rg, lm_rg

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: li, lj, mi, mj, L, M
        REAL(wp) :: get_coupling

        INTEGER :: test(6), i, j
        LOGICAL :: match

        get_coupling = 0.0_wp

        test(1:6) = (/li, mi, lj, mj, L, M/)

        DO i = 1, n_rg
            match = .true.

            DO j = 1, 6
                IF (test(j) .NE. lm_rg(j, i)) THEN
                    match = .false.
                    EXIT
                END IF
            END DO

            IF (match) THEN
                get_coupling = rg(i)
                EXIT
            END IF

        END DO !i

    END FUNCTION get_coupling

!-------------------------------------------------------------------------------------
! SET UP PMAT AND COEFF (based on R-matrix book, pg 399) VELOCITY GAUGE
!-------------------------------------------------------------------------------------

    SUBROUTINE setuppmatco_vel_atomic(get_size, mat_size)


    ! This subroutine is required only to compute the expectation value of
    ! dipole velocity in the outer region. For this purpose, the
    ! electric-field array is replaced with factors_axis (see
    ! subroutine hhg_inner in module inner_propagators) that
    ! selects which dipole blocks should be included in the calculations.
    ! Although factors_axis does not represent a field, 
    ! its elements are treated like those of the electric-field
    ! array, and are multiplied by the appropriate phase factors in
    ! field_sph_component. The phase factors appearing in this subroutine
    ! compensate for these multiplications, such as to produce the correct
    ! harmonic phases.



        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER     :: ii, jj, i, j, ist, jst, ncti, nctf
        INTEGER     :: bli, ptyi, blf, ptyf, ichi, jchf
        INTEGER     :: chcount1, chcount2, targ1, targ2
        INTEGER     :: lcfgi, li, lcfgf, lj, err
        INTEGER     :: bli2, blf2, li2, lj2, lcfgi2
        INTEGER     :: mli, mlf, delta_ml
        REAL(wp)    :: sign1, b, c, d, w6j
        COMPLEX(wp) :: pmtrxel1, pmtrxel2
        REAL(wp)    :: al, bl
        COMPLEX(wp) :: sign2

        IF (get_size) THEN
            mat_size = 0
        ELSE
            ALLOCATE (wp1pot(1, -mat_size:mat_size, LML_block_tot_nchan), &
                      wp2pot_v(1, -mat_size:mat_size, LML_block_tot_nchan), stat=err)

            CALL assert(err .EQ. 0, 'allocation error in wp1pot and/or wp2pot_v ')
            wp1pot = (0.0_wp, 0.0_wp)
            wp2pot_v = (0.0_wp, 0.0_wp)
        END IF



        !wp1pot = (0.0_wp, 0.0_wp)
        !wp2pot_v = (0.0_wp, 0.0_wp)
        ii = 0
        DO i = 1, no_of_LML_blocks
            bli = LML_block_lrgl(i)
            ptyi = LML_block_npty(i)
            mli = LML_block_ml(i)
            chcount1 = 0

            DO ist = 1, ntarg
                DO ncti = 1, LML_block_nconat(ist, i)
                    chcount1 = chcount1 + 1
                    ichi = chcount1
                    targ1 = ist
                    lcfgi = ltarg(targ1)
                    li = LML_block_l2p(ichi, i)
                    ii = ii + 1
                    jj = 0

                    DO j = 1, no_of_LML_blocks
                        blf = LML_block_lrgl(j)
                        ptyf = LML_block_npty(j)
                        mlf = LML_block_ml(j)
                        chcount2 = 0

                        DO jst = 1, ntarg
                            DO nctf = 1, LML_block_nconat(jst, j)
                                chcount2 = chcount2 + 1
                                jchf = chcount2
                                targ2 = jst
                                lcfgf = ltarg(targ2)
                                lj = LML_block_l2p(jchf, j)
                                jj = jj + 1

                                IF (targ1 == targ2 .AND. (ptyi /= ptyf)) THEN
                                    pmtrxel1 = (0._wp, 0.0_wp)
                                    pmtrxel2 = (0._wp, 0.0_wp)
                                    sign1 = -1.0_wp

                                    ! warning with signs
                                    IF (MOD(blf + bli, 2) == 1) THEN
                                        sign1 = 1.0_wp
                                    END IF

                                    ! WARNING ON SIGNS
                                    sign2 = (0.0_wp, 1.0_wp)


                                    b = SQRT((2._wp*REAL(lj, wp) + 1._wp)*(2._wp*REAL(bli, wp) + 1._wp))
                                    !c = cg(bli, 1, blf, ml, 0, ml)
                                    d = cg(li, 1, lj, 0, 0, 0)
                                    li2 = li + li
                                    lj2 = lj + lj
                                    bli2 = bli + bli
                                    blf2 = blf + blf
                                    lcfgi2 = lcfgi + lcfgi
                                    CALL dracah(2, lj2, bli2, lcfgi2, li2, blf2, w6j)

                                    IF (li == (lj - 1)) THEN
                                        !!!!!!!!WARNING ON SIGN!!!!!!!!!!!!
                                        al = REAL(lj, wp)/SQRT((2._wp*REAL(lj, wp) - 1._wp)* &
                                                               (2._wp*REAL(lj, wp) + 1._wp))
                                        !!!!!!!!!!!!!!!!!!!!!
                                        bl = REAL(lj, wp)
                                    ELSE
                                        IF (li == (lj + 1)) THEN
                                            !warning on sign change for al in length gauge
                                            al = -(REAL(lj, wp) + 1._wp)/SQRT((2._wp*REAL(lj, wp) + 1._wp)* &
                                                                              (2._wp*REAL(lj, wp) + 3._wp))
                                            bl = -REAL(lj, wp) - 1._wp
                                        END IF
                                    END IF


                                    delta_ml = mlf - mli


                                    IF (ABS(delta_ml) .LE. 1) THEN

                                        IF (ABS(d) < 0.1e-11_wp) THEN

                                            pmtrxel1 = (0.0_wp, 0.0_wp)
                                            pmtrxel2 = (0.0_wp, 0.0_wp)

                                        ELSE

                                            c = cg(bli, 1, blf, mli, delta_ml, mlf)*(-1.0_wp)**(delta_ml)
                                            pmtrxel1 = (sign2*sign1*b*c*w6j*al*bl)/d  ! Unique to velocity gauge
                                            pmtrxel2 = (sign2*sign1*b*c*w6j*al)/d!

                                        END IF

                                    END IF

                                        IF (pmtrxel2 /= (0.0_wp, 0.0_wp)) THEN
                                            wp1pot(1, ii-jj, jj) = pmtrxel2
                                        END IF
                                        IF (pmtrxel1 /= (0.0_wp, 0.0_wp)) THEN
                                            wp2pot_v(1, ii-jj, jj) = pmtrxel1
                                        END iF

                                END IF

                            END DO
                        END DO
                    END DO
                END DO
            END DO
        END DO

    END SUBROUTINE setuppmatco_vel_atomic

    !> \brief determine size/allocate and set up coefficients for WE
    !>
    !> The WE matrix describes the coupling between the outer electron and residual ion. This
    !> routine is called twice, once with `get_size = .TRUE.` to determine the
    !> size of the matrix cfuu, and once with `get_size = .FALSE.` to allocate and
    !> populate the matrix.
    SUBROUTINE makecfuu(get_size,mat_size)

        LOGICAL, INTENT(IN) :: get_size
        INTEGER, INTENT(INOUT) :: mat_size
        INTEGER :: ii, jj, i, j, ist, jst
        INTEGER :: chcount1, chcount2, ncti, nctf
        INTEGER :: lj, ichi, ichf, err, lambda

        IF (get_size) THEN
            mat_size = 0
            ALLOCATE (liuu(LML_block_tot_nchan), &
                  etuu(LML_block_tot_nchan), ksq(LML_block_tot_nchan), ltuu(LML_block_tot_nchan), &
                  lmuu(LML_block_tot_nchan), miuu(LML_block_tot_nchan), icuu(LML_block_tot_nchan), &
                  lpuu(LML_block_tot_nchan), lsuu(LML_block_tot_nchan), stat=err)
            CALL assert(err .EQ. 0, 'allocation error in wemat ')
        ELSE
            ALLOCATE (cfuu(-mat_size:mat_size, LML_block_tot_nchan, lamax), stat=err)
            cfuu = 0._wp
            CALL assert(err .EQ. 0, 'allocation error in wemat ')
        END IF

        ii = 0
        DO i = 1, no_of_LML_blocks
            chcount1 = 0

            DO ist = 1, ntarg
                DO ncti = 1, LML_block_nconat(ist, i)
                    chcount1 = chcount1 + 1
                    ichi = chcount1
                    ii = ii + 1
                    etuu(ii) = etarg(ist) - etarg(1)
                    ksq(ii) = -2.0_wp*(etuu(ii))
                    liuu(ii) = LML_block_l2p(ichi, i)
                    miuu(ii) = m2p(ichi, i)

                    IF (molecular_target) THEN
                        icuu(ii) = ichl(ichi, i)
                    ELSE
                        DO j = 1, ii
                            IF (ABS(etuu(ii) - etuu(j)) < 1e-8_wp) THEN
                                icuu(ii) = j
                                EXIT
                            END IF
                        END DO
                    END IF

                    ltuu(ii) = LML_block_lrgl(i)

                    lmuu(ii) = LML_block_ml(i)

                    lpuu(ii) = LML_block_npty(i)
                    lsuu(ii) = LML_block_nspn(i)

                    jj = 0
                    DO j = 1, no_of_LML_blocks
                        chcount2 = 0

                        DO jst = 1, ntarg
                            DO nctf = 1, LML_block_nconat(jst, j)
                                chcount2 = chcount2 + 1
                                ichf = chcount2
                                lj = LML_block_l2p(ichf, j)
                                jj = jj + 1

                                IF (i == j) THEN
                                    IF (get_size) THEN
                                        mat_size = MAX( ABS(ii-jj), mat_size)
                                    ELSE
                                        DO lambda = 1, lamax
                                            cfuu(ii - jj, jj, lambda) = LML_block_cf(ichi, ichf, lambda, j)
                                        END DO
                                    END IF
                                END IF

                            END DO
                        END DO
                    END DO
                END DO
            END DO
        END DO


    END SUBROUTINE makecfuu

!---------------------------------------------------------------------------
    !> \brief Construct the region III W_E and W_P matrices.
    !>
    !>        The routine must be called twice: firstly with get_width = .TRUE., 
    !>        in which case the required sizes of the matrices are determined,
    !>        by calculating the maximum value of ABS(i - j) for each pair of 
    !>        two-electron channels i and j, coupled by either W_E or W_P in region III.
    !>
    !>        Then the routine is called with get_width = .FALSE., 
    !>        the matrices are allocated and constructed.

    !> \param[in] get_width    If true, the routine calculates the required widths
    !>                         of the matrices, based on the allowed channel couplings.
    !>                         If false, the matrices are constructed.
    !> \param[inout] we_width  The determined width of the region III W_E matrix, i.e. the
    !>                         maximum width between any pair of channels coupled by the
    !>                         W_E interaction in region III.
    !> \param[inout] wp_width  The determined width of the region III W_P matrix, i.e. the
    !>                         maximum width between any pair of channels coupled by the
    !>                         W_P interaction in region III.

    SUBROUTINE setup_region_three_ham_matrix(get_width, we_width, wp_width)

        LOGICAL, INTENT(IN)  :: get_width
        INTEGER, INTENT(INOUT) :: we_width, wp_width

        INTEGER     :: i, j, lambda
        INTEGER     :: lion, sion, la, lb, lee, bigl, bigml
        INTEGER     :: lionp, sionp, lap, lbp, leep, biglp, bigmlp
        INTEGER     :: err

        region_three_wpmat = zero
        region_three_wemat = zero

        IF (get_width) THEN
           we_width = 0; wp_width = 0
        ELSE
           ALLOCATE(region_three_wpmat(-wp_width:wp_width, two_electron_nchan), &
                    region_three_wemat(-we_width:we_width, two_electron_nchan, 0:lamax), &
                    stat=err)
        END IF

        DO i = 1, two_electron_nchan
           lion = ion_l(i)
           sion = ion_s(i)
           la = first_l(i)
           lb = second_l(i)
           lee = two_electron_l(i)
           bigl = tot_l(i)
           bigml = tot_ml(i)

           DO j = 1, two_electron_nchan
              lionp = ion_l(j)
              sionp = ion_s(j)
              lap = first_l(j)
              lbp = second_l(j)
              leep = two_electron_l(j)
              biglp = tot_l(j)
              bigmlp = tot_ml(j)

              IF (region_three_wp_coupled(i, j)) THEN
                  IF (get_width) THEN
                      wp_width = MAX(ABS(i - j), wp_width)
                  ELSE
                      region_three_wpmat(i - j, j) = region_three_int_ham(   &
                                           lion, la, lb, lee, bigl, bigml, &
                                           lap, lbp, leep, biglp, bigmlp)
                  END IF
              END IF

              IF (region_three_we_coupled(i, j)) THEN
                  IF (get_width) THEN
                      we_width = MAX(ABS(i - j), we_width)
                  ELSE
                      DO lambda = 0, lamax
                          region_three_wemat(i - j, j, lambda) = region_three_we_ham(lambda, &
                                           lion, la, lb, lee, bigl, &
                                           lionp, lap, lbp, leep, biglp)
                      END DO
                  END IF
              END IF

           END DO
        END DO

        CALL test_wp_for_helium
        CALL test_two_electron_symmetry

    END SUBROUTINE setup_region_three_ham_matrix

!---------------------------------------------------------------------------

    !> \brief Set up the laser interaction potential (W_P) for two electrons 
    !> in region III

    !>  This function is the angular part of Eq.(50) in 
    !>  <a href="../double_ionization/RMT_double_ionization_theory.pdf">
    !> the double ionization theory pdf under /docs.</a>
    !>  It includes all factors in Eq.(50) except the electric field E(t) and
    !>  the electron radius r_j.  

    !> \param lion   Angular momentum of the ion
    !> \param la     Initial angular momentum of electron 'a'
    !> \param lb     Initial angular momentum of electron 'b'
    !> \param lee    Initial total angular momentum of electrons 'a' and 'b'
    !> \param bigL   Initial total angular momentum of the system
    !> \param bigML  Initial total magnetic quantum number of the system
    !> \param lap    Final angular momentum of electron 'a'
    !> \param lbp    Final angular momentum of electron 'b'
    !> \param leep   Final total angular momentum of electrons 'a' and 'b'
    !> \param bigLp  Final total angular momentum of the system
    !> \param bigMLp Final total magnetic quantum number of the system
    !>
    COMPLEX(wp) FUNCTION region_three_int_ham(lion, la, lb, lee, bigL, bigML, &
                                                    lap, lbp, leep, bigLp, bigMLp)
      USE global_data, ONLY: zero, im 

      INTEGER     :: lion, la, lb, lee, bigL, bigML
      INTEGER     :: lap, lbp, leep, bigLp, bigMLp
      INTEGER     :: lj, ljp, lk, lkp
      REAL(wp)    :: coeffs, real_phase, square_roots, four_term_product
      REAL(wp)    :: rac1, rac2
      COMPLEX(wp) :: imag_phase

      region_three_int_ham = zero

      IF (lb == lbp) THEN 
          lj = la ; ljp = lap ; lk = lb ; lkp = lbp
      ELSE IF (la == lap) THEN 
          lj = lb ; ljp = lbp ; lk = la ; lkp = lap
      END IF

      IF ( (la == lap) .OR. (lb == lbp) ) THEN
         real_phase = (-1.0_wp) ** (lion + lk + bigLp + ljp)
         imag_phase = im**(lj - ljp)

         four_term_product = (2.0_wp * lee + 1.0_wp) &
                           * (2.0_wp * leep + 1.0_wp)  &
                           * (2.0_wp * lj + 1.0_wp)   &
                           * (2.0_wp * bigL + 1.0_wp)
         square_roots = SQRT(four_term_product)

         CALL dracah(2*bigL, 2*bigLp, 2*lee, 2*leep, 2, 2*lion, rac1)
         CALL dracah(2*lee, 2*leep, 2*lj, 2*ljp, 2, 2*lk, rac2)

         coeffs = cg(bigL, 1, bigLp, bigML, 0, bigMLp)*cg(lj, 1, ljp, 0, 0, 0) &
                 * rac1 * rac2

         region_three_int_ham = real_phase * imag_phase * square_roots * coeffs
      END IF
 
    END FUNCTION region_three_int_ham

!---------------------------------------------------------------------------

    !> \brief Sets up the angular part of the W_E interaction in region III.

    !> This function gives the angular part of the two-electron part (third term) in Eq.(48)
    !> in <a href="../double_ionization/RMT_double_ionization_theory.pdf">
    !> the double ionization theory pdf under /docs.</a>
    !> except the radial term, and the sum over k.

    !> \param lion  Initial angular momentum of the ion
    !> \param la    Initial angular momentum of electron 'a'
    !> \param lb    Initial angular momentum of electron 'b'
    !> \param lee   Initial total angular momentum of electrons 'a' and 'b'
    !> \param bigL  Initial total angular momentum of the system
    !> \param lionp Final angular momentum of the ion
    !> \param lap   Final angular momentum of electron 'a'
    !> \param lbp   Final angular momentum of electron 'b'
    !> \param leep  Final total angular momentum of electrons 'a' and 'b'
    !> \param bigLp Final total angular momentum of the system
    !>

    COMPLEX(wp) FUNCTION region_three_we_ham(k, lion, la, lb, lee, bigL, &
                                          &  lionp, lap, lbp, leep, bigLp)

      USE global_data, ONLY: im, zero

      INTEGER     :: lion, la, lb, lee, bigL
      INTEGER     :: lionp, lap, lbp, leep, bigLp
      INTEGER     :: k
      INTEGER     :: power
      REAL(wp)    :: coeffs, real_phase, square_roots
      REAL(wp)    :: two_term_product
      REAL(wp)    :: rac
      COMPLEX(wp) :: imag_phase

      region_three_we_ham = zero

      IF ( (bigL == bigLp) .AND. (lion == lionp) .AND. (lee == leep) ) THEN
      ! interaction between two outer electrons
         power = la + lbp + leep
         real_phase = (-1.0_wp)**(power)
         imag_phase = im**(la + lb - lap - lbp)

         two_term_product = (2.0_wp * la + 1.0_wp) * (2.0_wp * lb + 1.0_wp)
         square_roots = SQRT(two_term_product)

         CALL dracah(2*la, 2*lap, 2*lb, 2*lbp, 2*k, 2*lee, rac)

         coeffs = cg(la, k, lap, 0, 0, 0)*cg(lb, k, lbp, 0, 0, 0)*rac

         region_three_we_ham = real_phase * imag_phase * square_roots * coeffs
      END IF


    END FUNCTION region_three_we_ham

!---------------------------------------------------------------------------

    !> Angular part of the laser interaction Hamiltonian for helium.
    !> Taken from Meharg et al. J. Phys. B. 38 237 (2005), Eq.(25),
    !> with Fano-Racah phases included.

    !> \param la    Initial angular momentum of electron 'a'
    !> \param lb    Initial angular momentum of electron 'b'
    !> \param lee   Initial total angular momentum of electrons 'a' and 'b'
    !> \param ml    Initial magnetic quantum number of electrons 'a' and 'b'
    !> \param lap   Final angular momentum of electron 'a'
    !> \param lbp   Final angular momentum of electron 'b'
    !> \param leep  Final total angular momentum of electrons 'a' and 'b'
    !> \param mlp   Final magnetic quantum number of electrons 'a' and 'b'
    !>
    COMPLEX(wp) FUNCTION helium_int_ham(la, lb, lee, ml, lap, lbp, leep, mlp)

      USE global_data, ONLY: im, zero
 
      INTEGER     :: la, lb, lee, ml, lap, lbp, leep, mlp
      INTEGER     :: lj, lk, ljp, lkp
      INTEGER     :: power
      REAL(wp)    :: product_term, square_roots, real_phase
      REAL(wp)    :: rac, six_j, three_j_one, three_j_two
      COMPLEX(wp) :: imag_phase
 
      helium_int_ham = zero
 
      IF (lb == lbp) THEN
          lj = la ; ljp = lap ; lk = lb ; lkp = lbp
      ELSE IF (la == lap) THEN
          lj = lb ; ljp = lbp ; lk = la ; lkp = lap
      END IF
 
      IF ( (ml == mlp) .AND. ((la == lap) .OR. (lb ==lbp)) ) THEN
          power = lkp - mlp
          real_phase = (-1.0_wp)**power
          imag_phase = im**(lj + lk - ljp -lkp)
          product_term = (2.0_wp*leep + 1.0_wp) * (2.0_wp*lee + 1.0_wp) &
                    * (2.0_wp*ljp + 1.0_wp) * (2.0_wp*lj + 1.0_wp)
          square_roots = SQRT(product_term)
 
          three_j_one = (-1.0_wp)**(ljp-1) * cg(ljp, 1, lj, 0, 0, 0) /SQRT(2.0_wp*lj + 1.0_wp)
          three_j_two = (-1.0_wp)**(leep-lee) * cg(leep, lee, 1, mlp, ml, 0)/SQRT(3.0_wp)
 
          CALL dracah(2*leep, 2*lee, 2*ljp, 2*lj, 2, 2*lk, rac)
          six_j = (-1.0_wp)**(leep + lee + lj + ljp) * rac
 
          helium_int_ham = imag_phase * real_phase * square_roots * three_j_one * three_j_two * six_j
      END IF
 
    END FUNCTION helium_int_ham

!---------------------------------------------------------------------------

    !> Test the region III W_P matrix for the special case of helium. 
    !> Aborts execution if W_P matrix doesn't match expected
    SUBROUTINE test_wp_for_helium

        INTEGER     :: i, j
        INTEGER     :: la, lb, lee, ml, lap, lbp, leep, mlp
        INTEGER     :: nerr
        REAL(wp)    :: absdiff
        COMPLEX(wp) :: reduced_general_wp
        COMPLEX(wp) :: helium_wp

        nerr = 0
        reduced_general_wp = (0.0_wp, 0.0_wp)
        helium_wp = (0.0_wp, 0.0_wp)

        DO i = 1, two_electron_nchan
           la = first_l(i)
           lb = second_l(i)
           lee = two_electron_l(i)
           ml = tot_ml(i)
           DO j = 1, two_electron_nchan
              lap = first_l(j)
              lbp = second_l(j)
              leep = two_electron_l(j)
              mlp = tot_ml(j)
              reduced_general_wp = region_three_int_ham(   &
                                 &         0, la, lb, lee, lee, ml, &
                                 &         lap, lbp, leep, leep, mlp)
              helium_wp = helium_int_ham(la, lb, lee, ml, lap, lbp, leep, mlp)

              absdiff = ABS(reduced_general_wp - helium_wp)
              IF (absdiff > 1e-8) nerr = nerr + 1
           END DO
        END DO

        CALL assert(nerr == 0, 'Error in test_wp_for_helium')

    END SUBROUTINE test_wp_for_helium

!---------------------------------------------------------------------------

    !> Test to ensure both the region III  W_P and W_E matrices are symmetric w.r.t electron swap
    !> Aborts execution if either one is not.

    SUBROUTINE test_two_electron_symmetry

        INTEGER     :: i, j, k
        INTEGER     :: la, lb, l, lion, total_l, total_ml
        INTEGER     :: lap, lbp, lp, lionp, total_lp, total_mlp
        INTEGER     :: nerr_wp, nerr_we
        REAL(wp)    :: absdiff_wp, absdiff_we
        COMPLEX(wp) :: two_elec_wp_ham_no_swaps, two_elec_wp_ham_with_swaps
        COMPLEX(wp) :: two_elec_we_ham_no_swaps, two_elec_we_ham_with_swaps

        nerr_wp = 0; nerr_we = 0
        DO i = 1, two_electron_nchan
           la = first_l(i)
           lb = second_l(i)
           l  = two_electron_l(i)
           lion = ion_l(i)
           total_l = tot_l(i)
           total_ml = tot_ml(i)
           DO j = 1, two_electron_nchan
              lap = first_l(j)
              lbp = second_l(j)
              lp  = two_electron_l(j)
              lionp = ion_l(j)
              total_lp = tot_l(j)
              total_mlp = tot_ml(j)
              ! Test W_P for invariance under swap of la and lb
              two_elec_wp_ham_no_swaps = region_three_int_ham(       &
                                       lion, la, lb, l, total_l, total_ml,    &
                                       lap, lbp, lp, total_lp, total_mlp)
              two_elec_wp_ham_with_swaps = region_three_int_ham(     &
                                       lion, lb, la, l, total_l, total_ml,     &
                                       lbp, lap, lp, total_lp, total_mlp)

              absdiff_wp = ABS(two_elec_wp_ham_no_swaps - two_elec_wp_ham_with_swaps)
              IF (absdiff_wp > 1e-8) nerr_wp = nerr_wp + 1

              ! Test W_E for invariance under swap of la and lb
              DO k = 0, lamax
                 two_elec_we_ham_no_swaps = region_three_we_ham(k,   &
                                       lion, la, lb, l, total_l,     &
                                       lionp, lap, lbp, lp, total_lp)
                 two_elec_we_ham_with_swaps = region_three_we_ham(k, &
                                       lion, lb, la, l, total_l,     &
                                       lionp, lbp, lap, lp, total_lp)

                 absdiff_we = ABS(two_elec_we_ham_no_swaps - two_elec_we_ham_with_swaps)
                 IF (absdiff_we > 1e-8) nerr_we = nerr_we + 1
              END DO

           END DO
        END DO

        CALL assert(nerr_wp == 0, 'Error in symmetry properties of region III WP')
        CALL assert(nerr_we == 0, 'Error in symmetry properties of region III WE')

    END SUBROUTINE test_two_electron_symmetry

!---------------------------------------------------------------------------

    !> \brief Determine if a pair of two-electron channels (i and j) are coupled by
    !> the two-electron dipole operator.
    !> \date 2021
    !> 
    !> \param[in] i first two-electron channel
    !> \param[in] j second two-electron channel
    !>
    !      la    Initial angular momentum of the first electron
    !      lb    Initial angular momentum of the second electron
    !      lion  Initial residual dication angular momentum
    !      sion  Initial residual dication spin angular momentum
    !      lee   Initial two-electron angular momentum
    !      bigl  Initial total system angular momentum
    !
    !      lap   Final angular momentum of the first electron
    !      lbp   Final angular momentum of the second electron
    !      lionp Final residual dication angular momentum
    !      sionp Final residual dication spin angular momentum
    !      leep  Final two-electron angular momentum
    !      biglp Final total system angular momentum
    !
    !  ion_coupling            Check that the initial and final residual ion L and S 
    !                          are equal.
    !  one_electron_coupling   Check that the initial and final angular momenta of one
    !                          electron differ by one, while the other is unchanged.
    !  two_electron_coupling   Check that the initial and final angular momenta of 
    !                          the two outer electrons differ by one.
    !  total_symmetry_coupling Check that the initial and final total symmetries of the system
    !                          differ by one.
    !   verdict                Check that all the above coupling rules for the W_P interaction
    !                          in region III are satisfied.
 
    PURE LOGICAL FUNCTION region_three_wp_coupled(i, j) RESULT(verdict)

        USE readhd, ONLY: first_l, second_l, two_electron_l, ion_l, ion_s, tot_l

        INTEGER, INTENT(IN) :: i, j
        INTEGER             :: la, lb, lion, sion, lee, bigl
        INTEGER             :: lap, lbp, lionp, sionp, leep, biglp

        LOGICAL :: ion_coupling, one_electron_coupling, two_electron_coupling, total_symmetry_coupling

        la = first_l(i); lb = second_l(i)
        lion = ion_l(i); sion = ion_s(i)
        lee = two_electron_l(i); bigl = tot_l(i)

        lap = first_l(j); lbp = second_l(j)
        lionp = ion_l(j); sionp = ion_s(j)
        leep = two_electron_l(j); biglp = tot_l(j)

        ion_coupling = (lion == lionp) .AND. (sion == sionp)
        one_electron_coupling = (la == lap) .AND. (ABS(lb - lbp) == 1)
        one_electron_coupling = one_electron_coupling .OR. ((lb == lbp) .AND. (ABS(la - lap) == 1))
        two_electron_coupling = ABS(lee - leep) == 1
        total_symmetry_coupling = ABS(bigl - biglp) == 1

        verdict = ion_coupling .AND. one_electron_coupling .AND. two_electron_coupling .AND. total_symmetry_coupling

    END FUNCTION region_three_wp_coupled

!---------------------------------------------------------------------------

    !> \brief Determine if a pair of two-electron channels are coupled by
    !> the electron-electron repulsion operator in region III.
    !> \date 2021
    !> 
    !> \param[in] i     first (initial) two-electron channel
    !> \param[in] j     second (final) two-electron channel
    !>
    !     la    Initial angular momentum of the first electron
    !     lb    Initial angular momentum of the second electron
    !     lion  Initial residual dication angular momentum
    !     sion  Initial residual dication spin angular momentum
    !     lee   Initial two-electron angular momentum
    !     bigl  Initial total system angular momentum
    !
    !     lap   Final angular momentum of the first electron
    !     lbp   Final angular momentum of the second electron
    !     lionp Final residual dication angular momentum
    !     sionp Final residual dication spin angular momentum
    !     leep  Final two-electron angular momentum
    !     biglp Final total system angular momentum
    !
    ! ion_coupling            Check that the initial and final residual ion L and S 
    !                         are equal (for the two-electron part of W_E).
    ! two_electron_coupling   Check that the initial and final angular momenta of 
    !                         the two outer electrons are equal.
    ! total_symmetry_coupling Check that the initial and final total symmetries of the system
    !                         are equal.
    ! verdict                 Check that all the above coupling rules for the W_E interaction
    !                         in region III are satisfied.

    PURE LOGICAL FUNCTION region_three_we_coupled(i, j) RESULT(verdict)

        USE readhd, ONLY: first_l, second_l, two_electron_l, ion_s, tot_l

        INTEGER, INTENT(IN) :: i, j
        INTEGER             :: la, lb, lion, sion, lee, bigl
        INTEGER             :: lap, lbp, lionp, sionp, leep, biglp

        LOGICAL :: ion_coupling, two_electron_coupling, total_symmetry_coupling

        la = first_l(i); lb = second_l(i)
        lion = ion_l(i); sion = ion_s(i)
        lee = two_electron_l(i); bigl = tot_l(i)

        lap = first_l(j); lbp = second_l(j)
        lionp = ion_l(j); sionp = ion_s(j)
        leep = two_electron_l(j); biglp = tot_l(j)

        ion_coupling = (lion == lionp) .AND. (sion == sionp)     !For two-electron part
        two_electron_coupling = (lee == leep)
        total_symmetry_coupling = (bigl == biglp)

        verdict = ion_coupling .AND. two_electron_coupling .AND. total_symmetry_coupling

    END FUNCTION region_three_we_coupled

!---------------------------------------------------------------------------

    SUBROUTINE deallocate_lrpots

        INTEGER :: err

        DEALLOCATE (wdpot, wp2pot, cfuu, liuu, etuu, ksq, ltuu, lmuu, lpuu, lsuu, stat=err)
        IF (err /= 0) THEN
            PRINT *, 'deallocation error lrpots'
        END IF

        IF (dipole_velocity_output) THEN
            DEALLOCATE (wdpot_v, wp1pot, wp2pot_v, stat=err)
            IF (err /= 0) THEN
                PRINT *, 'deallocation error lrpots_v'
            END IF
        END IF

        IF (double_ionization) THEN
            DEALLOCATE (region_three_wemat, region_three_wpmat, stat=err)
            IF (err /= 0) THEN
                PRINT *, 'deallocation error region_three_mats'
            END IF
        END IF

    END SUBROUTINE deallocate_lrpots

END MODULE lrpots
