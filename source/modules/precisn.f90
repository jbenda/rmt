! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Standard R-matrix module for setting parameters related to the numerical
!> precision of the calculations.

MODULE precisn

    ! Definition of precision-related parameters
    ! from Martin Plummer, Nov 2009
    ! Revised and updated by Martin Plummer,Andrew Brown and Jimena Gorfinkiel, Nov 2017
    IMPLICIT NONE

    PUBLIC

    ! From ukrmol precisn module
    INTEGER, PARAMETER    :: rbyte = 8 ! is there a way of setting this automatically that's not compiler-dependent???
    ! Uncomment if needed (N.B. not used in the code currently)
!   LOGICAL, PARAMETER    :: real64 = .true.
!   INTEGER, PARAMETER    :: zbyte = 16

    ! From rmt precisn module
    ! Ensure real(wp) is same as real(wp) where wp is defined in module REAL_Types
    INTEGER, PARAMETER    :: decimal_precision_wp = 15

    ! Set kind type parameters.
    ! The argument to the function `selected_real_kind' is the
    ! requested minimum number of decimal digits of accuracy.
    INTEGER, PARAMETER    :: sp = SELECTED_REAL_KIND(6)   ! `single' precision
    INTEGER, PARAMETER    :: wp = SELECTED_REAL_KIND(12)  ! `double' precision
    INTEGER, PARAMETER    :: ep1 = SELECTED_REAL_KIND(19) ! `quad', extended, precision
    INTEGER, PARAMETER    :: ep = MAX(ep1, wp) ! extended precision if possible; double precision if not

    ! The use of `selected_int_kind' below is to ensure that the corresponding integer kind type
    ! parameters can support integers up to at least 10^9 and 10^10, respectively.
    INTEGER, PARAMETER    :: shortint = SELECTED_INT_KIND(9) ! i.e. a 32-bit signed integer
    INTEGER, PARAMETER    :: longint = SELECTED_INT_KIND(10) ! i.e. a 64-bit signed integer

    ! Uncomment if needed (N.B. not used in the code currently)
!   REAL(wp), PARAMETER   :: acc8 = 2.0e-16_wp
!   REAL(wp), PARAMETER   :: acc16 = 3.0e-33_ep
!   REAL(wp), PARAMETER   :: fpmax = 1.0e60_wp
!   REAL(wp), PARAMETER   :: fpmin = 1.0e-60_wp
!   REAL(wp), PARAMETER   :: fplmax = 140.0_wp
!   REAL(wp), PARAMETER   :: fplmin = -140.0_wp

END MODULE precisn
