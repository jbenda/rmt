! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Cleans up at the end of an RMT calculation. Frees memory, shuts down MPI
!> communications, and closes output files.

MODULE finalise

    IMPLICIT NONE

CONTAINS

    SUBROUTINE end_program

        USE mpi_communications,       ONLY: i_am_in_outer_region, &
                                            i_am_in_inner_region, &
                                            i_am_outer_master, &
                                            i_am_inner_master, &
                                            i_am_outer_rank1, &
                                            helium_finalize, &
                                            all_processor_barrier
        USE io_files,                 ONLY: close_output_files
        USE mpi_layer_lblocks,        ONLY: i_am_block_master
        USE lrpots,                   ONLY: deallocate_lrpots
        USE outer_propagators,        ONLY: deallocate_outer_propagators
        USE local_ham_matrix,         ONLY: dealloc_local_ham_matrix
        USE inner_propagators,        ONLY: deallocate_inner_propagators
        USE outer_to_inner_interface, ONLY: deallocate_fderivs
        USE inner_to_outer_interface, ONLY: dealloc_psi_at_inner_fd_pts
        USE setup_wv_data,            ONLY: deallocate_distribute_wv_data
        USE inner_parallelisation,    ONLY: deallocate_inner_region_parallelisation_2
        USE readhd,                   ONLY: deallocate_readhd
        USE kernel,                   ONLY: deallocate_psi_inner_on_grid
        USE wavefunction,             ONLY: deallocate_wavefunction

        CALL all_processor_barrier
        CALL close_output_files(i_am_outer_master, &
                                i_am_inner_master, i_am_outer_rank1)
        CALL deallocate_wavefunction
        CALL deallocate_psi_inner_on_grid

        ! Deallocations:
        IF (i_am_in_outer_region) THEN
            CALL deallocate_lrpots
            CALL deallocate_outer_propagators
            CALL dealloc_local_ham_matrix
        END IF

        IF (i_am_in_inner_region) THEN
            CALL deallocate_inner_propagators

            CALL deallocate_fderivs
!           CALL dealloc_setup_Lblock_comm_size
            CALL dealloc_psi_at_inner_fd_pts
            IF (i_am_block_master) CALL deallocate_distribute_wv_data
            CALL deallocate_inner_region_parallelisation_2
        END IF
        CALL deallocate_readhd(i_am_inner_master, &
                               i_am_in_outer_region)

        CALL helium_finalize

    END SUBROUTINE end_program

END MODULE finalise
