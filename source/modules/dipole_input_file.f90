! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief A piecemeal reimplementation of the code in `readhd`.
MODULE dipole_input_file
    ! NOTE: We define the d file in terms of explicit kinds to decouple RMT's working precision from the types used in the H file format.
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64

    USE initial_conditions, ONLY: jK_coupling_id, LS_coupling_id    !initial_conditions is setup upon build
    !WP allows us to work with at least 14 decimal digits accuracy and at least decimal exponents between - 300 and + 300
    USE precisn, ONLY: wp
    USE rmt_assert, ONLY: assert
    USE wall_clock, ONLY: time => hel_time

    IMPLICIT NONE

    !> \brief A derived type defining the format of D files.
    !>
    !> The structure of this type maps almost 1:1 with the structure of the D file itself. The structure is defined in terms of
    !> specified-width numeric types (i.e. `int32` and `real64`) because it is an external filetype and setting `wp` to a different
    !> width shouldn't affect the way the file is read.
    !>
    !> The names of the fields match what is currently being used in the code.
    !>
    !> # D File Format
    !>
    !> The 2 files each containing a series of [_Unformatted I/O Records_][scipy-FortranFile]. The format of these records is compiler-and
    !> machine-dependent, although most compilers will use a `<record bytes><record data><record bytes>` format and most machines
    !> will be little-endian and use IEEE 754 floating point numbers.
    !>
    !> The first file contains 2 32-bit integers followed by 2 1D arrays both of which are 64-bit real arrays.
    !> 
    !> The final record in this file defines the D block described as "blocks"
    !>
    !> The second file contains 13 32-bit integers and then 2 \f$3 \times n\f$ matrix, with \f$n\f$ specified by the integers "nev" & "nod".
    !>
    !> Lastly there are 2 more 32-bit integers.
    !>
    !> [scipy-FortranFile]: https://docs.scipy.org/doc/scipy-1.9.3/reference/generated/scipy.io.FortranFile.html


    TYPE D_file
        INTEGER(INT32):: ntarg
        INTEGER(INT32):: fintr
        REAL(REAL64), ALLOCATABLE :: crlv(:, :)
        REAL(REAL64), ALLOCATABLE :: crlv_v(:, :)
        TYPE(dipole_block), DIMENSION(:), ALLOCATABLE:: blocks
    END TYPE

    TYPE dipole_block
        INTEGER(INT32):: noterm, isi, inch, jsi, jnch   
        INTEGER(INT32):: nev, nod !number of even, number of odd
        INTEGER(INT32):: lgsf, lglf, lgpf, lgsi, lgli, lgpi ! quantum numbers initial/final
        REAL(REAL64), ALLOCATABLE :: dipsto(:, :) ! length gauge reduced dipole transition matrix
        REAL(REAL64), ALLOCATABLE :: dipsto_v(:, :) ! velocity gauge reduced dipole transition matrix
        INTEGER(INT32) :: iidip, ifdip
    END TYPE dipole_block

    INTEGER, PARAMETER           :: nocoupling = INT(b'000')
    INTEGER, PARAMETER           :: downcoupling = INT(b'001')
    INTEGER, PARAMETER           :: samecoupling = INT(b'010')
    INTEGER, PARAMETER           :: upcoupling = INT(b'100')

    PUBLIC :: read_D_file2
    contains 

    !> \brief Parse a D file into the 'read_D_file2' derived type.
    !>
    !> \param[in] d00_path The path to the header path. d_path The path to the data path. coupling_id Defines which coupling scheme to use.
    !> ntarg The number of target dipoles. dipole_velocity_output Bool to give velocity information as well as position information. nstmx The max number of state transitions.
    !> no_of_L_blocks The number of L blocks. no_of_LML_blocks Orbital magnetic quantum number for this L block. ML_max The maximum absolute value of ML to be used in the calculation..
    !> lplusp Defines the sum, modulo 2, of the angular momentum and parity of the ground state. debug Write extra debugging information to the screen at run-time. 
    !> xy_plane_desired Bool that changes the polarization plane to the xy plane. L_block_lrgl Large L L block. L_block_nspn L block for spin. L_block_npty number of L blocks with parity.
    !> LML_block_lrgl Orbital magnetic quantum number for L block with Large L. LML_block_nspn Orbital magnetic quantum number for L block with for spin.
    !> LML_block_npty Orbital magnetic quantum number for L block with parity. LML_block_ml Orbital magnetic quantum number for L block with the magnetic sublevel.
    !> mnp1 Number of states within a given symmetry. D The output of the parse subroutine with the structure of the D_file TYPE. dipole_coupled The direct interaction between 2 magnetic dipoles. block_ind Dipole block indices usable.
    !>

    subroutine parse_D_file(&
                            d00_path, &
                            d_path, &
                            coupling_id, &
                            ntarg, &
                            dipole_velocity_output, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            ML_max, &
                            lplusp, &
                            debug, &
                            xy_plane_desired, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_lrgl, &
                            LML_block_nspn, &
                            LML_block_npty, &
                            LML_block_ml, &
                            mnp1, &
                            D, &
                            dipole_coupled, &
                            block_ind)
        
        IMPLICIT NONE
        CHARACTER(len=*), INTENT(IN)         :: d00_path
        CHARACTER(len=*), INTENT(IN)         :: d_path
        INTEGER, INTENT(IN)                  :: coupling_id
        INTEGER, INTENT(IN)                  :: ntarg
        LOGICAL, INTENT(IN)                  :: dipole_velocity_output
        INTEGER, INTENT(IN)                  :: no_of_L_blocks
        INTEGER, INTENT(IN)                  :: no_of_LML_blocks
        INTEGER, INTENT(IN)                  :: ML_max
        INTEGER, INTENT(IN)                  :: lplusp
        LOGICAL, INTENT(IN)                  :: debug
        LOGICAL, INTENT(IN)                  :: xy_plane_desired(:)
        INTEGER, INTENT(IN)                  :: L_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: L_block_nspn(:)
        INTEGER, INTENT(IN)                  :: L_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: LML_block_nspn(:)
        INTEGER, INTENT(IN)                  :: LML_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE, INTENT(IN)                  :: mnp1(:)
        TYPE(D_file), INTENT(OUT)            :: D
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: block_ind(:, :, :)

        INTEGER  :: lgsf, lgsi, lgli, lglf, lgpi, lgpf, ni, nf, err, intr
        INTEGER  :: blocki, blockf, lgmli, lgmlf
        INTEGER  :: i, icount
        REAL(wp) :: bbb
        INTEGER  :: lptesti, lptestf, j
        INTEGER  :: lptermi, lptermf, stride
        INTEGER  :: io_header, io_data
    
        CALL assert(coupling_id.eq.LS_coupling_id,'jK Coupling cant be used with RMatrixII input data')

        OPEN (newunit = io_header, FILE=d00_path, STATUS='old', FORM='unformatted')
        OPEN (newunit = io_data, FILE=d_path, STATUS='old', FORM='unformatted')


        D%ntarg = ntarg
        ! Read target dipoles
        ALLOCATE (D%crlv(ntarg, ntarg))
        IF (dipole_velocity_output) THEN
            ALLOCATE (D%crlv_v(ntarg, ntarg))
        END IF

        READ (io_data) D%crlv(1:ntarg, 1:ntarg)  !length gauge reduced dipoles
        IF (dipole_velocity_output) THEN
            READ (io_data) D%crlv_v(1:ntarg, 1:ntarg) ! velocity dipoles
        ELSE
            READ (io_data)
        END IF

        IF (debug) THEN
            WRITE(*, *) 'length reduced targ moments'
            DO i = 1, ntarg
                DO j = 1, ntarg
                    WRITE (*, *) i, j, D%crlv(i, j)
                END DO
            END DO
        END IF

        ! Note form of d00 is Li - L(i-1)
        READ (io_header) intr  ! number of block transitions

        ALLOCATE (dipole_coupled(no_of_LML_blocks, no_of_LML_blocks))

        ALLOCATE (block_ind(no_of_L_blocks, no_of_L_blocks, 3))

        ALLOCATE (D%blocks(intr))

        icount = 0
        dipole_coupled = NoCoupling

        DO i = 1, 3*intr       ! If only S targets
            READ (io_header, IOSTAT=err) lgsf, lglf, lgpf, lgsi, lgli, lgpi
            IF (debug) THEN
                WRITE (*, *) i
                WRITE (*, *) lgsf, lglf, lgpf, lgsi, lgli, lgpi
            END IF

            IF (err < 0) THEN ! end of file reached
                EXIT
            END IF
            CALL assert(err .LE. 0, 'ERROR READING FROM d FILE')

            ! i refers now to the LEFT-HAND SIDE
            ! f to the RIGHT-HAND SIDE
            lptesti = MOD(lgli + lgpi, 2)
            lptestf = MOD(lglf + lgpf, 2)
            IF ((lgpf <= 1) .AND. (lgpi <= 1)) THEN
                IF (ML_max .EQ. 0 .AND. (lplusp == lptesti) .AND. (lplusp == lptestf) .OR. ML_max .GT. 0) THEN
                    CALL finind_L(lgsf, lglf, lgpf, nf, no_of_L_blocks, L_block_nspn, L_block_lrgl, L_block_npty)
                    CALL finind_L(lgsi, lgli, lgpi, ni, no_of_L_blocks, L_block_nspn, L_block_lrgl, L_block_npty)
                    if ((ni==-1) .or. (nf==-1)) then
                        cycle
                    end if
                    icount = icount + 1
                    ASSOCIATE (bloc => D%blocks(icount))
                    bloc%lgsf = lgsf
                    bloc%lglf = lglf
                    bloc%lgpf = lgpf
                    bloc%lgsi = lgsi
                    bloc%lgli = lgli
                    bloc%lgpi = lgpi
                    bloc%iidip = ni
                    bloc%ifdip = nf
                    block_ind(ni, nf, :) = icount
                    block_ind(nf, ni, :) = 0
                    bloc%nev = mnp1(ni)
                    bloc%nod = mnp1(nf)

                    IF (debug) WRITE (*, *)  'nrl=', bloc%nev, 'ncl=', bloc%nod

                    allocate(bloc%dipsto(bloc%nev, bloc%nod))
                    IF (dipole_velocity_output) THEN
                        allocate(bloc%dipsto_v(bloc%nev, bloc%nod))
                    END IF
                    
                    ! LENGTH GAUGE USED THROUGHOUT
                    READ (io_data) bloc%noterm, bloc%isi, bloc%inch, bloc%jsi, bloc%jnch
                    IF (debug) WRITE (*, *) bloc%noterm, bloc%isi, bloc%inch, bloc%jsi, bloc%jnch


                    READ (io_data) bloc%dipsto(1:bloc%nev, 1:bloc%nod)
                    IF (dipole_velocity_output) THEN
                        READ (io_data) bloc%dipsto_v(1:bloc%nev, 1:bloc%nod)
                        IF (debug) WRITE (*, *) 'Check ', bloc%dipsto(1, 1), bloc%dipsto_v(1, 1)
                    ELSE
                        READ (io_data) bbb
                        IF (debug) WRITE (*, *) 'Check ', bloc%dipsto(1, 1), bbb
                    END IF
                    END ASSOCIATE

                ELSE IF (ML_max .EQ. 0 .AND. (lplusp .NE. lptesti) .OR. ML_max .EQ. 0 .AND. (lplusp .NE. lptestf)) THEN

                    ! Values for ml not compatible
                    READ (io_data)
                    READ (io_data)
                    READ (io_data)
                END IF

                !>  stride = 2 in xy plane since every other ML value is dipole accessible.
                !>  stride = 1 in all other polarization planes since all ML values are accessible.
                !>
                !>  lptermi,lptermf determine the range of ML values retained for each L.
                !>  These depend on both L and Parity, similar to the lpfact term used earlier.
                !>   
                stride = MERGE(2,1,xy_plane_desired(1))
                lptermi = MERGE(MOD(lgli+lgpi,2),0,xy_plane_desired(1))
                lptermf = MERGE(MOD(lglf + lgpf,2),0,xy_plane_desired(1))

                IF (ML_max > 0 .OR. ML_max == 0 .AND. (lptesti == lplusp) .AND. (lptestf == lplusp)) THEN
                    DO lgmli = -MIN(lgli, ML_max) + lptermi, MIN(lgli, ML_max) - lptermi, stride
                        DO lgmlf = -MIN(lglf, ML_max) + lptermf, MIN(lglf, ML_max) - lptermf, stride
                            IF (lgmli == lgmlf .OR. ABS(lgmli - lgmlf) == 1) THEN
                                CALL finind_LML(lgsi, lgli, lgpi, lgmli, blocki, &
                                                no_of_LML_blocks, LML_block_nspn, LML_block_lrgl, LML_block_npty, LML_block_ml)
                                CALL finind_LML(lgsf, lglf, lgpf, lgmlf, blockf, &
                                                no_of_LML_blocks, LML_block_nspn, LML_block_lrgl, LML_block_npty, LML_block_ml)
                                IF (blocki == -1) WRITE (*, *) 'vals:', lgsi, lgli, lgpi, lgmli, blocki
                                IF (lglf < lgli) THEN
                                    dipole_coupled(blockf, blocki) = downcoupling
                                    dipole_coupled(blocki, blockf) = upcoupling
                                END IF
                                IF (lglf == lgli) THEN
                                    dipole_coupled(blockf, blocki) = samecoupling
                                    dipole_coupled(blocki, blockf) = samecoupling
                                END IF
                                IF (lglf > lgli) THEN
                                    dipole_coupled(blockf, blocki) = upcoupling
                                    dipole_coupled(blocki, blockf) = downcoupling
                                END IF
                            END IF
                        END DO
                    END DO
                END IF

            END IF
            
        END DO

        



        CLOSE (io_header)
        CLOSE (io_data)
        D%fintr = icount
        
        IF (debug) THEN
            WRITE (*, *)  'fintr in Read_D_File = ', D%fintr
            WRITE (*, *)  'intr in Read_D_File = ', intr
        END IF
        
    end subroutine parse_D_file

    !> \brief Read data that the code needs from the D file.
    !> 
    !> \param[in] d00_path The path to the header path. d_path The path to the data path. coupling_id Defines which coupling scheme to use.
    !> ntarg The number of target dipoles. dipole_velocity_output Bool to give velocity information as well as position information. nstmx The max number of state transitions.
    !> no_of_L_blocks The number of L blocks. no_of_LML_blocks Orbital magnetic quantum number for this L block. ML_max The maximum absolute value of ML to be used in the calculation..
    !> lplusp Defines the sum, modulo 2, of the angular momentum and parity of the ground state. debug Write extra debugging information to the screen at run-time. 
    !> xy_plane_desired Bool that changes the polarization plane to the xy plane. L_block_lrgl Large L L block. L_block_nspn L block for spin. L_block_npty number of L blocks with parity.
    !> LML_block_lrgl Orbital magnetic quantum number for L block with Large L. LML_block_nspn Orbital magnetic quantum number for L block with for spin.
    !> LML_block_npty Orbital magnetic quantum number for L block with parity. LML_block_ml Orbital magnetic quantum number for L block with the magnetic sublevel.
    !> crlv Target dipole elements in the length gauge. crlv_v Target dipole elements in the velocity gauge. iidip Indexof initial dipole. ifdip Index of final dipole. dipsto Length gauge reduced dipole transition matrix. dipsto_v Velocity gauge reduced dipole transition matrix. 
    !> mnp1 Number of states within a given symmetry. dipole_coupled The direct interaction between 2 magnetic dipoles. block_ind Dipole block indices usable.
    !> fintr Total number of unique dipole blocks.
    !> 
    !> This function retieves the data from `parse_D_file`and allocates the remaining data for each unique dipole block.
    
    subroutine read_D_file2(&
                            d00_path, &
                            d_path, &
                            coupling_id, &
                            ntarg, &
                            dipole_velocity_output, & 
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            ML_max, &
                            lplusp, &
                            debug, &
                            xy_plane_desired, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_lrgl, &
                            LML_block_nspn, &
                            LML_block_npty, &
                            LML_block_ml, &
                            crlv, &
                            crlv_v, &
                            iidip, &
                            ifdip, &
                            dipsto, &
                            dipsto_v, &
                            dipole_coupled, &
                            block_ind, &
                            mnp1, &
                            fintr)
        
        IMPLICIT NONE
        CHARACTER(len=*), INTENT(IN)         :: d00_path
        CHARACTER(len=*), INTENT(IN)         :: d_path
        INTEGER, INTENT(IN)                  :: coupling_id
        INTEGER, INTENT(IN)                  :: ntarg
        LOGICAL, INTENT(IN)                  :: dipole_velocity_output 
        INTEGER, INTENT(IN)                  :: nstmx
        INTEGER, INTENT(IN)                  :: no_of_L_blocks
        INTEGER, INTENT(IN)                  :: no_of_LML_blocks
        INTEGER, INTENT(IN)                  :: ML_max
        INTEGER, INTENT(IN)                  :: lplusp
        LOGICAL, INTENT(IN)                  :: debug
        LOGICAL, INTENT(IN)                  :: xy_plane_desired(:)
        INTEGER, INTENT(IN)                  :: L_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: L_block_nspn(:)
        INTEGER, INTENT(IN)                  :: L_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: LML_block_nspn(:)
        INTEGER, INTENT(IN)                  :: LML_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_ml(:)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: crlv(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: crlv_v(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: iidip(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: ifdip(:)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: dipsto(:, :, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: dipsto_v(:, :, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT)  :: block_ind(:, :, :)
        INTEGER, ALLOCATABLE, INTENT(IN)     :: mnp1(:)
        INTEGER, INTENT(INOUT)               :: fintr

        TYPE(D_file) :: D
        INTEGER :: ii

        CALL parse_D_file(&
                          d00_path, &
                          d_path, &
                          coupling_id, &
                          ntarg, &
                          dipole_velocity_output, & 
                          no_of_L_blocks, &
                          no_of_LML_blocks, &
                          ML_max, &
                          lplusp, &
                          debug, &
                          xy_plane_desired, &
                          L_block_lrgl, &
                          L_block_nspn, &
                          L_block_npty, &
                          LML_block_lrgl, &
                          LML_block_nspn, &
                          LML_block_npty, &
                          LML_block_ml, &
                          mnp1, &
                          D, &
                          dipole_coupled, &
                          block_ind)

        fintr = D%fintr
        ALLOCATE (crlv(D%ntarg, D%ntarg))
        ALLOCATE (dipsto(nstmx, nstmx, fintr))
        ALLOCATE (iidip(fintr))
        ALLOCATE (ifdip(fintr))
        crlv = D%crlv

        IF (dipole_velocity_output) THEN
            ALLOCATE (crlv_v(D%ntarg, D%ntarg))
            ALLOCATE (dipsto_v(nstmx, nstmx, fintr))
            crlv_v = D%crlv_v
        END IF

        DO ii=1, fintr

            dipsto(1:D%blocks(ii)%nev, 1:D%blocks(ii)%nod, ii) = D%blocks(ii)%dipsto(:, :)
            iidip(ii) = D%blocks(ii)%iidip
            ifdip(ii) = D%blocks(ii)%ifdip
            IF (dipole_velocity_output) THEN
                dipsto_v(1:D%blocks(ii)%nev, 1:D%blocks(ii)%nod, ii) = D%blocks(ii)%dipsto_v(:, :)
            END IF

        END DO

    end subroutine read_D_file2

 


    !> FIND SYMMETRY INDEX
    SUBROUTINE finind_L(lgsf, lglf, lgpf, nf, no_of_L_blocks, L_block_nspn, L_block_lrgl, L_block_npty)

        INTEGER, INTENT(IN)  :: lgsf, lglf, lgpf
        INTEGER, INTENT(IN)  :: no_of_L_blocks, L_block_nspn(:), L_block_lrgl(:), L_block_npty(:)
        INTEGER, INTENT(OUT) :: nf
        INTEGER :: i

        nf = -1
        DO i = 1, no_of_L_blocks  !inast
            IF (lgsf == L_block_nspn(i)) THEN
                IF (lglf == L_block_lrgl(i)) THEN
                    IF (lgpf == L_block_npty(i)) THEN
                        nf = i
                    END IF
                END IF
            END IF
        END DO

    END SUBROUTINE finind_L

!---------------------------------------------------------------------------

    !> FIND SYMMETRY INDEX (LML BLOCKS) ADDED BY DDAC
    SUBROUTINE finind_LML(lgs, lgl, lgp, lgml, sym_no, &
                          no_of_LML_blocks, LML_block_nspn, LML_block_lrgl, LML_block_npty, LML_block_ml)

        INTEGER, INTENT(IN)  :: lgs, lgl, lgp, lgml
        INTEGER, INTENT(IN)  :: no_of_LML_blocks, LML_block_nspn(:), LML_block_lrgl(:)
        INTEGER, INTENT(IN)  :: LML_block_npty(:), LML_block_ml(:)
        INTEGER, INTENT(OUT) :: sym_no
        INTEGER :: i

        sym_no = -1

        DO i = 1, no_of_LML_blocks
            IF (lgs == LML_block_nspn(i)) THEN
                IF (lgl == LML_block_lrgl(i)) THEN
                    IF (lgp == LML_block_npty(i)) THEN
                        IF (lgml == LML_block_ml(i)) THEN
                            sym_no = i
                        END IF
                    END IF
                END IF
            END IF
        END DO

    END SUBROUTINE finind_LML
END MODULE dipole_input_file
