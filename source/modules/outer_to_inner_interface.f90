! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Controls the flow of information from the outer to inner region,
!> specifically the calculation of the boundary amplitudes for propagation of
!> the outer region (grid) wavefunction into the inner region basis
!> representation.
!> Note that a separate module,@ref inner_to_outer_interface, handles the
!> flow of information in the other direction.

MODULE outer_to_inner_interface

    USE precisn,    ONLY: wp
    USE rmt_assert, ONLY: assert
    USE readhd,     ONLY: LML_block_tot_nchan
    USE initial_conditions, only: no_of_pes_per_sector 
    USE initial_conditions, ONLY: numsols => no_of_field_confs
    USE mpi_communications, only: mpi_comm_0_outer_block, block_comm_channel_remainder
    
    USE MPI

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE  :: fderivs(:, :)
    real(wp), allocatable     :: fderiv_ri(:,:)

    PUBLIC send_fderivs_outer_to_inner
    PUBLIC get_fderivs_and_project
    PUBLIC allocate_fderivs
    PUBLIC deallocate_fderivs

CONTAINS

    SUBROUTINE send_fderivs_outer_to_inner(fderivs_ext)
        USE mpi_communications, only: mpi_comm_0_outer_block, get_my_block_group_pe_id, get_my_group_pe_id, &
                                      disp_for_pe, counts_per_pe, gather_cmplx_array_from_first_outer_block, &
                                      send_cmplx_array, get_my_pe_id
        USE grid_parameters,    only: my_num_channels, my_channel_id_1st, my_channel_id_last
        USE initial_conditions, only: no_of_pes_per_sector

        IMPLICIT NONE

        COMPLEX(wp), INTENT(INOUT) :: fderivs_ext(my_channel_id_1st:my_channel_id_last, 1:numsols) ! inout needed but unaltered

        INTEGER :: reqd_id_of_inner_pe, tag, ierror, my_group_pe_id, my_pe_id, num_of_elements
        INTEGER :: send_length_of_array, my_block_group_pe_id, recv_pe, length_of_array, isol, number_of_pes  
        
        ! Find out which inner region processor handles this propagation order
!       Reqd_ID_of_Inner_PE = Master_Inner_PE_ID_for_Order(k1)
        Reqd_ID_of_Inner_PE = 0 ! perhaps should be PE_ID_1st?

        CALL get_my_group_pe_id(my_group_pe_id)
        CALL get_my_pe_id(my_pe_id)
        IF (no_of_pes_per_sector == 1) THEN 
        
           ! Send data
           tag = my_pe_id  ! the tag is assumed to be the same as the sending pe id in the communicator
           num_of_elements = LML_block_tot_nchan * numsols
           CALL send_cmplx_array(fderivs_ext, num_of_elements, reqd_id_of_inner_pe, tag, &
                MPI_COMM_WORLD, ierror)
!           CALL MPI_SEND(fderivs_ext, LML_block_tot_nchan*numsols, MPI_DOUBLE_COMPLEX, &
!                         reqd_id_of_inner_pe, tag, MPI_COMM_WORLD, ierror)
        ELSE
           call get_my_block_group_pe_id(my_block_group_pe_id)
           call get_my_group_pe_id(my_group_pe_id)
           
           length_of_array = my_num_channels
           number_of_pes = no_of_pes_per_sector + 1
           send_length_of_array = length_of_array
           recv_pe = Reqd_id_of_inner_pe
           
           DO isol = 1, numsols
              CALL gather_cmplx_array_from_first_outer_block(number_of_pes, send_length_of_array, &
                                                             counts_per_pe, length_of_array, &
                                                             fderivs_ext(:,isol), recv_pe, &
                                                             disp_for_pe, mpi_comm_0_outer_block)
           END DO
        END IF
           
    END SUBROUTINE send_fderivs_outer_to_inner

!---------------------------------------------------------------------

    SUBROUTINE get_fderivs_and_project(k1, vecout_ri)

        USE initial_conditions,        ONLY: timings_desired, no_of_pes_per_sector
        USE communications_parameters, ONLY: id_of_1st_pe_outer
        USE distribute_hd_blocks2,     ONLY: numrows_sum
        USE mpi_communications,        ONLY: get_my_pe_id, mpi_comm_0_outer_block, disp_for_pe, counts_per_pe, &
                                             gather_cmplx_array_from_first_outer_block, receive_cmplx_array
        USE initial_conditions,        ONLY: no_of_pes_per_sector
        USE wall_clock,                ONLY: update_time_start_inner, &
                                             update_time_end_inner, update_time_test, update_time_testb

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: k1
        REAL(wp), INTENT(OUT)       :: vecout_ri(numrows_sum, 2 * numsols)

        INTEGER :: my_rank, reqd_id_of_inner_pe, ierror, num_of_elements
        INTEGER :: send_length_of_array, my_block_group_pe_id, recv_pe, length_of_array, isol, number_of_pes  

        fderivs = (0.0_wp, 0.0_wp)

        ! Get my rank
        CALL get_my_pe_id(my_rank)

        ! Find out which inner region processor handles this propagation order
        reqd_id_of_inner_pe = 0! Master_Inner_PE_ID_for_Order(k1)

        ! Receive data from outer region:
        IF (my_rank == reqd_id_of_inner_pe) THEN

            ! Timers
            IF (timings_desired) THEN
                CALL update_time_start_inner(k1)
            END IF

            IF (no_of_pes_per_sector == 1) THEN
               num_of_elements = LML_block_tot_nchan * numsols
               CALL receive_cmplx_array(fderivs, num_of_elements, id_of_1st_pe_outer, MPI_COMM_WORLD, ierror)
!               tag = 3
!               CALL MPI_RECV(fderivs, LML_block_tot_nchan*numsols, MPI_DOUBLE_COMPLEX, &
!                          id_of_1st_pe_outer, tag, MPI_COMM_WORLD, status, ierror)
            ELSE
               length_of_array = LML_block_tot_nchan
               number_of_pes = no_of_pes_per_sector + 1
               send_length_of_array =  0
               recv_pe = Reqd_id_of_inner_pe
               DO isol = 1, numsols
                  CALL gather_cmplx_array_from_first_outer_block(number_of_pes, send_length_of_array, &
                                                                 counts_per_pe, length_of_array, &
                                                                 fderivs(:,isol), recv_pe, &
                                                                 disp_for_pe, mpi_comm_0_outer_block)
                END DO
            END IF
            ! Timers
            IF (timings_desired) THEN
                CALL update_time_end_inner
            END IF

        END IF

        IF (my_rank == reqd_id_of_inner_pe .and. timings_desired) THEN
           call update_time_testb
        END IF

        CALL bcast_fderivs_to_Lb_masters

        vecout_ri = 0.0_wp
        CALL project_fderivs_onto_surf_amps(vecout_ri)
        IF (my_rank == reqd_id_of_inner_pe .and. timings_desired) THEN
           call update_time_test
        END IF

    END SUBROUTINE get_fderivs_and_project

!---------------------------------------------------------------------

    SUBROUTINE bcast_fderivs_to_Lb_masters

        USE mpi_communications, ONLY: mpi_comm_region

        IMPLICIT NONE

        INTEGER   :: ierr, j, isol, num1, num2


        CALL MPI_BCAST(fderivs, LML_block_tot_nchan*numsols, MPI_DOUBLE_COMPLEX, 0, mpi_comm_region, ierr)

        DO isol = 1, numsols
            num1 = (isol - 1) * 2 +1
            num2 = num1 + 1
            DO j = 1, LML_block_tot_nchan
               fderiv_ri(j,num1) = - AIMAG(fderivs(j,isol))
            END DO
            DO j = 1, LML_block_tot_nchan
               fderiv_ri(j,num2) = REAL(fderivs(j,isol),wp)
            END DO   
        END DO    
        
    END SUBROUTINE bcast_fderivs_to_Lb_masters

!---------------------------------------------------------------------

    SUBROUTINE project_fderivs_onto_surf_amps(vecout_ri)

        USE distribute_hd_blocks2, ONLY: numrows_sum, numrows_m, re_surf_amps
!        USE mpi_layer_lblocks, ONLY: inner_region_rank

        IMPLICIT NONE

        REAL(wp), INTENT(OUT)      :: vecout_ri(numrows_sum, 2 * numsols)

        REAL(wp), PARAMETER        :: rhalf = 0.5_wp
        REAL(wp), PARAMETER        :: rzero = 0.0_wp

        IF (numrows_sum > 0) THEN
            CALL DGEMM('N', 'N', numrows_sum, 2*numsols, LML_block_tot_nchan, rhalf, re_surf_amps, &
                     numrows_sum, fderiv_ri, LML_block_tot_nchan, rzero, vecout_ri, numrows_sum)
        END IF

    END SUBROUTINE project_fderivs_onto_surf_amps

!---------------------------------------------------------------------

    SUBROUTINE allocate_fderivs

        IMPLICIT NONE

        INTEGER  :: err

        ALLOCATE (fderivs(LML_block_tot_nchan, numsols), stat=err)
        ALLOCATE (fderiv_ri(LML_block_tot_nchan, 2*numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Fderivs')

    END SUBROUTINE allocate_fderivs

!---------------------------------------------------------------------

    SUBROUTINE deallocate_fderivs

        IMPLICIT NONE

        INTEGER  :: err

        DEALLOCATE (fderivs, stat=err)
        DEALLOCATE (fderiv_ri, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with Fderivs')

    END SUBROUTINE deallocate_fderivs

END MODULE outer_to_inner_interface
