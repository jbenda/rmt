! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Controls the flow of information from the inner to outer region,
!> specifically the matching of the wavefunction on the inner region grid
!> points. Note that a separate module, outer_to_inner_interface handles the
!> flow of information in the other direction.
MODULE inner_to_outer_interface

    USE precisn,    ONLY: wp
    USE readhd,     ONLY: LML_block_tot_nchan
    USE rmt_assert, ONLY: assert
    USE initial_conditions, ONLY: numsols => no_of_field_confs
    USE MPI

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE    :: psi_at_inner_fd_pts(:, :, :)

    PUBLIC get_psi_at_inner_fd_pts
    PUBLIC alloc_psi_at_inner_fd_pts
    PUBLIC dealloc_psi_at_inner_fd_pts
    PUBLIC get_outer_initial_state_at_b
    PUBLIC send_psi_at_inner_fd_pts
    PUBLIC recv_psi_at_inner_fd_pts
    PUBLIC share_psi_at_inner_fd_pts_with_first_outer
    PUBLIC psi_at_inner_fd_pts
    
CONTAINS

    SUBROUTINE get_psi_at_inner_fd_pts(nfdm, vecin)

        USE distribute_hd_blocks2, ONLY: rowbeg, rowend, numrows, numrows_sum, numrows_blocks
        USE distribute_wv_data,    ONLY: ib_surfs, my_nchan, wvo_counts, wvo_disp
        USE global_data,           ONLY: zero
        USE mpi_layer_lblocks,     ONLY: i_am_block_master, Lb_m_comm, Lb_comm, lb_size, lb_m_rank
        USE mpi_layer_lblocks,     ONLY: my_num_LML_blocks, inner_region_rank
        USE readhd,                ONLY: max_L_block_size
        use mpi_communications, only: get_my_pe_id
        
        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: nfdm
        COMPLEX(wp), INTENT(IN)     :: vecin(numrows_blocks, 1:numsols)  
        COMPLEX(wp), allocatable    :: wv(:,:,:)         ! my_nchan is now an array
        COMPLEX(wp), allocatable    :: my_wv(:,:,:)      ! my_nchan is now an array
        INTEGER                     :: rowbeg_v(my_num_LML_blocks), rowend_v(my_num_LML_blocks), i_block    
        INTEGER                     :: i, ierr, j, jj, isol, j_cumul, j2, my_nchan_sum, i_dummy, j_dummy
        integer :: my_rank
        
        my_nchan_sum = SUM(my_nchan(1:my_num_LML_blocks))
        ALLOCATE (wv(nfdm, my_nchan_sum, numsols), my_wv(nfdm, my_nchan_sum, numsols), stat=ierr)
        call assert (ierr == 0, 'allocation of wv, my_wv in inner_to_outer_interface')
         
        call get_my_pe_id(my_rank)
        IF (my_num_LML_blocks > 1) THEN
           rowbeg_v(1) = 1
           rowend_v(1) = numrows(1)
           DO i_block = 2, my_num_LML_blocks 
              rowbeg_v(i_block) = rowend_v(i_block-1) + 1
              rowend_v(i_block) = rowend_v(i_block-1) + numrows(i_block)
           END DO
           CALL assert (rowend_v(my_num_LML_blocks) == numrows_sum, 'numrows_sum inconsistency in get_Psi_at_inner_fd_points')
        ELSE
           rowbeg_v(1) = rowbeg
           rowend_v(1) = rowend
        END IF   

        DO isol = 1, numsols
           j_cumul = 0
           DO i_block = 1, my_num_LML_blocks
              DO j = 1, my_nchan(i_block)
                 j2 = j + j_cumul
                 DO i = 1, nfdm
                    wv(i, j2, isol) = zero
                    my_wv(i, j2, isol) = zero
                    DO jj = rowbeg_v(i_block), rowend_v(i_block) ! now breaking up into sub-blocks
                       my_wv(i, j2, isol) = my_wv(i, j2, isol) + ib_surfs(jj, j, i)*vecin(jj, isol) ! vecin to
                    END DO
                 END DO
              END DO
              j_cumul = j_cumul + my_nchan(i_block)
           END DO
        END DO

        ! sum contributions from each sub-block onto block master
        IF (my_nchan(1) /= 0 .and. lb_size > 1) THEN
            CALL MPI_REDUCE(my_wv, wv, nfdm*my_nchan(1)*numsols, MPI_DOUBLE_COMPLEX, MPI_SUM, 0, Lb_comm, ierr)
        ELSE
            wv = my_wv    
        END IF

        IF (i_am_block_master) THEN
            DO isol = 1, numsols
               CALL MPI_GATHERV(wv(:, :, isol), nfdm*my_nchan_sum, MPI_DOUBLE_COMPLEX, &
                    psi_at_inner_fd_pts(:, :, isol), wvo_counts, wvo_disp, MPI_DOUBLE_COMPLEX, 0, &
                    Lb_m_comm, ierr)
            END DO
        END IF
        DEALLOCATE (my_wv, wv, stat=ierr)
        CALL assert (ierr == 0, 'deallocation of wv, my_wv in inner_to_outer_interface')

    END SUBROUTINE get_psi_at_inner_fd_pts

!-----------------------------------------------------------------------

    SUBROUTINE alloc_psi_at_inner_fd_pts(nfdm)

        IMPLICIT NONE

        INTEGER, INTENT(IN)    :: nfdm
        INTEGER                :: err

        ALLOCATE (psi_at_inner_fd_pts(nfdm, LML_block_tot_nchan, numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with psi_at_inner_fd_pts')

    END SUBROUTINE alloc_psi_at_inner_fd_pts

!-----------------------------------------------------------------------

    SUBROUTINE dealloc_psi_at_inner_fd_pts

        IMPLICIT NONE

        INTEGER   :: err

        DEALLOCATE (psi_at_inner_fd_pts, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with psi_at_inner_fd_pts')

    END SUBROUTINE dealloc_psi_at_inner_fd_pts

!-----------------------------------------------------------------------
! INITIAL VALUE OF THE OUTER WAVEFUNCTION AT r=b
!----------------------------------------------------------------------- 

    SUBROUTINE get_outer_initial_state_at_b(psi_outer_at_b, psi_inner, simple_start)

        USE distribute_hd_blocks,  ONLY: my_surf_amps
        USE distribute_hd_blocks2, ONLY: numrows_blocks
        USE readhd,                ONLY: max_L_block_size
        USE mpi_layer_lblocks,     ONLY: my_ground_state_block
        
        COMPLEX(wp), INTENT(INOUT) :: psi_outer_at_b(1:LML_block_tot_nchan, 1:numsols)
        COMPLEX(wp), INTENT(IN)    :: psi_inner(1:numrows_blocks, 1:numsols)
        LOGICAL, INTENT(IN)        :: simple_start   ! note that, if true,
! psi_inner as input is set to = 1.0 for psi_inner(1,1:numsols) and 0.0 for psi_inner(2:,1:numsols)
        INTEGER                    :: channel_id, isol

        ! On the boundary f^gamma_p(b,t)= sum_k C^gamma_k w^gamma_kp
        ! where gamma labels symmetries, p labels channels, k labels eigenstates in inner basis
        ! C=coefficients of inner region basis functions, w_kp=surface amplitudes

        IF (simple_start) THEN
           DO isol = 1, numsols
               DO channel_id = 1, LML_block_tot_nchan
                  psi_outer_at_b(channel_id, isol) = &
                       my_surf_amps(1, channel_id, my_ground_state_block)
                  ! assumes that ground state is in the lowest LML_block
! 'deadly hack': see also the assumption in the calling routine (psi_inner(1,isol) = 1.0_wp, all other comonents zero).
               END DO
           END DO
        ELSE 
!           DO isol = 1, numsols
!               DO channel_id = 1, LML_block_tot_nchan
!                  psi_outer_at_b(channel_id, isol) = DOT_PRODUCT(psi_inner(:, isol), &
!                       my_surf_amps(:, channel_id, my_ground_state_block))
!                  ! assumes ground state
!               END DO
!           END DO
        END IF    
       

    END SUBROUTINE get_outer_initial_state_at_b

!-----------------------------------------------------------------------

    SUBROUTINE send_psi_at_inner_fd_pts(nfdm, k1)

        USE communications_parameters, ONLY: id_of_1st_pe_outer, pe_id_1st
        USE mpi_communications,        ONLY: get_my_pe_id, send_cmplx_array
        USE wall_clock,                ONLY: update_time_start_inner, &
                                             update_time_end_inner
        USE initial_conditions, ONLY       : timings_desired

        INTEGER, INTENT(IN) :: nfdm
        INTEGER, intent(IN) :: k1
        INTEGER             :: tag, ierror, my_rank
        integer :: num_of_elements
        
        ! Get my rank
        CALL get_my_pe_id(my_rank)

        ! If I am the master PE in the inner region then send data to outer region:
        IF (my_rank == pe_id_1st) THEN

            IF (timings_desired) THEN
                CALL update_time_start_inner(k1)
            END IF
            tag = my_rank
            num_of_elements = nfdm * LML_block_tot_nchan * numsols
            CALL send_cmplx_array(psi_at_inner_fd_pts, num_of_elements, id_of_1st_pe_outer, tag, &
                 MPI_COMM_WORLD, ierror)
!            CALL MPI_SEND(psi_at_inner_fd_pts, nfdm*LML_block_tot_nchan*numsols, MPI_DOUBLE_COMPLEX, &
!                          id_of_1st_pe_outer, tag, MPI_COMM_WORLD, ierror)
            IF (timings_desired) THEN
                CALL update_time_end_inner
            END IF
        END IF

    END SUBROUTINE send_psi_at_inner_fd_pts

!-----------------------------------------------------------------------

    SUBROUTINE recv_psi_at_inner_fd_pts(nfdm, number_channels, numsols, vecout)

        USE communications_parameters, ONLY: id_of_1st_pe_outer, pe_id_1st
        USE initial_conditions,        ONLY: no_of_pes_per_sector 
        USE mpi_communications,        ONLY: get_my_pe_id, receive_cmplx_array !&,
        !share_cmplx_array_among_comm, mpi_comm_block

        INTEGER, INTENT(IN)      :: nfdm, number_channels, numsols
        COMPLEX(wp), INTENT(OUT) :: vecout(nfdm, number_channels, numsols)

        INTEGER :: ierror, My_rank, length_of_array, send_pe

        vecout = (0.0_wp, 0.0_wp)

        ! Get my rank
        CALL get_my_pe_id(My_rank)
        
        ! If I am the master PE in the outer region then recv data from inner region:
        length_of_array = nfdm * number_channels * numsols
        IF (my_rank == id_of_1st_pe_outer) THEN
! in the following the recv tag is assumed to be the same as the sending pe id i the communicator (see above)
           CALL receive_cmplx_array(vecout, length_of_array, pe_id_1st, MPI_COMM_WORLD, ierror)
!           CALL MPI_RECV(vecout, length_of_array, MPI_DOUBLE_COMPLEX, &
!                          pe_id_1st, tag, MPI_COMM_WORLD, status, ierror)
        END IF
! The following is for back up and is not used in the calling routines (ie no_of_pes_per_sector = 1 here) 
!        IF (no_of_pes_per_sector > 1) THEN
!           send_pe = 0   !  1st outer pe has id 0 in mpi_comm_block.
           !The correct block is chosen in the calling routine.
!           CALL share_cmplx_array_among_comm (length_of_array, vecout, send_pe, mpi_comm_block)
!        END IF
           
    END SUBROUTINE recv_psi_at_inner_fd_pts

!-----------------------------------------------------------------------

    SUBROUTINE share_psi_at_inner_fd_pts_with_first_outer(nfdm, numsols, vecout, k1_val, gs_true, pe_gs_id)

        USE communications_parameters, ONLY: id_of_1st_pe_outer, pe_id_1st
        USE grid_parameters,           ONLY: my_num_channels, my_channel_id_1st, my_channel_id_last
        USE initial_conditions,        ONLY: no_of_pes_per_sector
        USE mpi_communications,        ONLY: get_my_pe_id, scatter_cmplx_array_to_first_outer_block, &
                                             get_my_group_pe_id, &
                                             mpi_comm_0_outer_block, mpi_comm_gs_outer_block, disp_for_pe, &
                                             counts_per_pe
        USE wall_clock,                ONLY: update_time_start_inner, &
                                             update_time_end_inner
        USE initial_conditions,        ONLY: timings_desired

        INTEGER, INTENT(IN)      :: nfdm, numsols
        COMPLEX(wp), INTENT(INOUT) :: vecout(nfdm, my_num_channels, numsols)
        INTEGER, INTENT(IN), optional        :: k1_val
        LOGICAL, INTENT(IN), optional        :: gs_true
        INTEGER, INTENT(IN), optional        :: pe_gs_id
        
        INTEGER :: send_length_of_array(0:no_of_pes_per_sector), &
                   send_displacements(0:no_of_pes_per_sector)
        INTEGER :: ierror, My_rank, length_of_array, send_pe, number_of_pes, isol
        integer :: my_group_pe_id, inner_pe_id, mpi_comm_choice
        LOGICAL :: gs_true_val
        
        IF (PRESENT(gs_true)) THEN
           gs_true_val = gs_true
           IF (.NOT.(PRESENT(pe_gs_id))) THEN
              CALL assert(.false., 'share_psi_at_inner_fd_pts_with_first_outer needs a pe_gs_id passed to it.')
           END IF
        ELSE
           gs_true_val = .false.
        END IF
        IF (gs_true_val) THEN
           mpi_comm_choice = mpi_comm_gs_outer_block
           inner_pe_id = pe_id_1st ! note, this is not a mistake: in the communicator, the inner region gs
           ! has rank 0
        ELSE   
           mpi_comm_choice = mpi_comm_0_outer_block
           inner_pe_id = pe_id_1st
        END IF
        
        !        vecout = (0.0_wp, 0.0_wp)

        ! Get my rank
        CALL get_my_pe_id(My_rank)
        CALL get_my_group_pe_id(My_group_pe_id)
        send_length_of_array(:) = counts_per_pe(:) * nfdm 
        send_displacements(:) = disp_for_pe(:) * nfdm  
!        IF (my_rank == pe_id_1st) THEN
        IF (my_rank == inner_pe_id) THEN
            IF (PRESENT(k1_val) .and. timings_desired) THEN
                CALL update_time_start_inner(k1_val)
            END IF
        END IF
        length_of_array = nfdm * my_num_channels 
        number_of_pes = no_of_pes_per_sector + 1
!        send_pe = pe_id_1st
        send_pe = inner_pe_id
        DO isol = 1, numsols
           call scatter_cmplx_array_to_first_outer_block(number_of_pes, send_length_of_array, &
                                                         length_of_array, length_of_array, &
                                                         vecout(:,:,isol), send_pe, send_displacements, &
                                                         mpi_comm_choice)
!                                                         mpi_comm_0_outer_block)
        END DO
        
        IF (PRESENT(k1_val) .and. timings_desired) THEN
             CALL update_time_end_inner()
        END IF
    END SUBROUTINE share_psi_at_inner_fd_pts_with_first_outer

  END MODULE inner_to_outer_interface
