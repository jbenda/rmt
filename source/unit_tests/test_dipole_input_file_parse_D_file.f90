! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

MODULE test_dipole_input_file_parse_D_file
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64

    USE testdrive, ONLY: check, error_type, new_unittest, test_failed, unittest_type

    USE testing_utilities, ONLY: compare, &
                                data_file, &
                                format_context, &
                                int_to_char, &
                                range, &
                                subarray, &
                                subarray_integer_int32, &
                                subarray_real_real64

    USE initial_conditions, ONLY: debug

    USE precisn, ONLY: wp
    USE dipole_input_file, ONLY: D_file, dipole_block, parse_D_file

    IMPLICIT NONE(TYPE, EXTERNAL)

    INTERFACE check
        MODULE PROCEDURE check_d_file
    END INTERFACE

    !> \brief The expected derived type based on the format of the D file derived type from `dipole_input_file`
    !>
    !> The structure of this type maps almost 1:1 with the structure of `dipole_input_file therefore the structure of D file itself. 
    !> The structure is defined in terms of specified-width numeric types (i.e. `int32` and `real64`) because it is an external 
    !> filetype and setting `wp` to a different width shouldn't affect the way the file is read.
    !>
    !> The names of the fields match what is currently being used in the code.
    !>
    !> # D File Format
    !>
    !> The 2 files each containing a series of [_Unformatted I/O Records_][scipy-FortranFile]. The format of these records is compiler-and
    !> machine-dependent, although most compilers will use a `<record bytes><record data><record bytes>` format and most machines
    !> will be little-endian and use IEEE 754 floating point numbers.
    !>
    !> The first file contains 2 32-bit integers followed by 2 1D arrays both of which are 64-bit real arrays.
    !> 
    !> The final record in this file defines the D block described as "blocks"
    !>
    !> The second file contains 13 32-bit integers and then 2 \f$3 \times n\f$ matrix, with \f$n\f$ specified by the integers "nev" & "nod".
    !>
    !> Lastly there are 2 more 32-bit integers.
    !>
    !> [scipy-FortranFile]: https://docs.scipy.org/doc/scipy-1.9.3/reference/generated/scipy.io.FortranFile.html

    TYPE expected_D_file
        INTEGER(INT32):: ntarg
        INTEGER(INT32):: fintr
        REAL(REAL64), ALLOCATABLE :: crlv(:, :)
        REAL(REAL64), ALLOCATABLE :: crlv_v(:, :)
        TYPE(expected_dipole_block), DIMENSION(:), ALLOCATABLE:: blocks
    END TYPE

    TYPE expected_dipole_block
        INTEGER(INT32):: noterm, isi, inch, jsi, jnch
        INTEGER(INT32):: nev, nod
        INTEGER(INT32):: lgsf, lglf, lgpf, lgsi, lgli, lgpi
        TYPE(subarray_real_real64) :: dipsto
        TYPE(subarray_real_real64) :: dipsto_v
    END TYPE

    PRIVATE
    PUBLIC :: collect_dipole_input_file_parse_D_file
CONTAINS

    !> \brief Collection of tests performed on `dipole_input_file :: parse_D_file`
    !> 
    !> \param[in] testsuite List of tests.

    SUBROUTINE collect_dipole_input_file_parse_D_file(testsuite)
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite

        testsuite = [new_unittest("atomic::small::Ne_4cores", test_atomic_small_Ne_4cores), &
                    new_unittest("atomic::small::ar+", test_atomic_small_arx), &
                    new_unittest("atomic::small:argon", test_atomic_small_argon), &
                    new_unittest("atomic::small:helium", test_atomic_small_helium)]

    END SUBROUTINE

    SUBROUTINE check_d_file(error, actual, expected, nstmx, loc_of_blocks)
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        INTEGER, INTENT(IN) :: nstmx
        !INTEGER, INTENT(IN) :: num_expected_dipole_block
        TYPE(D_file), INTENT(in) :: actual
        TYPE(expected_D_file), INTENT(in) :: expected

        CHARACTER(len=:), ALLOCATABLE       :: context
        INTEGER, DIMENSION(:), ALLOCATABLE  :: expected_shape
        INTEGER, INTENT(IN)                 :: loc_of_blocks(:)
        INTEGER                             :: i

        context = ""

        IF (actual%ntarg /= expected%ntarg) THEN
            CALL format_context(context, "ntarg", actual%ntarg, expected%ntarg)
        END IF

        IF (.NOT. compare(actual%crlv, expected%crlv)) THEN
            CALL format_context(context, "crlv", actual%crlv, expected%crlv)
        END IF

        IF (.NOT. compare(actual%crlv_v, expected%crlv_v)) THEN
            CALL format_context(context, "crlv_v", actual%crlv_v, expected%crlv_v)
        END IF

        expected_shape = [actual%ntarg, actual%ntarg]
        IF(.NOT. compare(SHAPE(actual%crlv), expected_shape)) THEN
            CALL format_context(context, "shape(crlv)", SHAPE(actual%crlv), expected_shape)
        END IF

        expected_shape = [actual%ntarg, actual%ntarg]
        IF(.NOT. compare(SHAPE(actual%crlv_v), expected_shape)) THEN
            CALL format_context(context, "shape(crlv_v)", SHAPE(actual%crlv_v), expected_shape)
        END IF

        IF (actual%fintr /= expected%fintr) THEN
            CALL format_context(context, "fintr", actual%fintr, expected%fintr)
        END IF
 

        DO i = 1, SIZE(loc_of_blocks)
            CALL check_dipole_block(context, &
                                    loc_of_blocks(i), &
                                    actual%blocks(loc_of_blocks(i)), &
                                    expected%blocks(i))
        END DO

        IF (context /= "") THEN
            CALL test_failed(error, "D file parsed incorrectly!", context)
        END IF

    END SUBROUTINE

    SUBROUTINE check_dipole_block(context, index, actual, expected)
        TYPE(dipole_block), INTENT(in) :: actual
        TYPE(expected_dipole_block), INTENT(in) :: expected
        INTEGER, INTENT(IN) :: index

        CHARACTER(len=:), ALLOCATABLE :: context, prefix
        INTEGER, DIMENSION(:), ALLOCATABLE :: expected_shape

        prefix = "dipole_block("//int_to_char(index)//")%"

        IF (actual%noterm /= expected%noterm) THEN
            CALL format_context(context, prefix//"noterm", actual%noterm, expected%noterm)
        END IF

        IF (actual%isi /= expected%isi) THEN
            CALL format_context(context, prefix//"isi", actual%isi, expected%isi)
        END IF

        IF (actual%inch /= expected%inch) THEN
            CALL format_context(context, prefix//"inch", actual%inch, expected%inch)
        END IF

        IF (actual%jsi /= expected%jsi) THEN
            CALL format_context(context, prefix//"jsi", actual%jsi, expected%jsi)
        END IF

        IF (actual%jnch /= expected%jnch) THEN
            CALL format_context(context, prefix//"jnch", actual%jnch, expected%jnch)
        END IF

        IF (actual%nev /= expected%nev) THEN
            CALL format_context(context, prefix//"nev", actual%nev, expected%nev)
        END IF

        IF (actual%nod /= expected%nod) THEN
            CALL format_context(context, prefix//"nod", actual%nod, expected%nod)
        END IF

        IF (actual%lgsf /= expected%lgsf) THEN
            CALL format_context(context, prefix//"lgsf", actual%lgsf, expected%lgsf)
        END IF

        IF (actual%lglf /= expected%lglf) THEN
            CALL format_context(context, prefix//"lglf", actual%lglf, expected%lglf)
        END IF

        IF (actual%lgpf /= expected%lgpf) THEN
            CALL format_context(context, prefix//"lgpf", actual%lgpf, expected%lgpf)
        END IF

        IF (actual%lgsi /= expected%lgsi) THEN
            CALL format_context(context, prefix//"lgsi", actual%lgsi, expected%lgsi)
        END IF

        IF (actual%lgli /= expected%lgli) THEN
            CALL format_context(context, prefix//"lgli", actual%lgli, expected%lgli)
        END IF

        IF (actual%lgpi /= expected%lgpi) THEN
            CALL format_context(context, prefix//"lgpi", actual%lgpi, expected%lgpi)
        END IF

        IF (.NOT. compare(actual%dipsto, expected%dipsto)) THEN
            CALL format_context(context, "dipsto", actual%dipsto, expected%dipsto)
        END IF

        IF (.NOT. compare(actual%dipsto_v, expected%dipsto_v)) THEN
            CALL format_context(context, "dipsto_v", actual%dipsto_v, expected%dipsto_v)
        END IF

        ! Check that array shapes are _internally_ consistent
        expected_shape = [actual%nev, actual%nod]
        IF(.NOT. compare(SHAPE(actual%dipsto), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%dipsto)", SHAPE(actual%dipsto), expected_shape)
        END IF

        IF(.NOT. compare(SHAPE(actual%dipsto_v), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%dipsto_v)", SHAPE(actual%dipsto_v), expected_shape)
        END IF



    END SUBROUTINE

    SUBROUTINE test_D_file( &
                            error, &
                            path, &
                            expected, &
                            ntarg, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            mnp1, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_lrgl, &
                            LML_block_nspn, &
                            LML_block_npty, &
                            LML_block_ml, &
                            dipole_coupled, &
                            block_ind, &
                            coupling_id, &
                            xy_plane_desired, &
                            ML_max, &
                            lplusp, &
                            dipole_velocity_output, &
                            loc_of_blocks)

        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file), INTENT(INOUT) :: expected
        TYPE(D_file) :: file
        CHARACTER(len=*), INTENT(in) :: path
        CHARACTER(len=:), ALLOCATABLE :: relative_path_header, D_file_path_header, relative_path_data, D_file_path_data
        
        INTEGER, INTENT(IN)                  :: ntarg
        INTEGER, INTENT(IN)                  :: nstmx
        INTEGER, INTENT(IN)                  :: no_of_L_blocks
        INTEGER, INTENT(IN)                  :: no_of_LML_blocks
        INTEGER, ALLOCATABLE, INTENT(IN)     :: mnp1(:)
        INTEGER, INTENT(IN)                  :: L_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: L_block_nspn(:)
        INTEGER, INTENT(IN)                  :: L_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_lrgl(:)
        INTEGER, INTENT(IN)                  :: LML_block_nspn(:)
        INTEGER, INTENT(IN)                  :: LML_block_npty(:)
        INTEGER, INTENT(IN)                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE, INTENT(OUT)     :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE, INTENT(OUT)     :: block_ind(:, :, :)
        INTEGER, INTENT(IN)                     :: coupling_id
        LOGICAL, INTENT(IN)                     :: xy_plane_desired(:)
        INTEGER, INTENT(IN)                     :: ML_max
        INTEGER, INTENT(IN)                     :: lplusp
        LOGICAL, INTENT(IN)                     :: dipole_velocity_output
        INTEGER, INTENT(IN)                     :: loc_of_blocks(:)

        relative_path_header = path//"/inputs/d00"
        relative_path_data = path//"/inputs/d"
        D_file_path_header = data_file(relative_path_header)
        D_file_path_data = data_file(relative_path_data)
        
        CALL parse_D_file(D_file_path_header, &
                            D_file_path_data, &
                            coupling_id, &
                            ntarg, &
                            dipole_velocity_output, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            ML_max, &
                            lplusp, &
                            debug, &
                            xy_plane_desired, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_lrgl, &
                            LML_block_nspn, &
                            LML_block_npty, &
                            LML_block_ml, &
                            mnp1, &
                            file, &
                            dipole_coupled, &
                            block_ind)

        CALL check(error, file, expected, nstmx, loc_of_blocks)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_Ne_4cores(error)
        USE initial_conditions, ONLY: xy_plane_desired
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected

        INTEGER(INT32)                      :: ntarg
        
        INTEGER(INT32)                      :: nstmx
        INTEGER(INT32)                      :: no_of_L_blocks
        INTEGER(INT32)                      :: no_of_LML_blocks
        INTEGER, ALLOCATABLE                :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                :: L_block_npty(:)
        INTEGER, ALLOCATABLE                :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                :: mnp1(:)
        INTEGER, ALLOCATABLE                :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE                :: block_ind(:, :, :)
        INTEGER(INT32)                      :: coupling_id
        INTEGER(INT32)                      :: ML_max
        INTEGER(INT32)                      :: lplusp
        LOGICAL                             :: dipole_velocity_output

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        ntarg = 2
        nstmx = 254
        no_of_L_blocks = 3
        no_of_LML_blocks = 3
        
        loc_of_blocks = [1,2]
        L_block_lrgl = [0,1,2]
        L_block_nspn = [1,1,1]
        L_block_npty = [0,1,0]
        LML_block_lrgl = [0,1,2]
        LML_block_nspn = [1,1,1]
        LML_block_npty = [0,1,0]
        LML_block_ml = [0,0,0]
        mnp1 = [134,233,254]
        coupling_id = 1
        xy_plane_desired = .false.
        ML_max = 0
        lplusp = 0
        dipole_velocity_output = .true.

        expected = expected_D_file( &
                                    ntarg=2_INT32, &
                                    fintr=2_INT32,&
                                    crlv=RESHAPE( &
                                        [ &
                                            0.000000E+00_REAL64, &
                                            -6.988487E-01_REAL64, &
                                            -6.988487E-01_REAL64, &
                                            0.000000E+00_REAL64 &
                                        ], &
                                        [2, 2] &
                                    ), &

                                    crlv_v=RESHAPE( &
                                        [ &
                                            0.000000E+00_REAL64, &
                                            7.088344E-01_REAL64, &
                                            -7.088344E-01_REAL64, &
                                            0.000000E+00_REAL64 &
                                        ], &
                                        [2, 2] &
                                    ), &
        
                                    blocks = [ &
                                        expected_dipole_block( &
                                            noterm=37_INT32,&
                                            isi=134_INT32,&
                                            inch=2_INT32,&
                                            jsi=233_INT32,&
                                            jnch=3_INT32,&
                                            nev=134_INT32,&
                                            nod=233_INT32,&
                                            lgsf=1_INT32,&
                                            lglf=1_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=1_INT32,&
                                            lgli=0_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                [ &
                                                1.110982E-02_REAL64, &
                                                2.551390E-02_REAL64, &
                                                6.750738E-02_REAL64, &
                                                -3.966213E-02_REAL64, &
                                                -1.543795E-02_REAL64, &
                                                -4.197854E-02_REAL64, &
                                                -4.190530E-02_REAL64, &
                                                -2.424282E-02_REAL64, &
                                                -5.941906E-04_REAL64, &
                                                -4.274368E-03_REAL64, &
                                                -4.380403E-03_REAL64, &
                                                8.332875E-03_REAL64, &
                                                1.301455E-03_REAL64, &
                                                6.221420E-03_REAL64, &
                                                1.438237E-02_REAL64, &
                                                6.362190E-03_REAL64, &
                                                -1.953993E-04_REAL64, &
                                                3.419101E-03_REAL64, &
                                                -3.975089E-02_REAL64, &
                                                -1.602746E-02_REAL64, &
                                                1.684893E-02_REAL64, &
                                                -9.848021E-04_REAL64, &
                                                2.350944E-02_REAL64, &
                                                -6.716700E-02_REAL64, &
                                                -3.093441E-03_REAL64, &
                                                7.217363E-02_REAL64, &
                                                -2.020857E-02_REAL64, &
                                                3.767787E-02_REAL64, &
                                                -4.206745E-01_REAL64, &
                                                -2.955628E-03_REAL64, &
                                                8.195707E-02_REAL64, &
                                                -2.281290E-02_REAL64, &
                                                1.490197E-01_REAL64, &
                                                1.388888E-01_REAL64, &
                                                1.996420E-02_REAL64, &
                                                -7.392055E-02_REAL64, &
                                                -9.303705E-03_REAL64, &
                                                -1.853542E-02_REAL64, &
                                                -1.052465E-01_REAL64, &
                                                1.987326E-02_REAL64, &
                                                -2.109181E-02_REAL64, &
                                                7.059327E-02_REAL64, &
                                                -9.366499E-03_REAL64, &
                                                -8.077935E-01_REAL64, &
                                                -1.250302E-02_REAL64, &
                                                7.963048E-02_REAL64, &
                                                -5.528245E-01_REAL64, &
                                                -1.797119E-02_REAL64, &
                                                1.733215E-01_REAL64, &
                                                2.150762E-01_REAL64, &
                                                -3.133102E-01_REAL64, &
                                                -3.928775E-02_REAL64, &
                                                -1.461644E-01_REAL64, &
                                                -2.071927E-02_REAL64, &
                                                -5.109402E-02_REAL64, &
                                                -3.873925E-02_REAL64, &
                                                6.941037E-02_REAL64, &
                                                -3.413381E-03_REAL64, &
                                                -3.712908E-02_REAL64, &
                                                2.009530E-02_REAL64, &
                                                -5.630472E-02_REAL64, &
                                                -1.473989E-01_REAL64, &
                                                -6.221881E-02_REAL64, &
                                                -1.900916E-01_REAL64, &
                                                1.347866E-02_REAL64, &
                                                -1.248341E-04_REAL64, &
                                                -8.280706E-02_REAL64, &
                                                2.005748E-02_REAL64, &
                                                -2.827255E-02_REAL64, &
                                                -8.508275E-02_REAL64, &
                                                3.627614E-02_REAL64, &
                                                9.792627E-03_REAL64, &
                                                -1.042983E-03_REAL64, &
                                                6.175391E-03_REAL64, &
                                                1.857263E-02_REAL64, &
                                                -5.001566E-03_REAL64, &
                                                -1.279049E-02_REAL64, &
                                                -2.265594E-02_REAL64, &
                                                -1.147405E-02_REAL64, &
                                                -8.579808E-03_REAL64, &
                                                4.869896E-04_REAL64, &
                                                -8.369387E-03_REAL64, &
                                                -6.448954E-05_REAL64, &
                                                -6.480548E-03_REAL64, &
                                                -5.147249E-05_REAL64, &
                                                4.821257E-03_REAL64, &
                                                1.563284E-04_REAL64, &
                                                5.354479E-03_REAL64, &
                                                3.570483E-05_REAL64, &
                                                3.614103E-03_REAL64, &
                                                -6.995507E-05_REAL64, &
                                                -3.000067E-03_REAL64, &
                                                -3.963513E-05_REAL64, &
                                                -2.407629E-03_REAL64, &
                                                -3.806297E-05_REAL64, &
                                                -3.575428E-03_REAL64, &
                                                2.401342E-05_REAL64, &
                                                -1.813179E-03_REAL64, &
                                                -3.241685E-06_REAL64, &
                                                -1.352032E-03_REAL64, &
                                                9.191498E-06_REAL64 &
                                                ], &
                                                [range(28,128), range(119,119)], &
                                                [134, 233] &
                                            ), &
                                            dipsto_v=subarray( &
                                                [ &
                                                3.943454E-01_REAL64, &
                                                2.042135E-01_REAL64, &
                                                -6.450676E-02_REAL64, &
                                                -6.603488E-02_REAL64, &
                                                4.812760E-02_REAL64, &
                                                3.463609E-02_REAL64, &
                                                2.615479E-02_REAL64, &
                                                2.442503E-02_REAL64, &
                                                5.294008E-02_REAL64, &
                                                6.931185E-01_REAL64, &
                                                8.589996E-02_REAL64, &
                                                4.589414E-02_REAL64, &
                                                6.454019E-03_REAL64, &
                                                1.984915E-03_REAL64, &
                                                -1.499452E-02_REAL64, &
                                                -5.045392E-02_REAL64, &
                                                -1.543047E-03_REAL64, &
                                                -3.230153E-02_REAL64, &
                                                -4.339407E-02_REAL64, &
                                                -2.466223E-03_REAL64, &
                                                6.575923E-02_REAL64, &
                                                -5.609501E-02_REAL64, &
                                                -6.941769E-02_REAL64, &
                                                1.736868E-01_REAL64, &
                                                5.221490E-02_REAL64, &
                                                -1.856094E-02_REAL64, &
                                                -7.963464E-02_REAL64, &
                                                6.413023E-03_REAL64, &
                                                5.189680E-02_REAL64, &
                                                -1.028724E-01_REAL64 &
                                                ], &
                                                [range(1,30), range(1,1)], &
                                                [134, 233] &  !array shape
                                            ) &
                                        ), &

                                        expected_dipole_block( &
                                            noterm=37_INT32,&
                                            isi=254_INT32,&
                                            inch=3_INT32,&
                                            jsi=233_INT32,&
                                            jnch=3_INT32,&
                                            nev=254_INT32,&
                                            nod=233_INT32,&
                                            lgsf=1_INT32,&
                                            lglf=1_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=1_INT32,&
                                            lgli=2_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                [ &
                                                    -6.109485E+00_REAL64, &
                                                    4.404401E-01_REAL64, &
                                                    1.396815E-02_REAL64, &
                                                    1.146191E-01_REAL64, &
                                                    2.600433E-02_REAL64, &
                                                    -1.426502E-01_REAL64, &
                                                    2.741847E-02_REAL64, &
                                                    1.977261E-01_REAL64, &
                                                    -4.219004E-02_REAL64, &
                                                    -2.306265E-03_REAL64, &
                                                    -1.929977E-01_REAL64, &
                                                    5.177589E-04_REAL64, &
                                                    1.687935E-01_REAL64, &
                                                    4.863001E-04_REAL64, &
                                                    -1.468804E-01_REAL64, &
                                                    1.725203E-03_REAL64, &
                                                    -1.280519E-01_REAL64, &
                                                    7.322311E-03_REAL64, &
                                                    7.931716E-03_REAL64, &
                                                    1.097730E-02_REAL64, &
                                                    2.448899E-03_REAL64, &
                                                    -2.159052E-02_REAL64, &
                                                    -1.129641E-01_REAL64, &
                                                    1.349985E-02_REAL64, &
                                                    3.523290E-03_REAL64, &
                                                    -2.265918E-02_REAL64, &
                                                    1.042022E-01_REAL64, &
                                                    1.894883E-02_REAL64, &
                                                    1.465857E-04_REAL64, &
                                                    2.994952E-01_REAL64 &
                                                ], &
                                                [range(1,30), range(1,1)], &
                                                [254, 233] &
                                            ), &
                                            dipsto_v=subarray( &
                                                [ &
                                                    -2.008332E-02_REAL64, &
                                                    -4.222173E-04_REAL64, &
                                                    -1.290333E-01_REAL64, &
                                                    2.298287E-01_REAL64, &
                                                    -1.493026E-02_REAL64, &
                                                    -5.531369E-02_REAL64, &
                                                    -7.688260E-03_REAL64, &
                                                    2.509366E-02_REAL64, &
                                                    -9.219870E-03_REAL64, &
                                                    -2.840849E-01_REAL64, &
                                                    1.292693E-01_REAL64, &
                                                    -1.803849E-01_REAL64, &
                                                    1.197697E-01_REAL64, &
                                                    -2.572229E-02_REAL64, &
                                                    -5.188018E-02_REAL64, &
                                                    2.128740E-02_REAL64, &
                                                    -6.100581E-03_REAL64, &
                                                    -7.376211E-02_REAL64, &
                                                    -4.338057E-02_REAL64, &
                                                    -2.336857E-01_REAL64, &
                                                    -7.454927E-03_REAL64, &
                                                    -4.111127E-02_REAL64, &
                                                    -1.596226E-01_REAL64, &
                                                    7.238405E-01_REAL64, &
                                                    -1.487925E-02_REAL64, &
                                                    -3.826361E-03_REAL64, &
                                                    9.632521E-02_REAL64, &
                                                    -3.904921E-02_REAL64, &
                                                    2.114128E-02_REAL64, &
                                                    8.542436E-03_REAL64 &
                                                ], &
                                                [range(28,57), range(119,119)], &
                                                [254, 233] &  !array shape
                                            ) &
                                        ) &
                                    ] &
                                )

        CALL test_D_file(error, &
                        "atomic_tests/small_tests/Ne_4cores", &
                        expected, &
                        ntarg, &
                        nstmx, &
                        no_of_L_blocks, &
                        no_of_LML_blocks, &
                        mnp1, &
                        L_block_lrgl, &
                        L_block_nspn, &
                        L_block_npty, &
                        LML_block_lrgl, &
                        LML_block_nspn, &
                        LML_block_npty, &
                        LML_block_ml, &
                        dipole_coupled, &
                        block_ind, &
                        coupling_id, &
                        xy_plane_desired, &
                        ML_max, &
                        lplusp, &
                        dipole_velocity_output, &
                        loc_of_blocks)

    END SUBROUTINE

    SUBROUTINE test_atomic_small_arx(error)
        USE initial_conditions, ONLY: xy_plane_desired
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected

        INTEGER(INT32)                      :: ntarg
        INTEGER(INT32)                      :: nstmx
        INTEGER(INT32)                      :: no_of_L_blocks
        INTEGER(INT32)                      :: no_of_LML_blocks
        INTEGER, ALLOCATABLE                :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                :: L_block_npty(:)
        INTEGER, ALLOCATABLE                :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                :: mnp1(:)
        INTEGER, ALLOCATABLE                :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE                :: block_ind(:, :, :)
        INTEGER(INT32)                      :: coupling_id
        INTEGER(INT32)                      :: ML_max
        INTEGER(INT32)                      :: lplusp
        LOGICAL                             :: dipole_velocity_output

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        !constants for ar+

        ntarg = 1
        nstmx = 141
        no_of_L_blocks = 7
        no_of_LML_blocks = 31

        loc_of_blocks = [1,4,8]
        L_block_lrgl = [0,1,1,2,2,3,3]
        L_block_nspn = [2,2,2,2,2,2,2]
        L_block_npty = [0,0,1,0,1,0,1]
        LML_block_lrgl = [0,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
        LML_block_nspn = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
        LML_block_npty = [0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1]
        LML_block_ml = [0,-1,0,1,-1,0,1,-2,-1,0,1,2,-2,-1,0,1,2,-3,-2,-1,0,1,2,3,-3,-2,-1,0,1,2,3]
        mnp1 = [49,48,95,141,94,94,139]
        coupling_id = 1
        xy_plane_desired = .false.
        ML_max = 3
        lplusp = 0
        dipole_velocity_output = .true.

        expected = expected_D_file( &
                                        ntarg=1_INT32, &
                                        fintr=8_INT32,&
                                        crlv=RESHAPE( &
                                        [ &
                                            0.000000E+00_REAL64 &
                                        ], &
                                        [1, 1] &
                                        ) , &

                                        crlv_v=RESHAPE( &
                                        [ &
                                            0.000000E+00_REAL64 &
                                        ], &
                                        [1, 1] &
                                        ), &
        
                                    blocks = [ &
                                        expected_dipole_block( &
                                            noterm=47_INT32,&
                                            isi=49_INT32,&
                                            inch=1_INT32,&
                                            jsi=95_INT32,&
                                            jnch=2_INT32,&
                                            nev=49_INT32,&
                                            nod=95_INT32,&
                                            lgsf=2_INT32,&
                                            lglf=1_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=2_INT32,&
                                            lgli=0_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                    [ &
                                                        -2.884016E-02_REAL64, &
                                                        -2.131831E-01_REAL64, &
                                                        -8.219555E-02_REAL64, &
                                                        2.727543E+00_REAL64, &
                                                        5.009463E+00_REAL64, &
                                                        -1.303807E+00_REAL64, &
                                                        3.815109E-02_REAL64, &
                                                        2.304239E-01_REAL64, &
                                                        -3.232367E-02_REAL64, &
                                                        1.024545E-01_REAL64, &
                                                        -2.285545E-02_REAL64, &
                                                        5.984241E-02_REAL64, &
                                                        -1.640228E-02_REAL64, &
                                                        3.961276E-02_REAL64, &
                                                        -1.201841E-02_REAL64, &
                                                        2.822502E-02_REAL64, &
                                                        -8.593788E-03_REAL64, &
                                                        2.091948E-02_REAL64, &
                                                        -9.436875E-03_REAL64, &
                                                        1.724995E-02_REAL64, &
                                                        1.559103E-03_REAL64, &
                                                        1.276142E-02_REAL64, &
                                                        -9.022314E-03_REAL64, &
                                                        1.060902E-01_REAL64, &
                                                        -4.131329E-01_REAL64, &
                                                        2.389520E-01_REAL64, &
                                                        3.090715E+00_REAL64, &
                                                        5.602612E+00_REAL64, &
                                                        -1.621495E+00_REAL64, &
                                                        -9.828150E-02_REAL64, &
                                                        1.744118E-01_REAL64, &
                                                        4.056144E-02_REAL64, &
                                                        5.871367E-02_REAL64, &
                                                        2.287642E-02_REAL64, &
                                                        2.882601E-02_REAL64, &
                                                        1.479367E-02_REAL64, &
                                                        1.718203E-02_REAL64, &
                                                        1.037333E-02_REAL64, &
                                                        1.144247E-02_REAL64, &
                                                        8.104346E-03_REAL64, &
                                                        8.657506E-03_REAL64, &
                                                        4.445088E-03_REAL64, &
                                                        6.032238E-03_REAL64, &
                                                        8.728198E-03_REAL64, &
                                                        2.133018E-03_REAL64, &
                                                        4.859207E-04_REAL64, &
                                                        7.400576E-02_REAL64, &
                                                        2.073017E-02_REAL64, &
                                                        2.215875E-01_REAL64, &
                                                        -6.142772E-02_REAL64, &
                                                        -2.645433E+00_REAL64, &
                                                        5.128850E+00_REAL64, &
                                                        1.454843E+00_REAL64, &
                                                        4.605786E-02_REAL64, &
                                                        -2.406563E-01_REAL64, &
                                                        3.257139E-02_REAL64, &
                                                        -1.042465E-01_REAL64, &
                                                        2.222980E-02_REAL64, &
                                                        -6.002538E-02_REAL64, &
                                                        1.573272E-02_REAL64, &
                                                        -3.940065E-02_REAL64, &
                                                        1.167034E-02_REAL64, &
                                                        -2.825193E-02_REAL64, &
                                                        7.711649E-03_REAL64, &
                                                        -2.074822E-02_REAL64, &
                                                        8.284950E-03_REAL64, &
                                                        -1.588613E-02_REAL64, &
                                                        1.064700E-02_REAL64, &
                                                        -1.134275E-02_REAL64 &
                                                    ], &
                                                    [range(8,30), range(20,22)], &
                                                    [49, 95] &
                                            ), &

                                            dipsto_v=subarray( &
                                                [ &
                                                    -5.815806E-02_REAL64, &
                                                    1.945893E-01_REAL64, &
                                                    2.816887E-01_REAL64, &
                                                    5.092349E-01_REAL64, &
                                                    -7.836964E-03_REAL64, &
                                                    -1.563587E-01_REAL64, &
                                                    3.844679E-01_REAL64, &
                                                    -1.386029E-01_REAL64, &
                                                    1.177647E-01_REAL64, &
                                                    3.327536E-02_REAL64, &
                                                    2.980310E-01_REAL64, &
                                                    1.250145E-01_REAL64, &
                                                    -9.003985E-02_REAL64, &
                                                    -3.025015E-02_REAL64, &
                                                    -2.465643E-01_REAL64, &
                                                    1.071005E-01_REAL64, &
                                                    -6.672760E-02_REAL64, &
                                                    -3.044829E-02_REAL64, &
                                                    -2.099873E-01_REAL64, &
                                                    9.456613E-02_REAL64, &
                                                    -4.751707E-02_REAL64, &
                                                    -3.144485E-02_REAL64, &
                                                    -1.826886E-01_REAL64, &
                                                    8.540221E-02_REAL64, &
                                                    -3.099628E-02_REAL64, &
                                                    -3.606428E-02_REAL64, &
                                                    -1.643236E-01_REAL64, &
                                                    8.003815E-02_REAL64, &
                                                    -3.465662E-02_REAL64, &
                                                    -1.849060E-02_REAL64, &
                                                    -1.463261E-01_REAL64, &
                                                    8.388967E-02_REAL64, &
                                                    7.190837E-02_REAL64, &
                                                    -7.622425E-02_REAL64, &
                                                    -1.289620E-01_REAL64, &
                                                    3.961241E-02_REAL64, &
                                                    -7.272195E-02_REAL64, &
                                                    8.464225E-03_REAL64, &
                                                    -1.080027E-01_REAL64, &
                                                    -1.501667E-01_REAL64, &
                                                    1.659150E-01_REAL64, &
                                                    2.862262E-04_REAL64, &
                                                    -7.866998E-02_REAL64, &
                                                    1.839647E-02_REAL64, &
                                                    -5.917042E-02_REAL64, &
                                                    8.732176E-03_REAL64, &
                                                    -6.151410E-02_REAL64, &
                                                    -2.054787E-01_REAL64, &
                                                    2.475087E-01_REAL64, &
                                                    -6.523751E-03_REAL64 &
                                                ], &
                                                [range(10,10), range(20,69)], &
                                                [49,95] &  !array shape
                                            ) &
                                        ), &

                                        expected_dipole_block( &
                                            noterm=47_INT32,&
                                            isi=141_INT32,&
                                            inch=3_INT32,&
                                            jsi=95_INT32,&
                                            jnch=2_INT32,&
                                            nev=141_INT32,&
                                            nod=95_INT32,&
                                            lgsf=2_INT32,&
                                            lglf=1_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=2_INT32,&
                                            lgli=2_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                    [ &
                                                        -1.120115E-01_REAL64, &
                                                        -2.740113E-02_REAL64, &
                                                        -9.789855E-02_REAL64, &
                                                        1.093848E-01_REAL64, &
                                                        2.391561E-02_REAL64, &
                                                        8.215366E-02_REAL64, &
                                                        1.030382E-01_REAL64, &
                                                        -2.036043E-02_REAL64, &
                                                        6.956051E-02_REAL64, &
                                                        -9.483944E-02_REAL64, &
                                                        -1.695233E-02_REAL64, &
                                                        -5.951975E-02_REAL64, &
                                                        8.593762E-02_REAL64, &
                                                        1.383052E-02_REAL64, &
                                                        5.155905E-02_REAL64, &
                                                        -7.700185E-02_REAL64, &
                                                        -1.106991E-02_REAL64, &
                                                        4.532910E-02_REAL64, &
                                                        6.837945E-02_REAL64, &
                                                        8.697474E-03_REAL64, &
                                                        -4.060891E-02_REAL64, &
                                                        6.018826E-02_REAL64, &
                                                        -6.707031E-03_REAL64, &
                                                        3.734062E-02_REAL64, &
                                                        -5.233503E-02_REAL64, &
                                                        5.071337E-03_REAL64 &
                                                    ], &
                                                    [range(30,55), range(1,1)], &
                                                    [141, 95] &
                                            ), &

                                            dipsto_v=subarray( &
                                                [ &
                                                    3.446751E-01_REAL64, &
                                                    4.423362E-02_REAL64, &
                                                    3.092930E-01_REAL64, &
                                                    -3.822474E-01_REAL64, &
                                                    -4.183882E-02_REAL64, &
                                                    -3.038655E-01_REAL64, &
                                                    -4.082498E-01_REAL64, &
                                                    3.835061E-02_REAL64, &
                                                    -2.986228E-01_REAL64, &
                                                    4.246754E-01_REAL64, &
                                                    3.414801E-02_REAL64, &
                                                    2.940140E-01_REAL64, &
                                                    -4.332132E-01_REAL64, &
                                                    -2.958058E-02_REAL64, &
                                                    -2.905691E-01_REAL64, &
                                                    4.351759E-01_REAL64, &
                                                    2.493839E-02_REAL64, &
                                                    -2.890294E-01_REAL64, &
                                                    -4.314129E-01_REAL64, &
                                                    -2.044319E-02_REAL64, &
                                                    2.906083E-01_REAL64, &
                                                    -4.221158E-01_REAL64, &
                                                    1.625034E-02_REAL64, &
                                                    -2.975850E-01_REAL64, &
                                                    4.062448E-01_REAL64, &
                                                    -1.245616E-02_REAL64 &
                                                ], &
                                                [range(30,55), range(1,1)], &
                                                [141,95] &  !array shape
                                            ) &
                                        ), &

                                        expected_dipole_block( &
                                            noterm=47_INT32,&
                                            isi=94_INT32,&
                                            inch=2_INT32,&
                                            jsi=139_INT32,&
                                            jnch=3_INT32,&
                                            nev=94_INT32,&
                                            nod=139_INT32,&
                                            lgsf=2_INT32,&
                                            lglf=3_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=2_INT32,&
                                            lgli=3_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                [ &
                                                    5.564935E-02_REAL64, &
                                                    -3.077433E-02_REAL64, &
                                                    -1.762101E-02_REAL64, &
                                                    1.783603E-02_REAL64, &
                                                    3.419511E-02_REAL64, &
                                                    -1.092902E-02_REAL64, &
                                                    1.609281E-02_REAL64, &
                                                    -3.235687E-03_REAL64, &
                                                    1.539373E-02_REAL64, &
                                                    1.511380E-02_REAL64, &
                                                    -1.585857E-02_REAL64, &
                                                    -1.193323E-03_REAL64, &
                                                    8.878366E-03_REAL64, &
                                                    -2.003986E-03_REAL64, &
                                                    4.981426E-03_REAL64, &
                                                    -7.615507E-04_REAL64, &
                                                    3.573982E-03_REAL64, &
                                                    5.501155E-03_REAL64, &
                                                    -6.554782E-03_REAL64, &
                                                    5.405429E-04_REAL64, &
                                                    -1.790965E-03_REAL64, &
                                                    2.250692E-04_REAL64, &
                                                    -1.005301E-03_REAL64, &
                                                    1.526696E-04_REAL64, &
                                                    -4.021032E-04_REAL64, &
                                                    3.486822E-03_REAL64, &
                                                    -4.237477E-03_REAL64, &
                                                    -8.573016E-07_REAL64, &
                                                    2.164106E-04_REAL64, &
                                                    -3.927903E-05_REAL64, &
                                                    -1.850214E-05_REAL64, &
                                                    4.028118E-05_REAL64, &
                                                    -1.820875E-05_REAL64, &
                                                    1.853292E-05_REAL64, &
                                                    -1.777413E-05_REAL64, &
                                                    9.310619E-05_REAL64, &
                                                    -1.131564E-04_REAL64, &
                                                    -1.693512E-05_REAL64, &
                                                    1.193687E-05_REAL64, &
                                                    1.293134E-05_REAL64, &
                                                    9.239226E-06_REAL64, &
                                                    -9.495638E-06_REAL64, &
                                                    6.313357E-06_REAL64, &
                                                    -7.158116E-06_REAL64, &
                                                    4.081763E-06_REAL64, &
                                                    2.795282E-06_REAL64, &
                                                    0.000000E+00_REAL64, &
                                                    0.000000E+00_REAL64, &
                                                    0.000000E+00_REAL64, &
                                                    0.000000E+00_REAL64, &
                                                    0.000000E+00_REAL64 &
                                                ], &
                                                [range(20,20), range(50,100)], &
                                                [94, 139] &
                                            ), &

                                            dipsto_v=subarray( &
                                                [ &
                                                    3.888513E-01_REAL64, &
                                                    -2.396207E-01_REAL64, &
                                                    -1.425465E-01_REAL64, &
                                                    1.432956E-01_REAL64, &
                                                    3.105434E-01_REAL64, &
                                                    -1.149255E-01_REAL64, &
                                                    1.910298E-01_REAL64, &
                                                    -4.807599E-02_REAL64, &
                                                    2.379316E-01_REAL64, &
                                                    2.541966E-01_REAL64, &
                                                    -2.675632E-01_REAL64, &
                                                    -2.582020E-02_REAL64, &
                                                    1.778556E-01_REAL64, &
                                                    -4.934471E-02_REAL64, &
                                                    1.355686E-01_REAL64, &
                                                    -2.862331E-02_REAL64, &
                                                    1.277999E-01_REAL64, &
                                                    2.568315E-01_REAL64, &
                                                    -3.072569E-01_REAL64, &
                                                    2.136910E-02_REAL64, &
                                                    -9.450268E-02_REAL64, &
                                                    1.796819E-02_REAL64, &
                                                    -6.870391E-02_REAL64, &
                                                    1.040349E-02_REAL64, &
                                                    -5.005228E-02_REAL64, &
                                                    3.587269E-01_REAL64, &
                                                    -4.359405E-01_REAL64, &
                                                    6.106495E-03_REAL64, &
                                                    3.096934E-02_REAL64, &
                                                    -4.351985E-03_REAL64, &
                                                    -1.578675E-02_REAL64, &
                                                    7.972186E-03_REAL64, &
                                                    1.387768E-03_REAL64, &
                                                    -5.165983E-04_REAL64, &
                                                    -5.115453E-03_REAL64, &
                                                    -9.644028E-01_REAL64, &
                                                    1.172717E+00_REAL64, &
                                                    -5.059746E-04_REAL64, &
                                                    -2.915744E-04_REAL64, &
                                                    -2.502617E-04_REAL64, &
                                                    -5.028938E-05_REAL64, &
                                                    4.768470E-05_REAL64, &
                                                    -4.106854E-05_REAL64, &
                                                    4.645912E-05_REAL64, &
                                                    -2.362837E-05_REAL64, &
                                                    -1.619839E-05_REAL64, &
                                                    0.000000E+00_REAL64, &
                                                    0.000000E+00_REAL64, &
                                                    0.000000E+00_REAL64, &
                                                    0.000000E+00_REAL64, &
                                                    0.000000E+00_REAL64 &
                                                ], &
                                                [range(20,20), range(50,100)], &
                                                [94,139] &  !array shape
                                            ) &
                                        ) &
                                    ] &
                        )


        CALL test_D_file(error, &
                        "atomic_tests/small_tests/ar+", &
                        expected, &
                        ntarg, &
                        nstmx, &
                        no_of_L_blocks, &
                        no_of_LML_blocks, &
                        mnp1, &
                        L_block_lrgl, &
                        L_block_nspn, &
                        L_block_npty, &
                        LML_block_lrgl, &
                        LML_block_nspn, &
                        LML_block_npty, &
                        LML_block_ml, &
                        dipole_coupled, &
                        block_ind, &
                        coupling_id, &
                        xy_plane_desired, &
                        ML_max, &
                        lplusp, &
                        dipole_velocity_output, &
                        loc_of_blocks)
    END SUBROUTINE test_atomic_small_arx

    SUBROUTINE test_atomic_small_argon(error)
        USE initial_conditions, ONLY: xy_plane_desired
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected

        INTEGER(INT32)                      :: ntarg
        INTEGER(INT32)                      :: nstmx
        INTEGER(INT32)                      :: no_of_L_blocks
        INTEGER(INT32)                      :: no_of_LML_blocks
        INTEGER, ALLOCATABLE                :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                :: L_block_npty(:)
        INTEGER, ALLOCATABLE                :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                :: mnp1(:)
        INTEGER, ALLOCATABLE                :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE                :: block_ind(:, :, :)
        INTEGER(INT32)                      :: coupling_id
        INTEGER(INT32)                      :: ML_max
        INTEGER(INT32)                      :: lplusp
        LOGICAL                             :: dipole_velocity_output

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        !constants for arg

        ntarg = 2
        nstmx = 253
        no_of_L_blocks = 5
        no_of_LML_blocks = 17

        loc_of_blocks =[1,5]
        L_block_lrgl = [0,1,1,2,2]
        L_block_nspn = [1,1,1,1,1]
        L_block_npty = [0,0,1,0,1]
        LML_block_lrgl = [0,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2]
        LML_block_nspn = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        LML_block_npty = [0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,1,1]
        LML_block_ml = [0,-1,0,1,-1,0,1,-2,-1,0,1,2,-2,-1,0,1,2]
        mnp1 = [148,110,236,253,127]
        coupling_id = 1
        xy_plane_desired = .false.
        ML_max = 2
        lplusp = 0
        dipole_velocity_output = .true.

        expected = expected_D_file( &
                                        ntarg=2_INT32, &
                                        fintr=5_INT32,&
                                        crlv=RESHAPE( &
                                        [ &
                                            0.000000E+00_REAL64, &
                                            -5.178020E-01_REAL64, &
                                            -5.178020E-01_REAL64, &
                                            0.000000E+00_REAL64 &
                                        ], &
                                        [2, 2] &
                                        ) , &

                                        crlv_v=RESHAPE( &
                                        [ &
                                            0.000000E+00_REAL64, &
                                            2.389534E-01_REAL64, &
                                            -2.389534E-01_REAL64, &
                                            0.000000E+00_REAL64 &
                                        ], &
                                        [2, 2] &
                                        ), &
        
                                    blocks = [ &
                                        expected_dipole_block( &
                                            noterm=56_INT32,&
                                            isi=148_INT32,&
                                            inch=2_INT32,&
                                            jsi=236_INT32,&
                                            jnch=3_INT32,&
                                            nev=148_INT32,&
                                            nod=236_INT32,&
                                            lgsf=1_INT32,&
                                            lglf=1_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=1_INT32,&
                                            lgli=0_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                    [ &
                                                        5.031942E-02_REAL64, &
                                                        5.470337E-04_REAL64, &
                                                        -1.712177E-02_REAL64, &
                                                        4.886472E-03_REAL64, &
                                                        1.608369E-02_REAL64, &
                                                        -8.886655E-03_REAL64, &
                                                        1.133004E-02_REAL64, &
                                                        1.325906E-02_REAL64, &
                                                        -9.147858E-03_REAL64, &
                                                        -1.161336E-02_REAL64, &
                                                        -7.718401E-03_REAL64, &
                                                        6.534789E-03_REAL64, &
                                                        -2.154901E-02_REAL64, &
                                                        -1.269164E-03_REAL64, &
                                                        -5.581286E-03_REAL64, &
                                                        -1.046570E-02_REAL64, &
                                                        -2.521171E-03_REAL64, &
                                                        6.429343E-03_REAL64, &
                                                        -1.119476E-03_REAL64, &
                                                        -1.114415E-02_REAL64, &
                                                        1.566675E-04_REAL64, &
                                                        -9.862381E-03_REAL64, &
                                                        -7.283755E-05_REAL64, &
                                                        8.990947E-03_REAL64, &
                                                        -1.164275E-04_REAL64, &
                                                        7.791520E-03_REAL64, &
                                                        -8.728383E-05_REAL64, &
                                                        7.077623E-03_REAL64, &
                                                        -4.165088E-05_REAL64, &
                                                        6.134795E-03_REAL64, &
                                                        9.479525E-07_REAL64, &
                                                        5.614971E-03_REAL64, &
                                                        3.744708E-05_REAL64, &
                                                        4.901176E-03_REAL64, &
                                                        5.943024E-05_REAL64, &
                                                        4.556257E-03_REAL64, &
                                                        7.814179E-05_REAL64, &
                                                        4.076007E-03_REAL64, &
                                                        8.703054E-05_REAL64, &
                                                        3.789669E-03_REAL64, &
                                                        7.467150E-05_REAL64, &
                                                        3.191407E-03_REAL64, &
                                                        6.090710E-05_REAL64, &
                                                        3.508745E-03_REAL64, &
                                                        1.009236E-04_REAL64, &
                                                        3.520077E-03_REAL64, &
                                                        1.094602E-04_REAL64, &
                                                        2.376136E-03_REAL64, &
                                                        2.620306E-03_REAL64, &
                                                        2.259885E-04_REAL64, &
                                                        5.832912E-05_REAL64 &
                                                    ], &
                                                    [range(50,100), range(1,1)], &
                                                    [148, 236] &
                                            ), &

                                            dipsto_v=subarray( &
                                                [ &
                                                    -8.113238E-02_REAL64, &
                                                    -2.356247E-02_REAL64, &
                                                    -9.722851E-02_REAL64, &
                                                    -8.152114E-02_REAL64, &
                                                    7.564886E-02_REAL64, &
                                                    9.068106E-02_REAL64, &
                                                    2.608631E-02_REAL64, &
                                                    1.164348E-01_REAL64, &
                                                    -5.458490E-02_REAL64, &
                                                    8.456954E-01_REAL64, &
                                                    -2.035323E-02_REAL64, &
                                                    1.854544E-01_REAL64, &
                                                    -1.536515E-01_REAL64, &
                                                    7.784576E-02_REAL64, &
                                                    4.220535E-02_REAL64, &
                                                    1.023078E-03_REAL64, &
                                                    6.787052E-02_REAL64, &
                                                    1.392114E-04_REAL64, &
                                                    2.770086E-03_REAL64 &
                                                ], &
                                                [range(10,28), range(50,50)], &
                                                [148,236] &  !array shape
                                            ) &
                                        ), &

                                        expected_dipole_block( &
                                            noterm=57_INT32,&
                                            isi=253_INT32,&
                                            inch=3_INT32,&
                                            jsi=127_INT32,&
                                            jnch=1_INT32,&
                                            nev=253_INT32,&
                                            nod=127_INT32,&
                                            lgsf=1_INT32,&
                                            lglf=2_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=1_INT32,&
                                            lgli=2_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                    [ &
                                                        -9.089862E-03_REAL64, &
                                                        3.585139E-03_REAL64, &
                                                        1.775836E-02_REAL64, &
                                                        1.156684E-03_REAL64, &
                                                        1.999247E-02_REAL64, &
                                                        2.962651E-03_REAL64, &
                                                        -1.048448E-02_REAL64, &
                                                        1.373996E-02_REAL64, &
                                                        1.173030E-03_REAL64, &
                                                        8.620638E-03_REAL64, &
                                                        -6.955897E-03_REAL64, &
                                                        2.481069E-03_REAL64, &
                                                        3.406730E-03_REAL64, &
                                                        5.454004E-03_REAL64, &
                                                        6.006768E-03_REAL64, &
                                                        5.160276E-03_REAL64, &
                                                        -1.108681E-02_REAL64, &
                                                        1.668180E-03_REAL64, &
                                                        -1.692183E-02_REAL64, &
                                                        1.993607E-04_REAL64, &
                                                        -1.117730E-03_REAL64, &
                                                        1.596936E-04_REAL64, &
                                                        2.572936E-04_REAL64, &
                                                        -4.852053E-03_REAL64, &
                                                        -1.665341E-05_REAL64, &
                                                        3.132290E-03_REAL64 &
                                                    ], &
                                                    [range(100,125), range(1,1)], &
                                                    [253, 127] &
                                            ), &

                                            dipsto_v=subarray( &
                                                [ &
                                                    1.303117E-02_REAL64, &
                                                    1.064041E-02_REAL64, &
                                                    -1.763949E-02_REAL64, &
                                                    -4.464284E-03_REAL64, &
                                                    -1.906660E-02_REAL64, &
                                                    -8.099315E-03_REAL64, &
                                                    1.310273E-02_REAL64, &
                                                    -3.568501E-02_REAL64, &
                                                    -2.179015E-02_REAL64, &
                                                    -2.403434E-02_REAL64, &
                                                    1.069614E-03_REAL64, &
                                                    -3.545089E-03_REAL64, &
                                                    -3.155020E-03_REAL64, &
                                                    -5.011089E-03_REAL64, &
                                                    -1.667112E-03_REAL64, &
                                                    -8.688718E-03_REAL64, &
                                                    3.083860E-02_REAL64, &
                                                    -8.565073E-04_REAL64, &
                                                    1.334753E-02_REAL64, &
                                                    -1.362267E-03_REAL64, &
                                                    -1.180076E-02_REAL64, &
                                                    -9.737083E-04_REAL64, &
                                                    -1.918783E-02_REAL64, &
                                                    -1.151048E-02_REAL64, &
                                                    -1.899546E-03_REAL64, &
                                                    -1.558553E-03_REAL64 &
                                                ], &
                                                [range(100,125), range(1,1)], &
                                                [253,127] &  !array shape
                                            ) &
                                        ) &
                                    ] &
                        )

        CALL test_D_file(error, &
                        "atomic_tests/small_tests/argon", &
                        expected, &
                        ntarg, &
                        nstmx, &
                        no_of_L_blocks, &
                        no_of_LML_blocks, &
                        mnp1, &
                        L_block_lrgl, &
                        L_block_nspn, &
                        L_block_npty, &
                        LML_block_lrgl, &
                        LML_block_nspn, &
                        LML_block_npty, &
                        LML_block_ml, &
                        dipole_coupled, &
                        block_ind, &
                        coupling_id, &
                        xy_plane_desired, &
                        ML_max, &
                        lplusp, &
                        dipole_velocity_output, &
                        loc_of_blocks)
        
    END SUBROUTINE test_atomic_small_argon

    SUBROUTINE test_atomic_small_helium(error)
        USE initial_conditions, ONLY: xy_plane_desired
        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file)               :: expected

        INTEGER(INT32)                      :: ntarg
        INTEGER(INT32)                      :: nstmx
        INTEGER(INT32)                      :: no_of_L_blocks
        INTEGER(INT32)                      :: no_of_LML_blocks
        INTEGER, ALLOCATABLE                :: L_block_lrgl(:)
        INTEGER, ALLOCATABLE                :: L_block_nspn(:)
        INTEGER, ALLOCATABLE                :: L_block_npty(:)
        INTEGER, ALLOCATABLE                :: LML_block_lrgl(:)
        INTEGER, ALLOCATABLE                :: LML_block_nspn(:)
        INTEGER, ALLOCATABLE                :: LML_block_npty(:)
        INTEGER, ALLOCATABLE                :: LML_block_ml(:)
        INTEGER, ALLOCATABLE                :: mnp1(:)
        INTEGER, ALLOCATABLE                :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE                :: block_ind(:, :, :)
        INTEGER(INT32)                      :: coupling_id
        INTEGER(INT32)                      :: ML_max
        INTEGER(INT32)                      :: lplusp
        LOGICAL                             :: dipole_velocity_output

        INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

        !constants for Helium

        ntarg = 3
        nstmx = 154
        no_of_L_blocks = 4
        no_of_LML_blocks = 4

        loc_of_blocks = [1, 3]
        L_block_lrgl = [0,1,2,3]
        L_block_nspn = [1,1,1,1]
        L_block_npty = [0,1,0,1]
        LML_block_lrgl = [0,1,2,3]
        LML_block_nspn = [1,1,1,1]
        LML_block_npty = [0,1,0,1]
        LML_block_ml = [0,0,0,0]
        mnp1 = [118,154,152,148]
        coupling_id = 1
        xy_plane_desired = .false.
        ML_max = 0
        lplusp = 0
        dipole_velocity_output = .true.

        expected = expected_D_file( &
                                        ntarg=3_INT32, &
                                        fintr=3_INT32,&
                                        crlv=RESHAPE( &
                                        [ &
                                            0.000000E+00_REAL64, &
                                            8.660254E-01_REAL64, &
                                            0.000000E+00_REAL64, &
                                            8.660254E-01_REAL64, &
                                            0.000000E+00_REAL64, &
                                            -1.000000E+00_REAL64, &
                                            0.000000E+00_REAL64, &
                                            -1.000000E+00_REAL64, &
                                            0.000000E+00_REAL64 &
                                        ], &
                                        [3, 3] &
                                        ) , &

                                        crlv_v=RESHAPE( &
                                        [ &
                                            0.000000E+00_REAL64, &
                                            -1.732051E+00_REAL64, &
                                            0.000000E+00_REAL64, &
                                            1.732051E+00_REAL64, &
                                            0.000000E+00_REAL64, &
                                            9.999999E-01_REAL64, &
                                            0.000000E+00_REAL64, &
                                            -9.999999E-01_REAL64, &
                                            0.000000E+00_REAL64 &
                                        ], &
                                        [3, 3] &
                                        ), &
        
                                    blocks = [ &
                                        expected_dipole_block( &
                                            noterm=38_INT32,&
                                            isi=118_INT32,&
                                            inch=3_INT32,&
                                            jsi=154_INT32,&
                                            jnch=4_INT32,&
                                            nev=118_INT32,&
                                            nod=154_INT32,&
                                            lgsf=1_INT32,&
                                            lglf=1_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=1_INT32,&
                                            lgli=0_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                    [ &
                                                        -1.155662E-01_REAL64, &
                                                        4.126236E-01_REAL64, &
                                                        -2.490217E-01_REAL64, &
                                                        -1.476910E-01_REAL64, &
                                                        1.419605E-02_REAL64, &
                                                        -5.728472E-02_REAL64, &
                                                        3.457452E-02_REAL64, &
                                                        2.520387E-02_REAL64, &
                                                        -4.004672E-03_REAL64, &
                                                        8.341671E-03_REAL64, &
                                                        3.940348E-03_REAL64, &
                                                        -2.037206E-03_REAL64, &
                                                        -8.358899E-04_REAL64, &
                                                        -8.029464E-04_REAL64, &
                                                        7.830308E-04_REAL64, &
                                                        5.594740E-03_REAL64, &
                                                        2.872925E-03_REAL64, &
                                                        1.782048E-03_REAL64, &
                                                        -6.060482E-04_REAL64, &
                                                        -6.326081E-03_REAL64, &
                                                        -3.124761E-03_REAL64, &
                                                        -4.766928E-04_REAL64, &
                                                        -9.213027E-03_REAL64, &
                                                        3.036452E-03_REAL64, &
                                                        -2.508103E-03_REAL64, &
                                                        9.151031E-03_REAL64 &
                                                    ], &
                                                    [range(10,35), range(1,1)], &
                                                    [118, 154] &
                                            ), &

                                            dipsto_v=subarray( &
                                                [ &
                                                    1.979000E-01_REAL64, &
                                                    -7.572385E-01_REAL64, &
                                                    5.103489E-01_REAL64, &
                                                    3.109574E-01_REAL64, &
                                                    -2.751344E-02_REAL64, &
                                                    1.249438E-01_REAL64, &
                                                    -8.171546E-02_REAL64, &
                                                    -6.374762E-02_REAL64, &
                                                    1.115695E-02_REAL64, &
                                                    -2.425890E-02_REAL64, &
                                                    -1.080034E-02_REAL64, &
                                                    5.813366E-03_REAL64, &
                                                    2.803150E-03_REAL64, &
                                                    3.984211E-03_REAL64, &
                                                    -3.324622E-03_REAL64, &
                                                    -1.786242E-02_REAL64, &
                                                    -8.482959E-03_REAL64, &
                                                    -4.696940E-03_REAL64, &
                                                    3.219054E-03_REAL64, &
                                                    2.240598E-02_REAL64, &
                                                    1.127660E-02_REAL64, &
                                                    6.675534E-04_REAL64, &
                                                    3.781281E-02_REAL64, &
                                                    -1.375109E-02_REAL64, &
                                                    9.992873E-03_REAL64, &
                                                    -4.265070E-02_REAL64 &
                                                ], &
                                                [range(10,35), range(1,1)], &
                                                [118,154] &  !array shape
                                            ) &
                                        ), &

                                        expected_dipole_block( &
                                            noterm=37_INT32,&
                                            isi=152_INT32,&
                                            inch=4_INT32,&
                                            jsi=148_INT32,&
                                            jnch=4_INT32,&
                                            nev=152_INT32,&
                                            nod=148_INT32,&
                                            lgsf=1_INT32,&
                                            lglf=3_INT32,&
                                            lgpf=1_INT32,&
                                            lgsi=1_INT32,&
                                            lgli=2_INT32,&
                                            lgpi=0_INT32,&
                                            dipsto=subarray( &
                                                    [ &
                                                        -7.301157E-02_REAL64, &
                                                        1.539795E-02_REAL64, &
                                                        -1.117700E+00_REAL64, &
                                                        -1.361651E-02_REAL64, &
                                                        -3.312499E-03_REAL64, &
                                                        1.064894E-02_REAL64, &
                                                        -6.189273E-02_REAL64, &
                                                        -4.239716E-03_REAL64, &
                                                        -7.980550E-03_REAL64, &
                                                        -1.611399E-04_REAL64, &
                                                        -8.751007E-03_REAL64, &
                                                        -3.075370E-03_REAL64, &
                                                        4.187768E-03_REAL64, &
                                                        5.084994E-02_REAL64, &
                                                        6.666067E-03_REAL64, &
                                                        4.280618E-04_REAL64, &
                                                        6.768195E-03_REAL64, &
                                                        6.849575E-04_REAL64, &
                                                        -4.658636E-04_REAL64, &
                                                        -9.899097E-04_REAL64, &
                                                        -2.511655E-02_REAL64, &
                                                        -3.478906E-02_REAL64, &
                                                        7.183202E-03_REAL64, &
                                                        6.769063E-05_REAL64, &
                                                        6.281703E-03_REAL64, &
                                                        -9.117800E-04_REAL64 &
                                                    ], &
                                                    [range(10,35), range(1,1)], &
                                                    [152, 148] &
                                            ), &

                                            dipsto_v=subarray( &
                                                [ &
                                                    1.302910E-01_REAL64, &
                                                    -3.062495E-02_REAL64, &
                                                    2.235380E+00_REAL64, &
                                                    2.717056E-02_REAL64, &
                                                    6.587244E-03_REAL64, &
                                                    -2.287850E-02_REAL64, &
                                                    1.369013E-01_REAL64, &
                                                    9.609204E-03_REAL64, &
                                                    1.853290E-02_REAL64, &
                                                    5.446821E-04_REAL64, &
                                                    2.241941E-02_REAL64, &
                                                    7.855264E-03_REAL64, &
                                                    -1.022574E-02_REAL64, &
                                                    -1.366712E-01_REAL64, &
                                                    -1.766644E-02_REAL64, &
                                                    -1.094049E-03_REAL64, &
                                                    -1.943313E-02_REAL64, &
                                                    -1.892001E-03_REAL64, &
                                                    1.229948E-03_REAL64, &
                                                    3.063584E-03_REAL64, &
                                                    7.979604E-02_REAL64, &
                                                    1.120562E-01_REAL64, &
                                                    -2.335250E-02_REAL64, &
                                                    -1.278847E-04_REAL64, &
                                                    -2.225641E-02_REAL64, &
                                                    3.364488E-03_REAL64 &
                                                ], &
                                                [range(10,35), range(1,1)], &
                                                [152,148] &  !array shape
                                            ) &
                                        ) &
                                    ] &
                        )

        CALL test_D_file(error, &
                        "atomic_tests/small_tests/helium", &
                        expected, &
                        ntarg, &
                        nstmx, &
                        no_of_L_blocks, &
                        no_of_LML_blocks, &
                        mnp1, &
                        L_block_lrgl, &
                        L_block_nspn, &
                        L_block_npty, &
                        LML_block_lrgl, &
                        LML_block_nspn, &
                        LML_block_npty, &
                        LML_block_ml, &
                        dipole_coupled, &
                        block_ind, &
                        coupling_id, &
                        xy_plane_desired, &
                        ML_max, &
                        lplusp, &
                        dipole_velocity_output, &
                        loc_of_blocks)

    END SUBROUTINE test_atomic_small_helium

END MODULE



