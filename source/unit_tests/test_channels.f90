MODULE test_channels
    USE ISO_FORTRAN_ENV, ONLY: REAL64
    USE testdrive, ONLY: error_type, new_unittest, unittest_type, test_failed
    USE testing_utilities, ONLY: compare, format_context

    IMPLICIT NONE
    PRIVATE
    PUBLIC :: collect_channels   !  rename this subroutine to suit your unit test (also line 15)

    CONTAINS
    SUBROUTINE collect_channels(testsuite)
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite

        testsuite = [ &
                    new_unittest("AttachWfnToChannel: ", test_AttachWfnToChannel), & 
                    new_unittest("AddChannel LS:", test_AddLSChannel), &
                    new_unittest("AddChannel jK:", test_AddjKChannel) &
                    ]      

    END SUBROUTINE collect_channels

    SUBROUTINE test_AddLSChannel(error) 
        USE modChannel, ONLY: channel, AddChannel
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(Channel), ALLOCATABLE :: Channels(:)
        INTEGER :: expected(9)
        INTEGER :: ii
        CHARACTER(len=:), ALLOCATABLE :: context

        context = "" 
        expected = [1, 2, 3, 4, 5, 7, 6, 0, 6]
        Allocate(Channels(0))

        CALL AddChannel(Target_Counter=1, &
                        l_of_Target=2, &
                        l_in_current_sym=3, &
                        Current_L=4, &
                        ml=5, &
                        pty=6, &
                        Current_S=7, &
                        s_of_target=8, &
                        coupling_id=1, &
                        channelsIn=Channels)

        IF (.NOT. compare(ubound(Channels, 1), 1)) THEN
            CALL format_context(context, "Number of channels", ubound(Channels, 1), 1)
        END IF
        ASSOCIATE(chan=>Channels(1))
            DO ii=1,9
                IF (.NOT. compare(chan%QNumber(ii), expected(ii))) THEN
                    CALL format_context(context, chan%label(ii), chan%QNumber(ii), expected(ii))
                END IF
            END DO
        END ASSOCIATE

        ! Try to add an identical channel, and assert that it hasn't been added
        CALL AddChannel(Target_Counter=1, &
                        l_of_Target=2, &
                        l_in_current_sym=3, &
                        Current_L=4, &
                        ml=5, &
                        pty=6, &
                        Current_S=7, &
                        s_of_target=8, &
                        coupling_id=1, &
                        channelsIn=Channels)
        IF (.NOT. compare(ubound(Channels, 1), 1)) THEN
            CALL format_context(context, "Added identical channel", ubound(Channels, 1), 1)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "LS channel set incorrectly!", context)
        END IF
    END SUBROUTINE test_AddLSChannel


    SUBROUTINE test_AddjKChannel(error) 
        USE modChannel, ONLY: channel, AddChannel
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(Channel), ALLOCATABLE :: Channels(:)
        CHARACTER(len=:), ALLOCATABLE :: context
        INTEGER :: expected(7)
        INTEGER :: ii

        context = "" 
        expected = [1, 2, 1, 2, 3, 6, 7]
        Allocate(Channels(0))

        CALL AddChannel(Target_Counter=1, &
                        l_of_Target=2, &
                        l_in_current_sym=1, &
                        Current_L=3, &
                        ml=6, &
                        pty=7, &
                        Current_S=8, &
                        s_of_target=9, &
                        coupling_id=2, &
                        channelsIn=Channels)

        IF (.NOT. compare(ubound(Channels, 1), 1)) THEN
            CALL format_context(context, "Number of channels", ubound(Channels, 1), 1)
        END IF
        ASSOCIATE(chan=>Channels(1))
            DO ii=1,7
                IF (.NOT. compare(chan%QNumber(ii), expected(ii))) THEN
                    CALL format_context(context, chan%label(ii), chan%QNumber(ii), expected(ii))
                END IF
            END DO
        END ASSOCIATE

        ! Try to add an identical channel, and assert that it correctly picks the new K value
        CALL AddChannel(Target_Counter=1, &
                        l_of_Target=2, &
                        l_in_current_sym=1, &
                        Current_L=3, &
                        ml=6, &
                        pty=7, &
                        Current_S=8, &
                        s_of_target=9, &
                        coupling_id=2, &
                        channelsIn=Channels)
        expected = [1, 2, 1, 4, 3, 6, 7]
        IF (.NOT. compare(ubound(Channels, 1), 2)) THEN
            CALL format_context(context, "Failed to add second K value", ubound(Channels, 1), 2)
        END IF
        ASSOCIATE(chan=>Channels(2))
            DO ii=1,7
                IF (.NOT. compare(chan%QNumber(ii), expected(ii))) THEN
                    CALL format_context(context, chan%label(ii), chan%QNumber(ii), expected(ii))
                END IF
            END DO
        END ASSOCIATE

        IF (context /= "") THEN
            CALL test_failed(error, "jK channel set incorrectly!", context)
        END IF
    END SUBROUTINE test_AddjKChannel

    SUBROUTINE test_AttachWfnToChannel(error) 
        USE modChannel, ONLY: channel, AttachWfnToChannel, DetachWfnFromChannel
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(Channel), ALLOCATABLE :: Channels(:)
        COMPLEX*16, ALLOCATABLE :: wv(:, :, :), wv_store(:, :, :), actual(:, :), expected(:, :)
        REAL(REAL64) :: re(1:4, 1:3, 1:2), im(1:4, 1:3, 1:2)
        INTEGER :: chan
        CHARACTER(len=:), ALLOCATABLE :: context

        context = "" 

        ALLOCATE(Channels(3), wv(4, 3, 2), wv_store(4, 3, 2))
        CALL random_number(re)
        CALL random_number(im)
        wv = cmplx(re, im, 8)
        wv_store = wv

        CALL AttachWfnToChannel(Channels, wv)

        DO chan=1,3
            actual = Channels(chan)%Wavefunction
            expected = wv_store(:, chan, :)
            IF (.NOT. compare(actual, expected)) THEN
                CALL format_context(context, "Channels(chan)%Wavefunction", actual, expected)
            END IF
        END DO

        IF (context /= "") THEN
            CALL test_failed(error, "Wfn not attached to channels properly", context)
        END IF

        context = "" 

        CALL DetachWfnFromChannel(Channels, wv)

        IF (.NOT. compare(wv, wv_store)) THEN
            CALL format_context(context, "wv", wv, wv_store)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "Wfn not detached from channels properly", context)
        END IF

    END SUBROUTINE test_AttachWfnToChannel
END MODULE test_channels