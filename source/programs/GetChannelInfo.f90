! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @brief program to print quantum numbers of electron emission channels in RMT calculations

PROGRAM GetChannelInfo

    USE precisn,            ONLY: wp
    USE readhd,             ONLY: reform_reads
    USE initial_conditions, ONLY: get_disk_path, &
                                  read_initial_conditions
    USE version_control,    ONLY: print_program_header
    USE modChannel,         ONLY: Channel, PrintAllChannels
    USE recoupling,         ONLY: Recouple
    USE postprocessing,     ONLY: process_command_line

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE :: wv(:, :, :)  ! the outer wavefunction (points,channels,solutions)
    REAL(wp),    ALLOCATABLE :: alpha(:), beta(:), gamma(:)
    INTEGER,     ALLOCATABLE :: channels_included(:)

    INTEGER  :: ntheta = 360, maxpt = 10000
    LOGICAL  :: calc_density = .FALSE., calc_momentum = .FALSE., vtk = .FALSE., &
                spectrum_only = .FALSE., recon = .TRUE., coulomb = .FALSE., decouple= .FALSE.
    REAL(wp):: skip = 200
    TYPE(Channel), ALLOCATABLE:: channels(:)


    ! initialize; find and read input configuration
    CALL print_program_header
    CALL get_disk_path('./')
    CALL read_initial_conditions
    CALL process_command_line(calc_density, calc_momentum, ntheta, maxpt, skip, alpha, beta, &
                              gamma, channels_included, vtk, spectrum_only, recon, coulomb, decouple)
    CALL reform_reads(channels)

    CALL PrintAllChannels(channels, filename="CoupledChannelInfo")
    WRITE (*, '(/,1x,"Coupled channels written to CoupledChannelInfo")')
    IF (decouple) THEN
        ALLOCATE(wv(1, 1, size(channels))) ! Recouple expects wv to be allocated
        CALL Recouple(channels, wv) 
        CALL PrintAllChannels(channels, filename="DecoupledChannelInfo")
        WRITE (*, '(/,1x,"Decoupled channels written to DecoupledChannelInfo")')
    END IF

END PROGRAM GetChannelInfo
