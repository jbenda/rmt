"""Classes for convenient application of windows.

By default the ``RMT_gen_tas`` utility will window some of the data with a window which starts at ``1.0`` and goes
to ``0.0`` at the last element. For ATAS the window starts halfway through the data, but for HHG the window starts
99% of the way through the data. There are options to change this, however.

If you just need a linear window starting at a certain time step index, you can use the ``window_start`` argument
to the ``RMT_gen_tas`` utility. ::

    RMT_gen_tas --window_start 65364

Using a negative value will disable windowing. ::

    RMT_gen_tas --window_start -1

If you need more complex windowing behaviour then you will have to write your own code to do so. An exponential
decay window can be applied using the following script.

.. code-block:: python

    from rmt_utilities import rmtutil, windowing

    calc = rmtutil.RMTCalc(".")

    window = windowing.ExponentialStep(0.1, 36732)
    tas = calc.ATAS(window=window)
    tas.write(fname="TAS_ExpDecay_", units="eV")

You can apply an arbitrary window by passing a NumPy array.

.. code-block:: python

    import numpy as np
    from rmt_utilities import rmtutil

    calc = rmtutil.RMTCalc(".")
    calc._initExpecFiles(None)

    window = np.bartlett(calc.field.shape[0])
    window[:calc.field.shape[0] // 2] = 1.0
    tas = calc.ATAS(window=window)
    tas.write(fname="TAS_BartlettArray_", units="eV")

However, it is *significantly* less error prone to define a ``Window`` subclass as this does not require the use of
internal ``RMTCalc`` attributes and will perform some basic sense checks on the returned window.

.. code-block:: python

    import numpy as np
    from rmt_utilities import rmtutil, windowing

    class Bartlett(windowing.Window):
        def window(self, length):
            mask = np.bartlett(length)
            mask[:length//2] = 1.0
            return mask

    calc = rmtutil.RMTCalc(".")

    tas = calc.ATAS(window=Bartlett())
    tas.write(fname="TAS_BartlettClass_", units="eV")
"""
import abc
import numpy as np
import warnings


class Window(abc.ABC):
    """An abstract base class describing a function that can window some data.

    Subclasses *must* define a ``window`` method that returns something that can be multiplied by the data to
    window it. Most commonly this will be a 1D NumPy array of the same length as the data.

    Generally your window should be ``1.0`` until your laser field becomes zero, then should monotonically decrease
    and become sufficiently small as to be essentially zero at the final element.

    Be careful of using windows built using sinusoids, as these have been known to cause misleading results.
    """

    @abc.abstractmethod
    def window(self, length):
        raise NotImplementedError

    def apply(self, data):
        window = self.window(len(data))

        warning_limit = 1e-16
        if window[-1] > warning_limit:
            warnings.warn(f"Window is greater than {warning_limit} at final element (value: {window[-1]}), see \
                          ``warnings`` module to silence this warning.")

        if np.any(window > 1):
            warnings.warn("Window has elements greater than 1.0, see ``warnings`` module to silence this warning.")

        return window * data


def _assert_inbounds(index, length):
    if index >= length:
        raise IndexError(f"index {index} is greater than maximum index {length + 1}")


def _linear(index, length):
    _assert_inbounds(index, length)

    mask = np.ones(length)
    # The ``-1`` in ``length - index - 1`` ensures the last element is exactly ``0``.
    mask[index:] = 1 - (np.arange(length - index) / (length - index - 1))
    return mask


class LinearStep(Window):
    """Window using a straight line curve starting at a given step.

    Parameters
    ----------
        index: int
            The time step index at which to start the window.
    """

    def __init__(self, index):
        self.index = index

    def window(self, length):
        return _linear(self.index, length)


class LinearProportion(Window):
    """Window using a straight line curve starting a given proportion of the way through the data.

    Parameters
    ----------
        proportion: float
            The proportion through the data to start windowing.
    """

    def __init__(self, proportion):
        self.proportion = proportion

    def window(self, length):
        index = round(self.proportion * length)
        return _linear(index, length)


class HalfBlackman(Window):
    """Window using a Blackman decay in the second half of the data only
    """

    def window(self, length):
        index = round(0.5 * length)
        mask = np.blackman(length)
        mask[:index] = 1.0
        return mask


class SuperGaussian(Window):
    """Window using a super Gaussian decay in the second half of the data only
    """

    def window(self, length):
        index = round(0.5 * length)
        t = np.linspace(-2, 2, length)
        mask = np.exp(-(t**4))
        mask[:index] = 1.0
        return mask


class ExponentialProportion(Window):
    """Window using an exponential decay starting some proportion of the way through the signal,
      and decaying smoothly to zero

    Parameters
    ----------
        proportion: float
            The proportion through the data to start windowing.
    """
    def __init__(self, proportion):
        self.proportion = proportion

    def window(self, length):

        mask = np.ones(length)
        decay_points = round((1-self.proportion) * length)

        t = np.linspace(0, 37, decay_points)  # 37 ensures final value of window is < 1e-16
        mask[-decay_points:] = np.exp(-t)

        return mask


class ExponentialStep(Window):
    """Window using an exponential decay with a given lifetime, starting at a given step.

    Parameters
    ----------
        lifetime: float
            The lifetime of the exponential decay. The window is defined as ``exp(-lifetime * (i - index))`` where
            ``i`` is the time step index.
        index: int
            The time step index at which to start the window.
    """
    def __init__(self, lifetime, index):
        self.lifetime = lifetime
        self.index = index

    def window(self, length):
        _assert_inbounds(self.index, length)

        mask = np.ones(length)
        mask[self.index:] = np.exp(-self.lifetime * np.arange(length - self.index))

        return mask
