import pytest


def pytest_addoption(parser):
    parser.addoption("--skip_slow", action="store_true",
                     help="skip slow tests (marked with marker @slow)")


def pytest_runtest_setup(item):
    if 'slow' in item.keywords and item.config.getoption("--skip_slow"):
        pytest.skip("slow tests skipped with option skip_slow")
